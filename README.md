# Sapot Browser

This is a fork of Morph browser (https://gitlab.com/ubports/core/morph-browser) but with experimental changes which may or may not come to Morph.

For logging issues and merge requests, I highly advised to log them in Morph's repo if they are not specific to Sapot's changes or features.
I want to avoid stealing efforts and contributions to the Morph browser :)

Morph Icon is based on Origami Licensed CC-by Creative Mania from the Noun Project

