import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Controls 2.12 as QQC2
import QtWebEngine 1.5
import QtQuick.Layouts 1.12
import "TextUtils.js" as TextUtils

DialogWithContents {
    id: allowNewViewRequestDialog

    objectName: "confirmDialog"

    readonly property string elidedUrl: TextUtils.elideText(url, 200)

    property string url
    property string domain
    property int destination
    property bool showRememberDecision

    signal allow
    signal allowPermanentlyForDomain
    signal allowPermanentlyForAll
    signal deny
    signal denyPermanentlyForDomain

    title: {
        switch (destination) {
            case WebEngineView.NewViewInTab:
                return i18n.tr("Open link in new tab")

            case WebEngineView.NewViewInBackgroundTab:
               return i18n.tr("Open link in background tab")

            case WebEngineView.NewViewInWindow:
            case WebEngineView.NewViewInDialog:
                return i18n.tr("Open link in new window")
        }

        return i18n.tr("Open link in new view")
    }
    closePolicy: QQC2.Popup.NoAutoClose
    destroyOnClose: true

    onAllow: close()
    onAllowPermanentlyForDomain: close()
    onAllowPermanentlyForAll: close()
    onDeny: close()
    onDenyPermanentlyForDomain: close()

    QQC2.Label {
        text: i18n.tr("'%1' is trying to open this URL in a new view:").arg(allowNewViewRequestDialog.domain)
        Layout.fillWidth: true
        wrapMode: Text.WordWrap
    }
    
    DialogCaption {
        Layout.fillWidth: true
        text: elidedUrl
    }

    QQC2.ButtonGroup { id: rememberBtnGrp }

    ColumnLayout {
        Layout.fillWidth: true

        CustomizedCheckBoxDelegate {
            id: rememberSwitch

            visible: allowNewViewRequestDialog.showRememberDecision
            Layout.fillWidth: true
            text: i18n.tr("Remember decision")
        }

        ColumnLayout {
            Layout.fillWidth: true
            visible: rememberSwitch.checked

            CustomizedRadioButton {
                id: rememberForDomainButton

                checked: true
                text: i18n.tr("For '%1'").arg(allowNewViewRequestDialog.domain)
                Layout.fillWidth: true
                Layout.leftMargin: units.gu(2)
                QQC2.ButtonGroup.group: rememberBtnGrp
            }

            CustomizedRadioButton {
                id: rememberForAllButton

                checked: true
                text: i18n.tr("For all sites")
                Layout.fillWidth: true
                Layout.leftMargin: units.gu(2)
                QQC2.ButtonGroup.group: rememberBtnGrp
            }
        }
    }

    ColumnLayout {
        spacing: units.gu(2)
        Layout.fillWidth: true

        Button {
            id: saveButton
            objectName: "allowButton"

            Layout.fillWidth: true
            text: i18n.tr("Allow")
            color: theme.palette.normal.positive

            onClicked: {
                if (rememberSwitch.checked) {
                    if (rememberForDomainButton.checked) {
                        allowNewViewRequestDialog.allowPermanentlyForDomain()
                    } else if (rememberForAllButton.checked) {
                        allowNewViewRequestDialog.allowPermanentlyForAll()
                    }
                } else {
                    allowNewViewRequestDialog.allow()
                }
            }
        }

        Button {
            objectName: "denyButton"

            Layout.fillWidth: true
            visible: !rememberForAllButton.checked
            enabled: visible
            text: i18n.tr("Deny")
            color: theme.palette.normal.negative

            onClicked: {
                if (rememberSwitch.checked && rememberForDomainButton.checked) {
                    allowNewViewRequestDialog.denyPermanentlyForDomain()
                } else {
                    allowNewViewRequestDialog.deny()
                }
            }
        }
    }
}
