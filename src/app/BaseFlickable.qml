import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12

Flickable {
    id: baseFlickable

    property CustomizedPageHeader pageHeader
    property bool enableScrollPositioner: true

    boundsBehavior: Flickable.DragOverBounds
    boundsMovement: Flickable.StopAtBounds
    maximumFlickVelocity: units.gu(500)

    PullDownFlickableConnections {
        pageHeader: baseFlickable.pageHeader
        target: baseFlickable
    }

    Loader {
        active: baseFlickable.parent instanceof Layout ? false : baseFlickable.enableScrollPositioner
        z: 1
        parent: baseFlickable.parent
        anchors {
            right: active ? parent.right : undefined
            rightMargin: units.gu(2)
            bottom: active ? parent.bottom : undefined
            bottomMargin: units.gu(3) + baseFlickable.bottomMargin
        }

        sourceComponent: ScrollPositioner {
            id: scrollPositioner

            target: baseFlickable
            mode: "Down"
        }
    }
}
