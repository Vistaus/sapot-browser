import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12

ListView {
    id: baseListView

    property CustomizedPageHeader pageHeader
    property bool enableScrollPositioner: true

    boundsBehavior: Flickable.DragOverBounds
    boundsMovement: Flickable.StopAtBounds
    maximumFlickVelocity: units.gu(500)

    PullDownFlickableConnections {
        pageHeader: baseListView.pageHeader
        target: baseListView
    }

    Loader {
        active: baseListView.parent instanceof Layout ? false : baseListView.enableScrollPositioner
        z: 1
        parent: baseListView.parent
        anchors {
            right: active ? parent.right : undefined
            rightMargin: units.gu(2)
            bottom: active ? parent.bottom : undefined
            bottomMargin: units.gu(3) //+ baseListView.bottomMargin
        }

        sourceComponent: ScrollPositioner {
            id: scrollPositioner

            target: baseListView
            mode: "Down"
        }
    }
}
