/*
 * Copyright 2014-2016 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Window 2.2
import QtSystemInfo 5.0
import Ubuntu.Components 1.3
import QtSensors 5.2
import QtQuick.Controls 2.2 as QQC2
import QtQuick.Controls.Suru 2.2

QQC2.ApplicationWindow {
    id: window

    property bool developerExtrasEnabled: false
    property bool forceFullscreen: false
    property bool manualFullscreen: false // When user entered fullscreen manually via shortcut or button
    property var currentWebview: null
    property bool hasTouchScreen: false

    readonly property bool sensorExists: orientationSensor.connectedToBackend
    readonly property int sensorOrientation: orientationSensor.reading ? orientationSensor.reading.orientation : 0
    readonly property int rotationAngle: {
        if (orientationSensor.reading && Screen.angleBetween(Screen.orientation, orientationSensor.orientation) !== 0) {
            switch (orientationSensor.reading.orientation) {
                case 1: /* OrientationReading.TopUp */
                case 3: /* OrientationReading.LeftUp */
                    return 270
                    break
                case 4: /* OrientationReading.RightUp */
                case 2: /* OrientationReading.TopDown */
                    return 90
                    break
                
                case 5: /* OrientationReading.FaceUp */
                case 6: /* OrientationReading.FaceDown */
                default:
                    return 0
            }
        }

        return 0
    }

    minimumWidth: units.gu(40)
    minimumHeight: units.gu(20)

    width: units.gu(100)
    height: units.gu(75)

    // Change theme in real time when set to follow system theme
    // Only works when the app gets unfocused then focused
    // Possibly ideal so the change won't happen while the user is using the app
    property string previousTheme: Theme.name
    Connections {
        target: Qt.application
        onStateChanged: {
            if (previousTheme !== theme.name) {
                window.Suru.theme = Theme.name == "Ubuntu.Components.Themes.SuruDark" ? Suru.Dark : Suru.Light
                theme.name = Theme.name
                theme.name = ""
            }
            previousTheme = Theme.name
        }
    }

    QtObject {
        id: internal
        property int currentWindowState: Window.Windowed
    }

    OrientationSensor {
        id: orientationSensor

        readonly property int orientation: {
            if (reading) {
                switch (reading.orientation) {
                    case 1:
                    case 2:
                        if (screenNativeOrientation == Qt.LandscapeOrientation
                            || screenNativeOrientation == Qt.InvertedLandscapeOrientation) {
                            return Qt.LandscapeOrientation
                        } else {
                            return Qt.PortraitOrientation
                        }
                        break
                    case 3:
                    case 4:
                        if (screenNativeOrientation == Qt.LandscapeOrientation
                            || screenNativeOrientation == Qt.InvertedLandscapeOrientation) {
                            return Qt.PortraitOrientation
                        } else {
                            return Qt.LandscapeOrientation
                        }
                        break
                    default:
                        return orientation
                }
            } else {
                return Qt.PortraitOrientation
            }
        }

        active: window.visibility == Window.FullScreen && !browser.settings.autoVideoRotate
    }

    ScreenSaver {
        id: screenSaver
        screenSaverEnabled: ! ( window.active && window.currentWebview && (window.currentWebview.isFullScreen || window.currentWebview.recentlyAudible) )
    }

    Connections {
        target: window.currentWebview
        onIsFullScreenChanged: {
            if (!window.manualFullscreen) {
                window.setFullscreen(window.currentWebview.isFullScreen)
            }
        }
    }

    function setFullscreen(fullscreen) {
        if (!window.forceFullscreen) {
            if (fullscreen) {
                if (window.visibility != Window.FullScreen) {
                    internal.currentWindowState = window.visibility
                    window.visibility = Window.FullScreen
                }
            } else {
                window.visibility = internal.currentWindowState
            }
        }
    }
}
