/*
 * Copyright 2014-2015 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import "." as Common

AbstractButton {
    id: chromeButton

    property real iconSize: width
    property alias iconName: icon.name
    property alias iconColor: icon.color
    property bool enableContextMenu: false
    property var contextMenu: null
    property bool pressedState: false

    //For bottom menus only
    property bool closeMenuOnTrigger: false
    property bool alwaysDisplayAtBottom: false

    signal showContextMenu(bool fromBottom, var caller)
    signal triggerAction(bool fromBottom, var caller)

    onShowContextMenu: if (!fromBottom) pressedState = true

    Connections {
        target: contextMenu
        onVisibleChanged: if (!target.visible) pressedState = false
    }

    Rectangle {
        anchors.fill: parent
        color: theme.palette.selected.background
        visible: parent.pressed || pressedState
    }

    Icon {
        id: icon
        anchors.centerIn: parent
        width: parent.iconSize
        height: width
        asynchronous: true
    }

    opacity: enabled ? 1.0 : 0.3

    Behavior on width {
        UbuntuNumberAnimation {}
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.RightButton
        onClicked: if (enableContextMenu) showContextMenu(false, chromeButton)
    }

    onPressAndHold: if (enableContextMenu) showContextMenu(false, chromeButton)
    onClicked: {
        triggerAction(false, chromeButton)
        Common.Haptics.playSubtle()
    }
}
