import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12
import "." as Local

BaseDialog {
    id: confirmationDialog
    objectName: "confirmationDialog"

    readonly property real preferredHeight: contentColumn.height + units.gu(12)
    readonly property bool optionsAreAvailable: optionsList.length > 0
    property alias text: confirmationLabel.text
    property string confirmButtonText: i18n.tr("OK")
    property string cancelButtonText: i18n.tr("Cancel")
    property var acceptFunction // function to be executed upon confirming
    property bool confirmIsPositive: true // Determines the color of the confirm button
    property var optionsList: []

    standardButtons: QQC2.Dialog.NoButton
    height: parent ? Math.min(parent.height, preferredHeight) : preferredHeight

    signal confirm
    signal cancel

    onConfirm: {
        if (acceptFunction instanceof Function) {
            if (optionsAreAvailable) {
                acceptFunction(internal.getCheckedOptions())
            } else {
                acceptFunction()
            }
        }
        close()
    }
    onCancel: close()

    onClosed: destroy()

    Component {
        id: labelDelegate

        QQC2.Label {

            text: itemData ? itemData.title : ""
            wrapMode: Text.WordWrap
            Suru.textLevel: Suru.HeadingThree
        }
    }

    Component {
        id: checkboxDelegate

        CustomizedCheckBoxDelegate {
            id: checkBox

            readonly property string itemId: itemData ? itemData.id : ""

            text: itemData ? itemData.title : ""
            checked: true
        }
    }

    Flickable {
        clip: true
        anchors.fill: parent
        contentHeight: contentColumn.height + units.gu(5)
        boundsBehavior: Flickable.StopAtBounds

        ColumnLayout {
            id: contentColumn
            
            property real horizontalMargin: units.gu(2)

            spacing: Suru.units.gu(3)
            anchors {
                top: parent.top
                topMargin: Suru.units.gu(2)
                left: parent.left
                right: parent.right
            }
            
            QQC2.Label {
                id: confirmationLabel
                Layout.fillWidth: true
                Layout.leftMargin: contentColumn.horizontalMargin
                Layout.rightMargin: contentColumn.horizontalMargin

                wrapMode: Text.Wrap
            }

            RoundedItemDelegate {
                Layout.fillWidth: true
                Layout.leftMargin: contentColumn.horizontalMargin
                Layout.rightMargin: contentColumn.horizontalMargin

                text: i18n.tr("Select from options")
                icon {
                    name: "down"
                    width: units.gu(2)
                    height: units.gu(2)
                }
                visible: confirmationDialog.optionsAreAvailable
                onClicked: {
                    visible = false
                    optionsColumnLayout.visible = !optionsColumnLayout.visible
                }
            }

            ColumnLayout {
                id: optionsColumnLayout

                Layout.fillWidth: true
                Layout.leftMargin: contentColumn.horizontalMargin
                Layout.rightMargin: contentColumn.horizontalMargin
                spacing: 0
                visible: false

                Repeater {
                    model: confirmationDialog.optionsList
                    delegate: Loader {
                        property var itemData: modelData
                        property var itemControl: item

                        Layout.fillWidth: true
                        Layout.margins: modelData.type == "label" ? units.gu(1) : 0
                        asynchronous: true
                        sourceComponent: {
                            switch (modelData.type) {
                                case "label":
                                    return labelDelegate
                                case "checkbox":
                                    return checkboxDelegate
                            }

                            return labelDelegate
                        }
                    }
                }
                
            }

            ColumnLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true
                Layout.leftMargin: contentColumn.horizontalMargin
                Layout.rightMargin: contentColumn.horizontalMargin

                Button {
                    id: okButton
                    objectName: "okButton"

                    Layout.fillWidth: true
                    text: confirmationDialog.confirmButtonText
                    color: confirmationDialog.confirmIsPositive ? theme.palette.normal.positive
                                            : theme.palette.normal.negative

                    onClicked: {
                        confirmationDialog.confirm();
                    }
                }

                Button {
                    objectName: "cancelButton"

                    Layout.fillWidth: true

                    text: confirmationDialog.cancelButtonText
                    onClicked: {
                        confirmationDialog.cancel();
                    }
                }
            }
        }
    }

    QtObject {
        id: internal

        function getCheckedOptions() {
            let result = []
            for (let i = 0; i < optionsColumnLayout.visibleChildren.length; i++) {
                let item = optionsColumnLayout.visibleChildren[i]
                if (item.hasOwnProperty("itemControl")) {
                    item = item.itemControl
                    if (item.hasOwnProperty("checked") && item.hasOwnProperty("itemId")
                                && item.checked) {
                        result.push(item.itemId)
                    }
                }
            }
            return result
        }
    }
}
