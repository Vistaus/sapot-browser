/*
 * Copyright 2021 UBports Foundation
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Controls.Suru 2.2
import webbrowsercommon.private 0.1

BaseContentDialog {
    id: contentExportDialog

    objectName: "contentExportDialog"

    property alias path: exportPeerPicker.path
    property alias contentType: exportPeerPicker.contentType
    property string mimeType
    property string downloadUrl
    property string fileName

    signal preview(string url)

    function openDialog(_downloadPath, _contentType, _mimeType, _downloadURL, _fileName){
        path = _downloadPath
        contentType = _contentType
        mimeType = _mimeType
        downloadUrl = _downloadURL
        fileName = _fileName
        open()
    }

    headerTitle: i18n.tr("Open with")
    headerSubtitle: i18n.tr("File name: %1").arg(contentExportDialog.fileName)

    trailingActionBar.actions: [
        Action {
            iconName: "external-link"
            text: i18n.tr("Open link in browser")
            visible: (contentExportDialog.downloadUrl !== "") && (contentExportDialog.contentType !== ContentType.Unknown)
            onTriggered: {
                contentExportDialog.close()
                preview((contentExportDialog.mimeType === "application/pdf") ? UrlUtils.getPdfViewerExtensionUrlPrefix() + contentExportDialog.downloadUrl : contentExportDialog.downloadUrl);
            }
        },
        Action {
            iconName: "document-open"
            text: i18n.tr("Open file in browser")
            visible: (contentExportDialog.contentType !== ContentType.Unknown)
            onTriggered: {
                contentExportDialog.close()
                preview((contentExportDialog.mimeType === "application/pdf") ? UrlUtils.getPdfViewerExtensionUrlPrefix() + "file://%1".arg(contentExportDialog.path) : contentExportDialog.path);
            }
        }
    ]

    ContentPeerPicker {
        id: exportPeerPicker

        property string path

        focus: visible
        handler: ContentHandler.Destination
        showTitle: false

        onPeerSelected: {
            var transfer = peer.request()
            if (transfer.state === ContentTransfer.InProgress) {
                transfer.items = [contentItemComponent.createObject(contentExportDialog, {"url": path})]
                transfer.state = ContentTransfer.Charged
            }
            contentExportDialog.close()
        }
        onCancelPressed: contentExportDialog.close()
        Keys.onEscapePressed: contentExportDialog.close()
    }

    Component {
        id: contentItemComponent
        ContentItem {}
    }
}
