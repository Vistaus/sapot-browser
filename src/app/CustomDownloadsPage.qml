import QtQuick 2.12
import QtWebEngine 1.10
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3
import webbrowsercommon.private 0.1
import Morph.Web 0.1

import "MimeTypeMapper.js" as MimeTypeMapper
import "FileUtils.js" as FileUtils
import "." as Common

BasePage {
    id: downloadsPage

    // We can get file picking requests either via content-hub (activeTransfer)
    // Or via the internal oxide file picker (internalFilePicker) in the case
    // where the user wishes to upload a file from their previous downloads.
    property var activeTransfer
    property var internalFilePicker

    property bool selectMode
    property bool pickingMode
    property bool multiSelect
    property var mimetypeFilter: RegExp()
    property bool incognito: false

    signal done()

    title: i18n.tr("Downloads")
    allowRightOverflow: false
    headerRightActions: [ deleteAction, selectAllAction, cancelAction, selectModeAction, confirmAction ]

    Component.onDestruction: {
        if (selectMode) {
            selectMode = false
        } else {
            if (activeTransfer) {
                activeTransfer.state = ContentTransfer.Aborted
            }
            if (internalFilePicker) {
                internalFilePicker.reject()
            }
            if (activeTransfer || internalFilePicker) {
                done()
            }
        }
    }

    BaseAction {
        id: confirmAction

        text: i18n.tr("Confirm selection")
        tooltipText: i18n.tr("Confirm selected item")
        iconName: "tick"
        visible: pickingMode
        enabled: downloadsListView.ViewItems.selectedIndices.length > 0

        onTrigger: {
            var results = [];
            var selectedDownload, i;
            if (internalFilePicker) {
                for (i = 0; i < downloadsListView.ViewItems.selectedIndices.length; i++) {
                    selectedDownload = downloadsListView.model.get(downloadsListView.ViewItems.selectedIndices[i]);
                    results.push(selectedDownload.path);
                }
                internalFilePicker.accept(results);
            } else {
                for (i = 0; i < downloadsListView.ViewItems.selectedIndices.length; i++) {
                    selectedDownload = downloadsListView.model.get(downloadsListView.ViewItems.selectedIndices[i]);
                    results.push(resultComponent.createObject(downloadsPage, {"url": "file://" + selectedDownload.path}));
                }
                activeTransfer.items = results;
                activeTransfer.state = ContentTransfer.Charged;
            }
            downloadsPage.done()
        }
    }
    
    BaseAction {
        id: cancelAction

        text: i18n.tr("Exit mode")
        tooltipText: i18n.tr("Exit selection mode")
        iconName: "cancel"
        visible: downloadsPage.selectMode

        onTrigger: downloadsPage.selectMode = false
    }

    BaseAction {
        id: deleteAction

        text: i18n.tr("Delete items")
        tooltipText: i18n.tr("Delete selected items")
        iconName: "delete"
        visible: downloadsPage.selectMode && downloadsListView.ViewItems.selectedIndices.length > 0

        onTrigger: {
            var toDelete = []
            for (var i = 0; i < downloadsListView.ViewItems.selectedIndices.length; i++) {
                var selectedDownload = downloadsListView.model.get(downloadsListView.ViewItems.selectedIndices[i])
                toDelete.push(selectedDownload.path)
            }
            for (var i = 0; i < toDelete.length; i++) {
                DownloadsModel.deleteDownload(toDelete[i])
            }
            downloadsListView.ViewItems.selectedIndices = []
            downloadsPage.selectMode = false
        }
    }

    BaseAction {
        id: selectModeAction

        text: i18n.tr("Selection mode")
        tooltipText: i18n.tr("Enter selection mode")
        iconName: "edit"
        visible: !downloadsPage.selectMode && !downloadsPage.pickingMode
        enabled: downloadsListView.count > 0

        onTrigger: {
            selectMode = true
            multiSelect = true
        }
    }

    BaseAction {
        id: selectAllAction

        text: downloadsListView.allItemsSelected ? i18n.tr("Deselect all") : i18n.tr("Select all")
        tooltipText: downloadsListView.allItemsSelected ? i18n.tr("Deselect all items") : i18n.tr("Select all items")
        iconName: downloadsListView.allItemsSelected ? "select-none" : "select"
        visible: downloadsPage.selectMode

        onTrigger: {
            if (downloadsListView.allItemsSelected) {
                downloadsListView.ViewItems.selectedIndices = [];
            } else {
                var indices = [];
                for (var i = 0; i < downloadsListView.count; ++i) {
                    indices.push(i);
                }
                downloadsListView.ViewItems.selectedIndices = indices;
            }
        }
    }

    Loader {
        id: thumbnailLoader
        source: "Thumbnailer.qml"
    }

    Component {
        id: resultComponent
        ContentItem {}
    }

    RowLayout {
        id: downloadPathLabel

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        Label {
            Layout.alignment: Qt.AlignVCenter
            Layout.margins: units.gu(1)
            Layout.leftMargin: units.gu(2)
            Layout.rightMargin: Layout.leftMargin
            color: theme.palette.normal.backgroundSecondaryText
            text: i18n.tr("%1 : %2").arg("Location: ").arg(SharedWebContext.sharedContext.downloadPath.replace('/home/phablet', '~'))
        }
    }

    Common.BaseListView {
        id: downloadsListView

        property bool allItemsSelected: ViewItems.selectedIndices.length === count
        clip: true
        pageHeader: downloadsPage.pageManager ? downloadsPage.pageManager.header : downloadsPage.header

        anchors {
            top: downloadPathLabel.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        model: SortFilterModel {
            model: SortFilterModel {
                model: DownloadsModel
                filter {
                    property: "incognito"
                    pattern: RegExp(downloadsPage.incognito ? "" : "^false$")
                }
            }
            filter {
                id: downloadModelFilter
                property: "mimetype"
                pattern: downloadsPage.mimetypeFilter
            }
        }

        property int selectedIndex: -1
        ViewItems.selectMode: downloadsPage.selectMode || downloadsPage.pickingMode
        ViewItems.onSelectedIndicesChanged: {
            if (downloadsPage.multiSelect) {
                return
            }
            // Enforce single selection mode to work around
            // the lack of such a feature in the UITK.
            if (ViewItems.selectedIndices.length > 1 && selectedIndex != -1) {
                var selection = ViewItems.selectedIndices
                selection.splice(selection.indexOf(selectedIndex), 1)
                selectedIndex = selection[0]
                ViewItems.selectedIndices = selection
                return
            }
            if (ViewItems.selectedIndices.length > 0) {
                selectedIndex = ViewItems.selectedIndices[0]
            } else {
                selectedIndex = -1
            }
        }

        delegate: DownloadDelegate {
            id: downloadDelegate
                         
            download: ActiveDownloadsSingleton.currentDownloads[model.downloadId]
            downloadId: model.downloadId
            title: FileUtils.getFilename(model.path)
            url: model.url
            image: model.complete && thumbnailLoader.status == Loader.Ready 
                                  && (model.mimetype.indexOf("image") === 0 
                                      || model.mimetype.indexOf("video") === 0)
                                  ? "image://thumbnailer/file://" + model.path : ""
            icon: MimeDatabase.iconForMimetype(model.mimetype)
            incomplete: !model.complete
            visible: !(selectMode && incomplete)
            errorMessage: model.error
            paused: download ? download.isPaused : false
            incognito: model.incognito

            function getDisplayPath(path)
            {
               if (path.substring(0,14) === "/home/phablet/")
               {
                  path = "~/" + model.path.substring(14);
               }

               if (path.substring(0, subtitle.length) === subtitle)
               {
                 path = "." + path.substring(subtitle.length);
               }

               return path;
            }

            onClicked: {
                if (model.complete && !selectMode) {
                    contentExportLoader.item.openDialog(model.path, MimeTypeMapper.mimeTypeToContentType(model.mimetype), model.mimetype, model.url, title)
                } else {
                    if (download) {
                        if (paused) {
                            download.resume()
                        } else {
                            download.pause()
                        }
                    }
                }
            }

            onPressAndHold: {
                if (downloadsPage.selectMode || downloadsPage.pickingMode) {
                    return
                }
                downloadsPage.selectMode = true
                downloadsPage.multiSelect = true
                if (downloadsPage.selectMode) {
                    downloadsListView.ViewItems.selectedIndices = [index]
                }
            }

            onRemoved: {
                if (model.complete) {
                    DownloadsModel.deleteDownload(model.path)
                }
            }

            onCancelled: {
                DownloadsModel.cancelDownload(model.downloadId)
            }
        }

        Keys.onEscapePressed: {
            if (selectMode) {
                selectMode = false
            } else {
                event.accepted = false
            }
        }
        Keys.onDeletePressed: {
            if (!selectMode && !pickingMode) {
                currentItem.removed()
            }
        }
    }

    Scrollbar {
        flickableItem: downloadsListView
    }

    Label {
        id: emptyLabel
        anchors.centerIn: parent
        visible: downloadsListView.count == 0
        wrapMode: Text.Wrap
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: i18n.tr("No downloads available")
    }

    Component {
        id: contentItemComponent
        ContentItem {}
    }
}
