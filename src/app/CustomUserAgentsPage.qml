/*
 * Copyright 2020 UBports Foundation
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.6
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3
import webbrowsercommon.private 0.1
import "." as Common

BasePage {
    id: customUserAgentsPage

    property bool selectMode
    signal done()
    signal reload()

    title: i18n.tr("Custom User Agents")
    allowRightOverflow: false
    showBackButton: !selectMode
    headerLeftActions: [ cancelAction ]
    headerRightActions: [ addAction, deleteAction, selectAllAction, selectModeAction ]

    function reloadData() {
        // WORKAROUND: Yup for some reason this is necessary
        // Previous approach was to reload the whole page!
        // Without this, the newly added domain will have the same name as the first one
        // Until you sort or reload the model
        customUserAgentsListView.model = null
        customUserAgentsListView.model = sortedUserAgentsModel
    }

    BaseAction {
        id: cancelAction

        text: i18n.tr("Exit mode")
        tooltipText: i18n.tr("Exit selection mode")
        iconName: "cancel"
        visible: customUserAgentsPage.selectMode

        onTrigger: customUserAgentsPage.selectMode = false
    }

    BaseAction {
        id: selectAllAction

        text: customUserAgentsListView.allItemsSelected ? i18n.tr("Deselect all") : i18n.tr("Select all")
        tooltipText: customUserAgentsListView.allItemsSelected ? i18n.tr("Deselect all items") : i18n.tr("Select all items")
        iconName: customUserAgentsListView.allItemsSelected ? "select-none" : "select"
        visible: customUserAgentsPage.selectMode

        onTrigger: {
            if (customUserAgentsListView.allItemsSelected) {
                customUserAgentsListView.ViewItems.selectedIndices = []
            } else {
                var indices = []
                for (var i = 0; i < customUserAgentsListView.count; ++i) {
                    indices.push(i)
                }
                customUserAgentsListView.ViewItems.selectedIndices = indices
            }
        }
    }

    BaseAction {
        id: deleteAction

        text: i18n.tr("Delete items")
        tooltipText: i18n.tr("Delete selected items")
        iconName: "delete"
        visible: customUserAgentsPage.selectMode && customUserAgentsListView.ViewItems.selectedIndices.length > 0

        onTrigger: {
            var toDelete = []
            for (var index = 0; index < customUserAgentsListView.ViewItems.selectedIndices.length; index++) {
                var selectedUserAgent = customUserAgentsListView.model.get(customUserAgentsListView.ViewItems.selectedIndices[index])
                toDelete.push(selectedUserAgent.id)
            }
            for (var i = 0; i < toDelete.length; i++) {
                DomainSettingsModel.removeUserAgentIdFromAllDomains(toDelete[i])
                UserAgentsModel.removeEntry(toDelete[i])
            }
            customUserAgentsListView.ViewItems.selectedIndices = []
            customUserAgentsPage.selectMode = false
        }
    }

    BaseAction {
        id: selectModeAction

        text: i18n.tr("Selection mode")
        tooltipText: i18n.tr("Enter selection mode")
        iconName: "edit"
        visible: !customUserAgentsPage.selectMode && customUserAgentsListView.count > 0

        onTrigger: customUserAgentsPage.selectMode = true
    }

    BaseAction {
        id: addAction

        text: i18n.tr("Add entry")
        tooltipText: i18n.tr("Add a new domain")
        iconName: "add"
        visible: !customUserAgentsPage.selectMode

        onTrigger: {
            var addDialog = promptDialogComponent.createObject(customUserAgentsPage)
            addDialog.openForNew()
        }
    }

    Component {
        id: promptDialogComponent

        Common.DialogWithContents {
            id: promptDialog

            property bool editMode: false
            property string itemId: ""
            property string previousUserAgentName: ""
            property alias userAgentName: editUserAgentName.text
            property alias userAgentString: editUserAgentString.text

            readonly property bool userAgentNameAlreadyTaken: (userAgentName !== previousUserAgentName) && UserAgentsModel.contains(userAgentName)

            function openForNew() {
                editMode = false
                openDialog()
            }

            function openForEdit(_itemId) {
                editMode = true
                itemId = _itemId
                openDialog()
            }

            function openDialog() {
                if (customUserAgentsPage.displayedInFullWidthPanel) {
                    openBottom();
                } else {
                    openNormal();
                }
            }

            parent: customUserAgentsPage.pageManager
            title: editMode ? i18n.tr("Edit User Agent") : i18n.tr("New User Agent")
            anchorToKeyboard: true

            onOpened: editUserAgentName.forceActiveFocus()

            function accept(userAgentName, userAgentString) {
                if (userAgentName !== "" && userAgentString !== "") {
                    if (editMode) {
                        UserAgentsModel.setUserAgentString(itemId, userAgentString);
                        UserAgentsModel.setUserAgentName(itemId, userAgentName);
                    } else {
                        UserAgentsModel.insertEntry(userAgentName, userAgentString);
                        customUserAgentsPage.reloadData();
                    }

                    promptDialog.close();
                }
            }
            
            QQC2.Label {
                visible: promptDialog.userAgentNameAlreadyTaken
                text: i18n.tr("This user agent name is already taken")
                color: theme.palette.normal.negative
            }

            QQC2.TextField {
                id: editUserAgentName

                Layout.fillWidth: true
                placeholderText: i18n.tr("Add the name for the user agent")
                inputMethodHints: Qt.ImhNoPredictiveText
            }

            QQC2.TextArea {
                id: editUserAgentString

                Layout.fillWidth: true
                placeholderText: i18n.tr("Enter user agent string...")
                inputMethodHints: Qt.ImhNoPredictiveText
                wrapMode: TextEdit.WordWrap
            }

            ColumnLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true

                Button {
                    id: saveButton

                    Layout.fillWidth: true
                    text: promptDialog.editMode ? i18n.tr("Save") : i18n.tr("Add")
                    color: theme.palette.normal.positive
                    enabled: promptDialog.userAgentName !== "" && promptDialog.userAgentString !== "" && ! userAgentNameAlreadyTaken

                    onClicked: promptDialog.accept(promptDialog.userAgentName, promptDialog.userAgentString)
                }

                Button {
                    Layout.fillWidth: true

                    text: i18n.tr("Cancel")
                    onClicked: promptDialog.close();
                }
            }
        }
    }

    SortFilterModel {
        id: sortedUserAgentsModel
        model: UserAgentsModel
        sort.property: "name"
        sort.order: Qt.AscendingOrder
    }

    ScrollView {
        id: mainScrollView

        anchors.fill: parent

        Common.BaseListView {
            id: customUserAgentsListView

            property bool allItemsSelected: ViewItems.selectedIndices.length === count

            anchors.fill: parent
            focus: true
            pageHeader: customUserAgentsPage.pageManager ? customUserAgentsPage.pageManager.header : customUserAgentsPage.header
            model: sortedUserAgentsModel

            ViewItems.selectMode: customUserAgentsPage.selectMode

            delegate: ListItem {
                id: item

                Label {
                    id: userAgentLabel
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: units.gu(1)
                    text: model.name
                }

                leadingActions: deleteActionList

                ListItemActions {
                    id: deleteActionList
                    actions: [
                        Action {
                            objectName: "leadingAction.delete"
                            iconName: "delete"
                            enabled: true
                            onTriggered: {
                                DomainSettingsModel.removeUserAgentIdFromAllDomains(model.id)
                                UserAgentsModel.removeEntry(model.id)
                            }
                        }
                    ]
                }

                trailingActions: trailingActionList

                ListItemActions {
                    id: trailingActionList
                    actions: [
                        Action {
                            objectName: "trailingActionList.edit"
                            iconName: "edit"
                            enabled: true
                            onTriggered: {
                                var editDialog = promptDialogComponent.createObject(customUserAgentsPage)
                                editDialog.previousUserAgentName = model.name;
                                editDialog.userAgentName = model.name;
                                editDialog.userAgentString = model.userAgentString;
                                editDialog.openForEdit(model.id)
                            }
                        }
                    ]
                }
            }
        }
    }

    Label {
        id: emptyLabel
        anchors.centerIn: parent
        visible: customUserAgentsListView.count == 0
        wrapMode: Text.Wrap
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: i18n.tr("No custom user agents available")
    }
}
