import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Suru 2.2
import "webbrowser"

RoundedItemDelegate {
    id: deviceSelectorDelegate

    property alias label: mainLabel
    property alias helpText: helpButton.text

    // Device Selector
    readonly property alias devicesCount: deviceSelector.devicesCount
    property alias isAudio: deviceSelector.isAudio
    property alias defaultDevice: deviceSelector.defaultDevice

    signal deviceSelected(string id)

    transparentBackground: true
    interactive: false

    contentItem: ColumnLayout {
        RowLayout {
            QQC2.Label {
                id: mainLabel

                Layout.fillWidth: true
                text: deviceSelectorDelegate.text
                wrapMode: Text.WordWrap
                elide: Text.ElideRight
                maximumLineCount: 2
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }

            SettingsHelpButton {
                id: helpButton
            }
        }

        SettingsDeviceSelector {
            id: deviceSelector

            Layout.fillWidth: true

            visible: devicesCount > 0
            enabled: devicesCount > 1

            onDeviceSelected: deviceSelected(id)
        }
    }
}
