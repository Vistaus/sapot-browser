import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Controls.Suru 2.2

QQC2.Label {
    wrapMode: Text.Wrap
    verticalAlignment: Text.AlignVCenter
    Suru.textLevel: Suru.Caption
}
