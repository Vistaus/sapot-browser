/*
 * Copyright 2020 UBports Foundation
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Qt.labs.settings 1.0
import QtQuick.Controls 2.2 as QQC2
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3
import webbrowsercommon.private 0.1
import "." as Common

BasePage {
    id: domainPermissionsPage

    function setDomainAsCurrentItem(domain) {
        for (var index = 0; index < domainPermissionsListView.count; index++) {
            var domainSetting = domainPermissionsListView.model.get(index);
            if (domainSetting.domain === domain) {
                domainPermissionsListView.currentIndex = index;
                return;
            }
        }
    }

    property QtObject domainPermissionsObject
    property bool selectMode
    property bool sortByLastRequested: true

    title: i18n.tr("Domain Blacklist/Whitelist")
    allowRightOverflow: false
    showBackButton: !selectMode
    headerLeftActions: [ cancelAction ]
    headerRightActions: [ addAction, sortAction, deleteAction, selectAllAction, selectModeAction ]

    function reloadData(selectedDomain) {
        // WORKAROUND: Yup for some reason this is necessary
        // Previous approach was to reload the whole page!
        // Without this, the newly added domain will have the same name as the first one
        // Until you sort or reload the model
        domainPermissionsListView.model = null
        domainPermissionsListView.model = sortedModel

        if (selectedDomain) {
          setDomainAsCurrentItem(selectedDomain)
        }
    }

    BaseAction {
        id: cancelAction

        text: i18n.tr("Exit mode")
        tooltipText: i18n.tr("Exit selection mode")
        iconName: "cancel"
        visible: domainPermissionsPage.selectMode

        onTrigger: domainPermissionsPage.selectMode = false
    }

    BaseAction {
        id: selectAllAction

        text: domainPermissionsListView.allItemsSelected ? i18n.tr("Deselect all") : i18n.tr("Select all")
        tooltipText: domainPermissionsListView.allItemsSelected ? i18n.tr("Deselect all items") : i18n.tr("Select all items")
        iconName: domainPermissionsListView.allItemsSelected ? "select-none" : "select"
        visible: domainPermissionsPage.selectMode

        onTrigger: {
            if (domainPermissionsListView.allItemsSelected) {
                domainPermissionsListView.ViewItems.selectedIndices = []
            } else {
                var indices = []
                for (var i = 0; i < domainPermissionsListView.count; ++i) {
                    indices.push(i)
                }
                domainPermissionsListView.ViewItems.selectedIndices = indices
            }
        }
    }

    BaseAction {
        id: deleteAction

        text: i18n.tr("Delete items")
        tooltipText: i18n.tr("Delete selected items")
        iconName: "delete"
        visible: domainPermissionsPage.selectMode && domainPermissionsListView.ViewItems.selectedIndices.length > 0

        onTrigger: {
            var toDelete = []
            for (var index = 0; index < domainPermissionsListView.ViewItems.selectedIndices.length; index++) {
                var selectedDomainSetting = domainPermissionsListView.model.get(domainPermissionsListView.ViewItems.selectedIndices[index])
                toDelete.push(selectedDomainSetting.domain)
            }
            for (var i = 0; i < toDelete.length; i++) {
                DomainPermissionsModel.removeEntry(toDelete[i])
            }
            domainPermissionsListView.ViewItems.selectedIndices = []
            domainPermissionsPage.selectMode = false
        }
    }

    BaseAction {
        id: selectModeAction

        text: i18n.tr("Selection mode")
        tooltipText: i18n.tr("Enter selection mode")
        iconName: "edit"
        visible: !domainPermissionsPage.selectMode && domainPermissionsListView.count > 0

        onTrigger: domainPermissionsPage.selectMode = true
    }

    BaseAction {
        id: addAction

        text: i18n.tr("Add entry")
        tooltipText: i18n.tr("Add a new domain")
        iconName: "add"
        visible: !domainPermissionsPage.selectMode

        onTrigger: {
            var addDialog = promptDialogComponent.createObject(domainPermissionsPage)
            if (domainPermissionsPage.displayedInFullWidthPanel) {
                addDialog.openBottom();
            } else {
                addDialog.openNormal();
            }
        }
    }

    BaseAction {
        id: sortAction

        text: i18n.tr("Sort")
        tooltipText: domainPermissionsPage.sortByLastRequested ? i18n.tr("Sort alphabetically") : i18n.tr("Sort by last requested/visited")
        iconName: domainPermissionsPage.sortByLastRequested ? "clock" : "indicator-keyboard-Az"
        visible: !domainPermissionsPage.selectMode

        onTrigger: domainPermissionsPage.sortByLastRequested = !domainPermissionsPage.sortByLastRequested
    }

    Component {
        id: promptDialogComponent

        Common.DialogWithContents {
            id: promptDialog

            parent: domainPermissionsPage.pageManager
            title: i18n.tr("Add domain")
            anchorToKeyboard: true

            onOpened: domainNameTextField.forceActiveFocus()

            function accept(domainName) {
                if (domainName !== "") {
                    var domain = DomainPermissionsModel.getDomainWithoutSubdomain(UrlUtils.extractHost(domainName));
                    if (DomainPermissionsModel.contains(domain)) {
                        domainPermissionsPage.setDomainAsCurrentItem(domain);
                    }
                    else {
                        DomainPermissionsModel.insertEntry(domain, false);
                        domainPermissionsPage.reloadData(domain);
                    }
                    promptDialog.close();
                }
            }
            
            QQC2.Label {
                Layout.fillWidth: true

                text: i18n.tr("Enter the name of the domain, e.g. example.com (subdomains will be removed).")
                verticalAlignment: Label.AlignVCenter
                horizontalAlignment: Label.AlignHCenter
                wrapMode: Text.WordWrap
            }

            QQC2.TextField {
                id: domainNameTextField

                Layout.fillWidth: true
                inputMethodHints: Qt.ImhUrlCharactersOnly | Qt.ImhNoPredictiveText
                placeholderText: i18n.tr("Enter domain name")
                onAccepted: {
                    Qt.inputMethod.commit()
                    if (saveButton.enabled) {
                        promptDialog.accept(text)
                    }
                }
            }

            ColumnLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true

                Button {
                    id: saveButton

                    Layout.fillWidth: true
                    text: i18n.tr("Add")
                    color: theme.palette.normal.positive
                    enabled: UrlUtils.looksLikeAUrl(domainNameTextField.text.trim())

                    onClicked: promptDialog.accept(domainNameTextField.text)
                }

                Button {
                    Layout.fillWidth: true

                    text: i18n.tr("Cancel")
                    onClicked: promptDialog.close();
                }
            }
        }
    }

    SortFilterModel {
        id: sortedModel

        model: DomainPermissionsModel
        sort.order: sortByLastRequested ? Qt.DescendingOrder : Qt.AscendingOrder
        sort.property: sortByLastRequested ? "lastRequested" : "domain"
    }

    ScrollView {
        id: mainScrollView

        anchors.fill: parent

        Common.BaseListView {
            id: domainPermissionsListView

            property bool allItemsSelected: ViewItems.selectedIndices.length === count

            anchors.fill: parent
            pageHeader: domainPermissionsPage.pageManager ? domainPermissionsPage.pageManager.header : domainPermissionsPage.header
            focus: true
            model:  sortedModel

            ViewItems.selectMode: selectMode

            delegate: ListItem {
                id: item
                readonly property bool isCurrentItem: item.ListView.isCurrentItem
                readonly property string domain: model.domain
                height: isCurrentItem ? layout.height : units.gu(5)
                color: isCurrentItem ? ((theme.palette.selected.background.hslLightness > 0.5) ? Qt.darker(theme.palette.selected.background, 1.05)
                                                                                               : Qt.lighter(theme.palette.selected.background, 1.5))
                                     : theme.palette.normal.background

                MouseArea {
                    anchors.fill: parent
                    onClicked: domainPermissionsListView.currentIndex = index
                }

                SlotsLayout {
                    id: layout
                    width: parent.width

                    mainSlot:

                        Column {

                        spacing: units.gu(2)

                        Row {
                            spacing: units.gu(1.5)
                            height: units.gu(1)
                            width: parent.width

                            Icon {
                                visible: (model.permission === DomainPermissionsModel.NotSet)
                                name: "dialog-question-symbolic"
                                height: units.gu(2)
                                width: height
                            }

                            IconWithColorOverlay {
                                overlayColor: theme.palette.normal.positive
                                visible: (model.permission === DomainPermissionsModel.Whitelisted)
                                name: "ok"
                                height: units.gu(2)
                                width: height
                            }

                            IconWithColorOverlay {
                                overlayColor: theme.palette.normal.negative
                                visible: (model.permission === DomainPermissionsModel.Blocked)
                                name: "cancel"
                                height: units.gu(2)
                                width: height
                            }

                            Label {
                                text: model.domain
                                font.bold: item.ListView.isCurrentItem
                                color: theme.palette.normal.foregroundText
                            }

                            Label {
                                visible: (model.requestedByDomain !== "")
                                text:  "(→%1)".arg(model.requestedByDomain)
                                color: theme.palette.normal.foregroundText
                            }

                        }

                        ColumnLayout {
                            visible: item.ListView.isCurrentItem
                            CustomizedRadioButton {
                                checked: (model.permission === DomainPermissionsModel.NotSet)
                                text: i18n.tr("Not Set")
                                color: theme.palette.normal.foregroundText
                                onCheckedChanged: {
                                    if (checked) {
                                        DomainPermissionsModel.setPermission(model.domain, DomainPermissionsModel.NotSet, false)
                                    }
                                }
                            }
                            CustomizedRadioButton {
                                checked: (model.permission === DomainPermissionsModel.Blocked)
                                text: i18n.tr("Never allow access")
                                color: theme.palette.normal.backgroundText
                                onCheckedChanged: {
                                    if (checked) {
                                        DomainPermissionsModel.setPermission(model.domain, DomainPermissionsModel.Blocked, false)
                                    }
                                }
                            }

                            CustomizedRadioButton {
                                checked: (model.permission === DomainPermissionsModel.Whitelisted)
                                text: i18n.tr("Always allow access")
                                color: theme.palette.normal.backgroundText
                                onCheckedChanged: {
                                    if (checked) {
                                        DomainPermissionsModel.setPermission(model.domain, DomainPermissionsModel.Whitelisted, false)
                                    }
                                }
                            }
                        }
                    }
                }

                leadingActions: deleteActionList

                ListItemActions {
                    id: deleteActionList
                    actions: [
                        Action {
                            objectName: "leadingAction.delete"
                            iconName: "delete"
                            enabled: true
                            onTriggered: DomainPermissionsModel.removeEntry(model.domain)
                        }
                    ]
                }
            }
        }
    }

    Label {
        id: emptyLabel
        anchors.centerIn: parent
        visible: domainPermissionsListView.count == 0
        wrapMode: Text.Wrap
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: i18n.tr("No sites have been granted special permissions")
    }
}
