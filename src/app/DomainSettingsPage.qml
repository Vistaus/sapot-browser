/*
 * Copyright 2020 UBports Foundation
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.6
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3
import webbrowsercommon.private 0.1
import "." as Common

BasePage {
    id: domainSettingsItem

    property QtObject domainSettingsObject
    property bool selectMode

    signal done()
    signal reload(string selectedDomain)

    title: i18n.tr("Domain-specific settings")
    allowRightOverflow: false
    showBackButton: !selectMode
    headerLeftActions: [ cancelAction ]
    headerRightActions: [ addAction, deleteAction, selectAllAction, selectModeAction ]

    function setDomainAsCurrentItem(domain) {
        for (var index = 0; index < domainSettingsListView.count; index++) {
            var domainSetting = domainSettingsListView.model.get(index);
            if (domainSetting.domain === domain) {
                domainSettingsListView.currentIndex = index;
                return;
            }
        }
    }

    function reloadData(selectedDomain) {
        // WORKAROUND: Yup for some reason this is necessary
        // Previous approach was to reload the whole page!
        // Without this, the newly added domain will have the same name as the first one
        // Until you sort or reload the model
        domainSettingsListView.model = null
        domainSettingsListView.model = sortedModel

        if (selectedDomain) {
          setDomainAsCurrentItem(selectedDomain)
        }
    }

    BaseAction {
        id: cancelAction

        text: i18n.tr("Exit mode")
        tooltipText: i18n.tr("Exit selection mode")
        iconName: "cancel"
        visible: domainSettingsItem.selectMode

        onTrigger: domainSettingsItem.selectMode = false
    }

    BaseAction {
        id: selectAllAction

        text: domainSettingsListView.allItemsSelected ? i18n.tr("Deselect all") : i18n.tr("Select all")
        tooltipText: domainSettingsListView.allItemsSelected ? i18n.tr("Deselect all items") : i18n.tr("Select all items")
        iconName: domainSettingsListView.allItemsSelected ? "select-none" : "select"
        visible: domainSettingsItem.selectMode

        onTrigger: {
            if (domainSettingsListView.allItemsSelected) {
                domainSettingsListView.ViewItems.selectedIndices = []
            } else {
                var indices = []
                for (var i = 0; i < domainSettingsListView.count; ++i) {
                    indices.push(i)
                }
                domainSettingsListView.ViewItems.selectedIndices = indices
            }
        }
    }

    BaseAction {
        id: deleteAction

        text: i18n.tr("Delete items")
        tooltipText: i18n.tr("Delete selected items")
        iconName: "delete"
        visible: domainSettingsItem.selectMode && domainSettingsListView.ViewItems.selectedIndices.length > 0

        onTrigger: {
            var toDelete = []
            for (var index = 0; index < domainSettingsListView.ViewItems.selectedIndices.length; index++) {
                var selectedDomainSetting = domainSettingsListView.model.get(domainSettingsListView.ViewItems.selectedIndices[index])
                toDelete.push(selectedDomainSetting.domain)
            }
            for (var i = 0; i < toDelete.length; i++) {
                DomainSettingsModel.removeEntry(toDelete[i])
            }
            domainSettingsListView.ViewItems.selectedIndices = []
            domainSettingsItem.selectMode = false
        }
    }

    BaseAction {
        id: selectModeAction

        text: i18n.tr("Selection mode")
        tooltipText: i18n.tr("Enter selection mode")
        iconName: "edit"
        visible: !domainSettingsItem.selectMode && domainSettingsListView.count > 0

        onTrigger: domainSettingsItem.selectMode = true
    }

    BaseAction {
        id: addAction

        text: i18n.tr("Add entry")
        tooltipText: i18n.tr("Add a new domain")
        iconName: "add"
        visible: !domainSettingsItem.selectMode

        onTrigger: {
            var addDialog = promptDialogComponent.createObject(domainSettingsItem)
            if (domainSettingsItem.displayedInFullWidthPanel) {
                addDialog.openBottom();
            } else {
                addDialog.openNormal();
            }
        }
    }

    Component {
        id: promptDialogComponent

        Common.DialogWithContents {
            id: promptDialog

            parent: domainSettingsItem.pageManager
            title: i18n.tr("Add domain")
            anchorToKeyboard: true

            onOpened: domainNameTextField.forceActiveFocus()

            function accept(domainName) {
                if (domainName !== "") {
                    var domain = UrlUtils.extractHost(domainName);
                    if (DomainSettingsModel.contains(domain)) {
                        domainSettingsItem.setDomainAsCurrentItem(domain);
                    }
                    else {
                        DomainSettingsModel.insertEntry(domain);
                        reloadData(domain);
                    }

                    promptDialog.close();
                }
            }
            
            QQC2.Label {
                Layout.fillWidth: true

                text: i18n.tr("Enter the name of the domain, e.g. m.example.com")
                verticalAlignment: Label.AlignVCenter
                horizontalAlignment: Label.AlignHCenter
                wrapMode: Text.WordWrap
            }

            QQC2.TextField {
                id: domainNameTextField

                Layout.fillWidth: true
                inputMethodHints: Qt.ImhUrlCharactersOnly | Qt.ImhNoPredictiveText
                placeholderText: i18n.tr("Enter domain name")
                onAccepted: {
                    Qt.inputMethod.commit()
                    if (saveButton.enabled) {
                        promptDialog.accept(text)
                    }
                }
            }

            ColumnLayout {
                spacing: units.gu(2)
                Layout.fillWidth: true

                Button {
                    id: saveButton

                    Layout.fillWidth: true
                    text: i18n.tr("Add")
                    color: theme.palette.normal.positive
                    enabled: UrlUtils.looksLikeAUrl(domainNameTextField.text.trim())

                    onClicked: promptDialog.accept(domainNameTextField.text)
                }

                Button {
                    Layout.fillWidth: true

                    text: i18n.tr("Cancel")
                    onClicked: promptDialog.close();
                }
            }
        }
    }

    DomainSettingsSortedModel {
        id: domainSettingsSortedModel

        model: DomainSettingsModel
        sortOrder: Qt.AscendingOrder
    }

    SortFilterModel {
        id: sortedModel

        model: domainSettingsSortedModel
    }

    ScrollView {
        id: mainScrollView

        anchors.fill: parent

        ListItem {
            id: useragentsMenu
            z: 3
            height: units.gu(6)
            color: theme.palette.normal.background
            ListItemLayout {
                title.text: i18n.tr("Custom User Agents")
                ProgressionSlot {}
            }

            onClicked: domainSettingsItem.pageManager.push(customUserAgentsPageComponent)
        }

        Common.BaseListView {
            id: domainSettingsListView

            property bool allItemsSelected: ViewItems.selectedIndices.length === count

            anchors.topMargin: useragentsMenu.height
            anchors.fill: parent
            focus: true
            pageHeader: domainSettingsItem.pageManager ? domainSettingsItem.pageManager.header : domainSettingsItem.header
            model:  sortedModel

            ViewItems.selectMode: selectMode

            delegate: ListItem {
                id: item
                readonly property bool isCurrentItem: item.ListView.isCurrentItem
                readonly property string domain: model.domain
                readonly property int userAgentId: model.userAgentId
                readonly property int locationPreference: model.allowLocation
                readonly property int newViewPreference: model.allowNewView
                height: isCurrentItem ? layout.height : units.gu(5)
                color: isCurrentItem ? ((theme.palette.selected.background.hslLightness > 0.5) ? Qt.darker(theme.palette.selected.background, 1.05) : Qt.lighter(theme.palette.selected.background, 1.5)) : theme.palette.normal.background

                MouseArea {
                    anchors.fill: parent
                    onClicked: domainSettingsListView.currentIndex = index
                }

                SlotsLayout {
                    id: layout
                    width: parent.width

                    mainSlot:

                        Column {

                        spacing: units.gu(3)

                        Label {
                            id: domainLabel
                            width: parent.width
                            height: units.gu(1)
                            text: model.domain
                            font.bold: item.ListView.isCurrentItem
                        }

                        Row {
                            spacing: units.gu(1.5)
                            height: units.gu(1)
                            visible: item.ListView.isCurrentItem

                            Label  {
                                width: parent.width * 0.9
                                text: i18n.tr("Allowed to launch other apps")
                                anchors.verticalCenter: parent.verticalCenter
                            }

                            CheckBox {
                                checked: model.allowCustomUrlSchemes
                                onTriggered: DomainSettingsModel.allowCustomUrlSchemes(model.domain, checked)
                                anchors.verticalCenter: parent.verticalCenter
                            }
                        }


                        Row {
                            spacing: units.gu(1.5)
                            height: units.gu(1)
                            visible: item.ListView.isCurrentItem

                            Label  {
                                width: parent.width * 0.5
                                text: i18n.tr("Access your location")
                                anchors.verticalCenter: parent.verticalCenter
                            }

                            QQC2.ComboBox {
                               model: [ i18n.tr("Ask each time"), i18n.tr("Allowed"), i18n.tr("Denied") ]
                               currentIndex: item.locationPreference
                               onCurrentIndexChanged: DomainSettingsModel.setLocationPreference(item.domain, currentIndex)
                               anchors.verticalCenter: parent.verticalCenter
                            }
                        }

                        Row {
                            spacing: units.gu(1.5)
                            height: units.gu(1)
                            visible: item.ListView.isCurrentItem

                            Label  {
                                width: parent.width * 0.5
                                text: i18n.tr("Open new view")
                                anchors.verticalCenter: parent.verticalCenter
                            }

                            QQC2.ComboBox {
                               model: [ i18n.tr("Ask each time"), i18n.tr("Allowed"), i18n.tr("Denied") ]
                               currentIndex: item.newViewPreference
                               onCurrentIndexChanged: DomainSettingsModel.setNewViewPreference(item.domain, currentIndex)
                               anchors.verticalCenter: parent.verticalCenter
                            }
                        }

                        Row {
                            spacing: units.gu(1.5)
                            height: units.gu(1)
                            visible: item.ListView.isCurrentItem

                            Label  {
                                width: parent.width * 0.9
                                text: i18n.tr("Custom user agent")
                                opacity: UserAgentsModel.count > 0 ? 1.0 : 0.5
                                anchors.verticalCenter: parent.verticalCenter
                            }

                            CheckBox {
                                id: customUserAgentCheckbox
                                enabled: UserAgentsModel.count > 0
                                checked: model.userAgentId > 0
                                onTriggered: {
                                    userAgentSelect.currentIndex = -1;

                                    if (checked) {
                                        if(UserAgentsModel.count === 1)
                                        {
                                            userAgentSelect.currentIndex = 0;
                                            DomainSettingsModel.setUserAgentId(item.domain, userAgentSelect.model.get(userAgentSelect.currentIndex).id);
                                        }
                                        else
                                        {
                                            userAgentSelect.onPressedChanged();
                                        }
                                    }
                                    else  {
                                        DomainSettingsModel.setUserAgentId(model.domain, 0);
                                    }
                                }
                                anchors.verticalCenter: parent.verticalCenter
                            }
                        }

                        QQC2.ComboBox {

                            id: userAgentSelect
                            visible: customUserAgentCheckbox.checked
                            enabled: (UserAgentsModel.count > 1)

                            model: SortFilterModel {
                                id: sortedUserAgentsModel
                                model: UserAgentsModel
                                sort.property: "name"
                                sort.order: Qt.AscendingOrder
                            }
                            
                            textRole: "name"
                            
                            function updateIndex() {
                                for (var i = 0; i < model.count; ++i) {
                                    if (item.userAgentId === model.get(i).id)
                                    {
                                        currentIndex = i;
                                    }
                                }
                            }

                            Connections {
                                target: item

                                onIsCurrentItemChanged: {
                                    if (item.isCurrentItem && (item.userAgentId > 0)) {
                                        userAgentSelect.updateIndex();
                                    }
                                }
                            }
                            
                            onActivated: DomainSettingsModel.setUserAgentId(item.domain, model.get(index).id);
                        }

                        Row {
                            spacing: units.gu(1.5)
                            height: units.gu(1)
                            visible: item.ListView.isCurrentItem

                            // within one label the check if zoom factor is set could not be properly done
                            Label  {
                                text: i18n.tr("Zoom: ") + Math.round(model.zoomFactor * 100) + "%"
                                visible: ! isNaN(model.zoomFactor)
                                anchors.verticalCenter: parent.verticalCenter
                            }
                            Label  {
                                text: i18n.tr("Zoom: ") + i18n.tr("not set")
                                visible: isNaN(model.zoomFactor)
                                anchors.verticalCenter: parent.verticalCenter
                            }
                        }
                    }
                }

                leadingActions: deleteActionList

                ListItemActions {
                    id: deleteActionList
                    actions: [
                        Action {
                            objectName: "leadingAction.delete"
                            iconName: "delete"
                            enabled: true
                            onTriggered: DomainSettingsModel.removeEntry(model.domain)
                        }
                    ]
                }
            }
        }
    }

    Label {
        id: emptyLabel
        anchors.centerIn: parent
        visible: domainSettingsListView.count == 0
        wrapMode: Text.Wrap
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        text: i18n.tr("No domain specific settings available")
    }

    Component {
        id: customUserAgentsPageComponent

        CustomUserAgentsPage{}
    }
}
