/*
 * Copyright 2013-2015 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import QtWebEngine 1.7
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5 as QQC2

Rectangle {
    id: errorSheet

    property string url
    property string errorString
    property int errorDomain
    property bool canGoBack
    property bool loading
    color: theme.palette.normal.background

    signal backToSafetyClicked()
    signal refreshClicked()

    ColumnLayout {
        spacing: units.gu(3)
        anchors {
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -units.gu(5)
            margins: units.gu(4)
        }

        ColumnLayout {
            Layout.preferredWidth: parent.width - (parent.anchors.margins * 2)
            Layout.maximumWidth: units.gu(80)
            Layout.alignment: Qt.AlignHCenter

            spacing: units.gu(3)

            Label {
                Layout.fillWidth: true
                fontSize: "x-large"
                text: (errorDomain === WebEngineView.CertificateErrorDomain) ? i18n.tr("Certificate Error") : i18n.tr("Network Error")
                color: theme.palette.normal.overlayText
            }

            Label {
                Layout.fillWidth: true
                // TRANSLATORS: %1 refers to the URL of the current page
                text: i18n.tr("It appears you are having trouble viewing: %1.").arg(url)
                wrapMode: Text.Wrap
                color: theme.palette.normal.overlayText
            }

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Error: %1".arg(errorString))
                visible: errorString !== ""
                color: theme.palette.normal.overlayText
            }

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Please check your network settings and try refreshing the page.")
                wrapMode: Text.Wrap
                visible: (errorDomain !== WebEngineView.CertificateErrorDomain)
                color: theme.palette.normal.overlayText
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.maximumWidth: units.gu(40)
            Layout.alignment: Qt.AlignHCenter

            spacing: units.gu(2)

            QQC2.Button {
                Layout.fillWidth: true
                // FIXME: Remove workaround for icon and label spacing
                text: " " + i18n.tr("Back to safety")
                icon {
                    name: "back"
                    width: units.gu(1.5)
                    height: units.gu(1.5)
                }
                visible: canGoBack && (errorDomain === WebEngineView.CertificateErrorDomain) && !errorSheet.loading
                onClicked: backToSafetyClicked()
            }

            ActivityIndicator {
                id: indicator
                Layout.alignment: Qt.AlignHCenter
                running: errorSheet.loading
                visible: running
            }

            QQC2.Button {
                Layout.fillWidth: true
                visible: !errorSheet.loading
                icon {
                    name: "reload"
                    width: units.gu(1.5)
                    height: units.gu(1.5)
                }
                // FIXME: Remove workaround for icon and label spacing
                text: " " + i18n.tr("Refresh page")
                onClicked: refreshClicked()
            }
        }
    }
}
