import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Suru 2.2

RoundedItemDelegate {
    id: navigationDelegate

    property alias label: mainLabel
    readonly property IconGroupedProperties progressionIcon: IconGroupedProperties {
        name: "go-next"
        width: units.gu(2)
        height: units.gu(2)
        color: theme.palette.normal.foregroundText
    }

    transparentBackground: true

    contentItem: RowLayout {
        spacing: units.gu(1)

        Icon {
            id: primaryIconItem

            visible: name ? true : false
            name: navigationDelegate.icon.name
            width: navigationDelegate.icon.width
            height: navigationDelegate.icon.height
            color: navigationDelegate.icon.color
        }

        QQC2.Label {
            id: mainLabel

            Layout.fillWidth: true
            text: navigationDelegate.text
            wrapMode: Text.WordWrap
            elide: Text.ElideRight
            maximumLineCount: 2
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
        }

        Icon {
            id: progressionIconItem

            visible: name ? true : false
            name: progressionIcon.name
            width: progressionIcon.width
            height: progressionIcon.height
            color: progressionIcon.color
        }
    }
}
