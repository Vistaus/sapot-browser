import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.12
import "." as Common
import "pageheader" as PageHeader

QQC2.Drawer {
    id: pageDrawer

    readonly property real nonFullMinimumWidth: units.gu(50)
    readonly property real maximumPageWidth: units.gu(140)
    readonly property bool isFullWidth: width === parent.width
    readonly property bool nonFullWidthCapable: parent.width > nonFullMinimumWidth * 2
    readonly property bool floating: isFullWidth || !forcePinned
    readonly property bool enableShortcuts: opened && isFullWidth
    readonly property string currentPageId: pagesModel[currentPageIndex] ? pagesModel[currentPageIndex].pageId : ""

    property var currentPageComponent
    property alias initialItem: corePage.initialItem
    property alias currentItem: corePage.currentItem
    property alias stackView: corePage.stackView
    property alias headerExpanded: corePage.headerExpanded
    property bool forceFullWidth: false
    property bool forcePinned: false

    // Page switcher
    readonly property string currentTitle: corePage.currentItem ? corePage.currentItem.title
                                                                : pagesModel[currentPageIndex] ? pagesModel[currentPageIndex].title
                                                                                               : ""
    readonly property string currentIconName: pagesModel[currentPageIndex] ? pagesModel[currentPageIndex].iconName : ""

    property list<Common.PageDrawerItem> pagesModel
    property int currentPageIndex: -1

    width: nonFullWidthCapable && !forceFullWidth ? nonFullMinimumWidth : parent.width
    interactive: false
    modal: isFullWidth
    closePolicy: QQC2.Popup.NoAutoClose

    function openWithItem(_item) {
        open()
        corePage.clear()

        if (opened) {
            corePage.push(_item)
        } else {
            internal.itemToPush = _item
        }
    }

    function openWithPageIndex(pageIndex) {
        currentPageIndex = pageIndex
    }

    onOpened: {
        if (internal.itemToPush) {
            corePage.push(internal.itemToPush)
            internal.itemToPush = null
        }
        forceActiveFocus()
    }
    onClosed: {
        corePage.clear()
    }

    onCurrentPageIndexChanged: {
        if (pagesModel.length > 0 && !internal.itemToPush) {
            let currentModelItem = pagesModel[currentPageIndex]
            if (currentModelItem) {
                openWithItem(currentModelItem.pageComponent)
            }
        }
    }

    onCurrentItemChanged: {
        if (currentItem) {
            if (currentItem.hasOwnProperty("displayedInFullWidthPanel")) {
                currentItem.displayedInFullWidthPanel = Qt.binding( function() { return pageDrawer.isFullWidth } )
            }
            if (currentItem.hasOwnProperty("enableShortcuts")) {
                currentItem.enableShortcuts = Qt.binding( function() { return pageDrawer.enableShortcuts } )
            }
            if (currentItem.hasOwnProperty("pageManager")) {
                currentItem.pageManager = Qt.binding( function() { return corePage } )
            }
            currentItem.forceActiveFocus()
        }
    }

    property var defaultLeftActions: [ backAction ]
    property var defaultRightActions: pageDrawer.nonFullWidthCapable && pageDrawer.isFullWidth ? [ expandFullAction ] : [ panelActions ]

    Common.BaseAction {
        id: backAction

        text: i18n.tr("Back")
        tooltipText: i18n.tr("Go back to previous page")
        iconName: "back"
        visible: pageDrawer.isFullWidth && ((pageDrawer.currentItem && pageDrawer.currentItem.showBackButton) || !pageDrawer.currentItem)
        shortcut: pageDrawer.enableShortcuts ? StandardKey.Cancel : ""

        onTrigger:{
            if (corePage.depth > 1) {
                corePage.pop()
            } else {
                pageDrawer.close()
                corePage.pop()
            }
        }
    }

    Common.BaseAction {
        id: panelActions

        visible: !pageDrawer.isFullWidth
        text: i18n.tr("Side panel actions")
        iconName: "other-actions"

        onTrigger: panelActionsMenu.show("", caller, isBottom)
    }

    Common.AdvancedMenu {
        id: panelActionsMenu

        type: Common.AdvancedMenu.Type.ItemAttached
        transformOrigin: QQC2.Menu.TopRight
        menuActions: [ pinAction, expandFullAction, closeAction ]
    }

    Common.BaseAction {
        id: pinAction

        visible: !pageDrawer.isFullWidth
        text: pageDrawer.forcePinned ? i18n.tr("Unpin panel") : i18n.tr("Pin panel")
        tooltipText: pageDrawer.forcePinned ? i18n.tr("Make the side panel float above the contents behind")
                                            : i18n.tr("Make the side panel occupy the side and move contents to the right")
        iconName: pageDrawer.forcePinned ? "unpinned" : "pinned"

        onTrigger: pageDrawer.forcePinned = !pageDrawer.forcePinned
    }

    Common.BaseAction {
        id: expandFullAction

        text: pageDrawer.forceFullWidth ? i18n.tr("Collapse panel") : i18n.tr("Expand panel")
        tooltipText: pageDrawer.forceFullWidth ? i18n.tr("Collapse into a side panel") : i18n.tr("Expand to full width of the app")
        iconName: pageDrawer.forceFullWidth ? "go-first" : "go-last"

        onTrigger: pageDrawer.forceFullWidth = !pageDrawer.forceFullWidth
    }

    Common.BaseAction {
        id: closeAction

        visible: !pageDrawer.isFullWidth
        text: i18n.tr("Close panel")
        iconName: "close"

        onTrigger: {
            pageDrawer.close()
            corePage.pop()
        }
    }

    Component {
        id: pagePickerComponent

        Common.CustomizedButton {
            id: pageSwitcherButton

            text: pageDrawer.currentTitle
            Suru.textLevel: Suru.HeadingTwo
            alignment: Qt.AlignLeft
            display: pageDrawer.headerExpanded ? QQC2.AbstractButton.IconOnly : QQC2.AbstractButton.TextBesideIcon
            icon.name: pageDrawer.currentIconName
            secondaryIcon {
                name: "go-down"
                width: units.gu(1)
                height: secondaryIcon.width
            }
            onClicked: pageSwitcherMenuComponent.createObject(corePage.header).show("", pageSwitcherButton, false)
        }
    }

    // Delay if further since the menu doesn't hide completely
    Timer {
        id: delayPush

        running: false
        interval: 1
        onTriggered: {
            if (internal.itemToPush) {
                corePage.push(internal.itemToPush)
                internal.itemToPush = null
            }
        }
    }

    Component {
        id: pageSwitcherMenuComponent

        Common.AdvancedMenu {
            id: overflowMenu

            type: Common.AdvancedMenu.Type.ItemAttached
            doNotOverlapCaller: true
            destroyOnClose: true
            onClosed: {
                if (internal.itemToPush) {
                    delayPush.restart()
                }
            }

            Repeater {
                id: pageMenuItemRepeater

                model: pageDrawer.pagesModel

                Common.CustomizedMenuItem {
                    text: modelData ? modelData.title : ""
                    tooltipText: modelData ? modelData.description : ""
                    visible: index != pageDrawer.currentPageIndex
                    iconName: modelData ? modelData.iconName : ""
                    rightDisplay: modelData ? modelData.shortcutText : ""
                    onTriggered: {
                        corePage.clear()
                        internal.itemToPush = pagesModel[index].pageComponent
                        pageDrawer.currentPageIndex = index
                    }
                }
            }
        }
    }

    RowLayout {
        anchors.fill: parent

        BasePageStack {
            id: corePage

            Layout.alignment: Qt.AlignHCenter
            Layout.maximumWidth: pageDrawer.maximumPageWidth
            Layout.fillWidth: true
            Layout.fillHeight: true

            defaultLeftActions: pageDrawer.defaultLeftActions
            defaultRightActions: pageDrawer.defaultRightActions

            customTitleItem {
                sourceComponent: pagePickerComponent
                hideOnExpand: false
                fillWidth: false
                alignment: Qt.AlignLeft
            }
        }
    }
    
    /* Workaround to allow closing the drawer with outside press
     * Disabling the interactive property disables both swipe closing and outside press 
     * We only need to disable swipe closing */
    MouseArea {
        id: outsideMouseArea

        property real negativeMargin: pageDrawer.parent ? -(pageDrawer.parent.width - pageDrawer.width) : -pageDrawer.width

        enabled: pageDrawer.floating
        onClicked: pageDrawer.close()

        anchors {
            right: parent.left
            left: parent.left
            leftMargin: negativeMargin
            top: parent.top
            bottom: parent.bottom
        }
        
        states: [
            State {
                name: "right"
                when: pageDrawer.edge == Qt.LeftEdge

                AnchorChanges {
                    target: outsideMouseArea

                    anchors.right: parent.right
                    anchors.left: parent.right
                }

                PropertyChanges {
                    target: outsideMouseArea

                    anchors.leftMargin: 0
                    anchors.rightMargin: negativeMargin
                }
            }
            , State {
                name: "left"
                when: pageDrawer.edge == Qt.RightEdge

                AnchorChanges {
                    target: outsideMouseArea

                    anchors.right: parent.left
                    anchors.left: parent.left
                }

                PropertyChanges {
                    target: outsideMouseArea

                    anchors.leftMargin: negativeMargin
                    anchors.rightMargin: 0
                }
            }
        ]
    }

    QtObject {
        id: internal

        property var itemToPush // For delaying pushing the item
    }
}
