import QtQuick 2.12

Loader {
    id: pageDrawerLoader

    readonly property bool opened: item && item.opened

    property string currentPageId: item ? item.currentPageId : ""
    property var currentPageComponent
    property int currentPageIndex: -1
    property list<PageDrawerItem> pagesModel
    property bool forcePinned: false
    property bool alwaysExpandToFullWidth: false
    property int edge: Qt.LeftEdge // Or Qt.RightEdge

    signal loseFocus

    active: false
    asynchronous: true
    sourceComponent: pageDrawerComponent

    function openWithItem(item) {
        currentPageComponent = item
        active = true
    }

    function openWithPageIndex(pageIndex) {
        currentPageIndex = pageIndex
        active = true
    }

    function openWithPageIndexSynchronously(pageIndex) {
        currentPageIndex = pageIndex
        asynchronous = false
        active = true
        asynchronous = true
    }

    function close() {
        if (item) {
            item.close()
        }
    }

    onActiveChanged: {
        if (!active) {
            currentPageComponent = null
            currentPageIndex = -1
        }
    }

    onCurrentPageIndexChanged: internal.openPageInIndex()
    onCurrentPageComponentChanged: internal.openPageInItem()
    onLoaded: {
        if (currentPageIndex > -1) {
            internal.openPageInIndex()
        }
        if (currentPageComponent) {
            internal.openPageInItem()
        }
    }

    QtObject {
        id: internal

        function openPageInItem() {
            if (item) item.openWithItem(currentPageComponent)
        }

        function openPageInIndex() {
            if (item) item.openWithPageIndex(currentPageIndex)
        }
    }

    Connections {
        target: pageDrawerLoader.item
        ignoreUnknownSignals: true

        onClosed: pageDrawerLoader.active = false
        onForcePinnedChanged: pageDrawerLoader.forcePinned = target.forcePinned
        onForceFullWidthChanged: pageDrawerLoader.alwaysExpandToFullWidth = target.forceFullWidth
    }

    // Persistent across sessions
    Binding {
        target: pageDrawerLoader.item
        property: "forcePinned"
        value: pageDrawerLoader.forcePinned
    }

    // Not persistent across sessions
    Binding {
        target: pageDrawerLoader.item
        property: "forceFullWidth"
        value: pageDrawerLoader.alwaysExpandToFullWidth
    }

    Component {
        id: pageDrawerComponent

        PageDrawer {
            edge: pageDrawerLoader.edge
            pagesModel: pageDrawerLoader.pagesModel
            height: parent.height

            onClosed: pageDrawerLoader.loseFocus()
        }
    }
}
