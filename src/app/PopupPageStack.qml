import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.12
import QtQuick.Controls.Suru 2.2
import Ubuntu.Content 1.3
import "."

QQC2.Dialog {
    id: popupPageStack

    readonly property real nonFullMinimumWidth: units.gu(50)
    readonly property real maximumPageWidth: units.gu(140)
    readonly property bool nonFullWidthCapable: parent.width > nonFullMinimumWidth * 2
    readonly property bool isFullWidth: width === parent.width
    readonly property string currentTitle: corePage.currentItem ? corePage.currentItem.title : ""

    property real maximumWidth: units.gu(90)
    property real preferredWidth: parent.width
    property real maximumHeight: units.gu(80)
    property real preferredHeight: parent.height

    property alias headerExpanded: corePage.headerExpanded
    property alias initialItem: corePage.initialItem
    property alias currentItem: corePage.currentItem
    property alias stackView: corePage.stackView
    property alias depth: corePage.depth

    property bool enableShortcuts: true
    property var defaultLeftActions: [ backAction ]
    property var defaultRightActions: []

    width: nonFullWidthCapable ? nonFullMinimumWidth : parent.width
    height: isFullWidth ? preferredHeight : Math.min(preferredHeight, maximumHeight)
    x: (parent.width - width) / 2
    parent: QQC2.Overlay.overlay
    topPadding: 0
    leftPadding: 0
    rightPadding: 0
    bottomPadding: 0
    closePolicy: QQC2.Popup.CloseOnEscape | QQC2.Popup.CloseOnPressOutside
    modal: true

    BaseAction {
        id: backAction
        
        readonly property bool isBackContext: popupPageStack.isFullWidth || popupPageStack.depth > 1

        text:isBackContext  ? i18n.tr("Back") : i18n.tr("Close")
        tooltipText: isBackContext ? i18n.tr("Go back to previous page") : i18n.tr("Close page")
        iconName: isBackContext ? "back" : "close"
        visible: popupPageStack.currentItem && popupPageStack.currentItem.showBackButton

        onTrigger:{
            if (corePage.depth > 1) {
                corePage.pop()
            } else {
                popupPageStack.close()
                corePage.pop()
            }
        }
    }

    QQC2.Overlay.modal: Rectangle {
        color: Suru.overlayColor
        Behavior on opacity { NumberAnimation { duration: Suru.animations.FastDuration } }
    }

    function openWithItem(item) {
        corePage.clear()
        corePage.push(item)
        open()
    }

    onCurrentItemChanged: {
        if (currentItem) {
            if (currentItem.hasOwnProperty("displayedInFullWidthPanel")) {
                currentItem.displayedInFullWidthPanel = Qt.binding( function() { return popupPageStack.isFullWidth } )
            }
            if (currentItem.hasOwnProperty("enableShortcuts")) {
                currentItem.enableShortcuts = Qt.binding( function() { return popupPageStack.enableShortcuts } )
            }
            if (currentItem.hasOwnProperty("pageManager")) {
                currentItem.pageManager = Qt.binding( function() { return corePage } )
            }
            currentItem.forceActiveFocus()
        }
    }

    onOpened: forceActiveFocus()
    onClosed: {
        corePage.headerExpanded = false
        corePage.clear()
    }

    onAboutToShow: y = Qt.binding( function() { return parent.width >= units.gu(90) ? (parent.height - height) / 2 : 0 } )

    RowLayout {
        anchors.fill: parent

        BasePageStack {
            id: corePage

            Layout.alignment: Qt.AlignHCenter
            Layout.maximumWidth: popupPageStack.maximumPageWidth
            Layout.fillWidth: true
            Layout.fillHeight: true

            defaultLeftActions: popupPageStack.defaultLeftActions
            defaultRightActions: popupPageStack.defaultRightActions
        }
    }
}
