import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import QtQuick.Controls.Suru 2.2

QQC2.ItemDelegate {
    id: roundedItemDelegate

    property real radius: units.gu(1.5)
    property string rightSideText
    property bool showDivider: false
    property bool transparentBackground: false
    property color backgroundColor: Suru.backgroundColor
    property color borderColor: Suru.backgroundColor
    property color highlightedBorderColor: Suru.highlightColor
    property string tooltipText
    property bool interactive: true

    // TODO: Temporarily disabled until the hover issue is fixed
    // Swiping and scrolling frequently leaves an item in hovered state
    hoverEnabled: false

    focusPolicy: Qt.TabFocus
    indicator: QQC2.Label {
        text: roundedItemDelegate.rightSideText
        verticalAlignment: Text.AlignVCenter
        color: Suru.foregroundColor
        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
            rightMargin: units.gu(2)
        }
    }
    background: Rectangle {
        id: backgroundRec

        color: roundedItemDelegate.transparentBackground ? "transparent" : roundedItemDelegate.backgroundColor
        border.width: roundedItemDelegate.highlighted ? units.gu(0.8) : units.gu(0.6)
        border.color: roundedItemDelegate.transparentBackground ? "transparent"
                                        : roundedItemDelegate.highlighted ? roundedItemDelegate.highlightedBorderColor : roundedItemDelegate.borderColor
        radius: roundedItemDelegate.radius
        Behavior on border.width {
            UbuntuNumberAnimation { duration: UbuntuAnimation.FastDuration }
        }
        
        Rectangle {
            id: highlightRect

            anchors.fill: parent

            visible: roundedItemDelegate.down || roundedItemDelegate.hovered || roundedItemDelegate.highlighted
            opacity: roundedItemDelegate.highlighted ? 0.1 : 1.0
            border.width: background.border.width
            border.color: "transparent"
            radius: background.radius
            color: {
                if (roundedItemDelegate.highlighted)
                    return Suru.highlightColor

                return roundedItemDelegate.interactive && roundedItemDelegate.down ? Qt.darker(Suru.secondaryBackgroundColor, 1.1) : Suru.secondaryBackgroundColor
            }

            Behavior on color {
                enabled: !roundedItemDelegate.highlighted
                ColorAnimation {
                    duration: Suru.animations.FastDuration
                    easing: Suru.animations.EasingIn
                }
            }

            Behavior on opacity {
                NumberAnimation {
                    duration: Suru.animations.FastDuration
                    easing: Suru.animations.EasingIn
                }
            }
        }

        Rectangle {
            visible: roundedItemDelegate.showDivider
            anchors.bottom: parent.bottom
            width: parent.width
            height: roundedItemDelegate.Suru.units.dp(1)
            color: roundedItemDelegate.Suru.neutralColor
        }
    }

    QQC2.ToolTip.delay: 1000
    QQC2.ToolTip.visible: hovered && roundedItemDelegate.tooltipText !== ""
    QQC2.ToolTip.text: roundedItemDelegate.tooltipText
}
