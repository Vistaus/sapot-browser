import QtQuick 2.12
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.12
import Ubuntu.Components 1.3
import "." as Common

BasePage {
    id: searchSuggestionsSettingsPage

    property alias model: searchSuggestionsListView.model

    signal modelDataChanged(var newModelData)

    title: i18n.tr("Search suggestions")
    allowRightOverflow: false

    ScrollView {
        id: mainScrollView

        anchors.fill: parent

        Common.BaseListView {
            id: searchSuggestionsListView

            readonly property bool allItemsSelected: ViewItems.selectedIndices.length === count

            anchors.fill: parent
            pageHeader: searchSuggestionsSettingsPage.pageManager ? searchSuggestionsSettingsPage.pageManager.header : searchSuggestionsSettingsPage.header
            focus: true

            ViewItems.dragMode: true
            ViewItems.onDragUpdated: {
                if (event.status == ListItemDrag.Started) {
                    if (model[event.from] == "Immutable")
                        event.accept = false;
                    return;
                }
                if (model[event.to] == "Immutable") {
                    event.accept = false;
                    return;
                }
                // No instantaneous updates
                if (event.status == ListItemDrag.Moving) {
                    event.accept = false;
                    return;
                }
                if (event.status == ListItemDrag.Dropped) {
                    var fromItem = model[event.from];
                    var list = model;
                    list.splice(event.from, 1);
                    list.splice(event.to, 0, fromItem);
                    searchSuggestionsSettingsPage.modelDataChanged(list);
                }
            }

            delegate: ListItem {
                height: layout.height + (divider.visible ? divider.height : 0)
                color: dragging ? theme.palette.selected.base : "transparent"

                SlotsLayout {
                    id: layout

                    mainSlot: ColumnLayout {
                        spacing: units.gu(1)

                        QQC2.Label {
                            Layout.fillWidth: true

                            text: internal.getTitle(modelData.id)
                            Suru.textLevel: Suru.HeadingThree
                        }

                        ColumnLayout {
                            Layout.leftMargin: units.gu(2)

                            QQC2.CheckBox {
                                id: enabledCheckBox

                                text: i18n.tr("Enabled")
                                checked: modelData.enabled

                                onClicked: internal.toggleEnabledOfItem(index, !modelData.enabled)
                            }

                            QQC2.CheckBox {
                                id: incognitoCheckBox

                                text: i18n.tr("Show in Private windows")
                                checked: modelData.showInIncognito

                                onClicked: internal.toggleIncognitoOfItem(index, !modelData.showInIncognito)
                            }

                            RowLayout {
                                spacing: units.gu(2)

                                QQC2.Label {
                                    text: i18n.tr("Result limit")
                                }

                                QQC2.SpinBox {
                                    from: 1
                                    to: 10
                                    stepSize: 1
                                    value: modelData.limit

                                    onValueModified: internal.changeResultLimit(index, value)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    QtObject {
        id: internal

        readonly property var titlesArray: [
            { "id": "tabs", "title": i18n.tr("Tabs") }
            , { "id": "history", "title": i18n.tr("History") }
            , { "id": "bookmarks", "title": i18n.tr("Bookmarks") }
            , { "id": "searchengine", "title": i18n.tr("Search engine suggestions") }
        ]

        function findFromArray(arr, itemId) {
            return arr.find(item => item.id == itemId)
        }

        function toggleEnabledOfItem(index, enabled) {
            let arrNewValues = searchSuggestionsSettingsPage.model
            arrNewValues[index].enabled = enabled
            searchSuggestionsSettingsPage.modelDataChanged(arrNewValues)
        }

        function toggleIncognitoOfItem(index, showInIncognito) {
            let arrNewValues = searchSuggestionsSettingsPage.model
            arrNewValues[index].showInIncognito = showInIncognito
            searchSuggestionsSettingsPage.modelDataChanged(arrNewValues)
        }

        function changeResultLimit(index, limit) {
            let arrNewValues = searchSuggestionsSettingsPage.model
            arrNewValues[index].limit = limit
            searchSuggestionsSettingsPage.modelDataChanged(arrNewValues)
        }

        function getTitle(itemId) {
            return findFromArray(titlesArray, itemId).title
        }
    }
}

