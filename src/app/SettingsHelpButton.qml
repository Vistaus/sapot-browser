import QtQuick 2.12
import QtQuick.Controls 2.12 as QQC2

CustomizedButton {
    id: helpButton

    display: QQC2.AbstractButton.IconOnly
    icon.name: "help"
    transparentBackground: true

    onClicked: {
        QQC2.ToolTip.delay = 0
        QQC2.ToolTip.visible = true
    }

    QQC2.ToolTip.delay: 500
    QQC2.ToolTip.visible: hovered
    QQC2.ToolTip.text: helpButton.text
    QQC2.ToolTip.onVisibleChanged: QQC2.ToolTip.delay = 500
}
