import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import QtQuick.Controls.Suru 2.2

QQC2.Label {
    id: headerTitle

    Suru.textLevel: Suru.HeadingTwo
    elide: Label.ElideRight
    fontSizeMode: Text.HorizontalFit
    minimumPixelSize: units.gu(1)
    verticalAlignment: Text.AlignVCenter
}
