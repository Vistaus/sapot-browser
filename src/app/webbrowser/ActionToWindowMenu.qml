import QtQuick 2.9
import "../TextUtils.js" as TextUtils
import ".." as Common

Common.CustomizedMenu {
    id: actionToWindowMenu

    property alias model: repeater.model
    property var thisWindow
    property bool showIcon: true

    signal actionToNewWindow
    signal actionToWindow(var targetWindow)

    enabled: model.length > 1

    Common.CustomizedMenuItem {
        text: i18n.tr("New window")
        onTriggered: {
            actionToWindowMenu.actionToNewWindow()
        }
    }

    Common.CustomizedMenuSeparator {}

    Repeater {
        id: repeater

        delegate: Common.CustomizedMenuItem {

            property string windowTitle: {
                if (modelData) {
                    if (modelData.windowName) {
                        return modelData.windowName
                    } else if (modelData.tabsModel.currentTab) {
                        if (modelData.tabsModel.currentTab.title) {
                            return TextUtils.elideText(modelData.tabsModel.currentTab.title)
                        } else if (modelData.tabsModel.currentTab.url) {
                            if (modelData.tabsModel.currentTab.url.toString().length > 0) {
                                return TextUtils.elideMidText(modelData.tabsModel.currentTab.url.toString())
                            } else {
                                return i18n.tr("New Tab")
                            }
                        } else {
                            return i18n.tr("New Tab")
                        }
                    }
                }
                
                return ""
            }

            enabled: thisWindow !== modelData && !modelData.incognito
            text: i18n.tr("%1 (%2 tab)", "%1 (%2 tabs)", modelData.tabsModel.count).arg(windowTitle).arg(modelData.tabsModel.count)
            onTriggered: {
                actionToWindowMenu.actionToWindow(modelData)
            }
        }
    }
}
