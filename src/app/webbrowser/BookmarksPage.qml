import QtQuick 2.12
import Ubuntu.Components 1.3
import ".." as Common

Common.BasePage {
    id: bookmarksPage

    readonly property real foldersPaneWidth: units.gu(30)
    property url homepageUrl: viewLoader.item ? viewLoader.item.homeBookmarkUrl : ""

    signal bookmarkEntryClicked(url url)

    title: i18n.tr("Bookmarks")
    allowRightOverflow: false
    wide: width >= foldersPaneWidth * 3

    Loader {
        id: viewLoader

        anchors.fill: parent
        active: true
        asynchronous: true
        sourceComponent: bookmarksPage.wide ? wideViewComponent : narrowViewComponent

        onLoaded: if (item) item.forceActiveFocus()

        Connections {
            target: viewLoader.item

            onBookmarkClicked: bookmarksPage.bookmarkEntryClicked(url)
            onBookmarkRemoved: BookmarksModel.remove(url)
        }
    }

    Component {
        id: narrowViewComponent

        BookmarksFoldersView {
            id: bookmarksFoldersView

            anchors.fill: parent
            interactive: true
            focus: true
            pageOwner: bookmarksPage
        }
    }

    Component {
        id: wideViewComponent

        BookmarksFoldersViewWide {
            id: bookmarksFoldersViewWide

            anchors.fill: parent
            focus: true
            folderListWidth: bookmarksPage.foldersPaneWidth
            pageOwner: bookmarksPage
        }
    }
}
