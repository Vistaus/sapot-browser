/*
 * Copyright 2013-2017 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0
import QtSystemInfo 5.5
import QtWebEngine 1.10
import Qt.labs.settings 1.0
import Morph.Web 0.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Content 1.3
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.12
import webbrowserapp.private 0.1
import webbrowsercommon.private 0.1
import webbrowsercommon.private 0.1
import "../TextUtils.js" as TextUtils
import ".." as Common
import "recentview" as RecentView
import "." as Local

Common.BrowserView {
    id: browser

    property Settings settings
    property var bookmarksModel: BookmarksModel

    leftPadding: leftPageDrawer ? leftPageDrawer.floating ? 0 : leftPageDrawer.width
                                                     : 0

    currentWebview: tabsModel && tabsModel.currentTab ? tabsModel.currentTab.webview : null

    TabChrome {
        id: invisibleTabChrome
        visible: false
    }

    property bool bigUIMode: false

    property bool incognito: false

    property var tabsModel: TabsModel {
        // These methods are required by the TabsBar component
        readonly property int selectedIndex: currentIndex

        function addTab(newUrl) {
            var url = newUrl ? newUrl : ""
            internal.openUrlInNewTab(url, true, true, count)
        }

        function addExistingTab(tab) {
            add(tab);

            browser.bindExistingTab(tab);
        }

        function moveTab(from, to) {
            if (from === to
                || from < 0 || from >= count
                || to < 0 || to >= count) {
                return;
            }

            move(from, to);
        }

        function removeTab(index) {
            internal.closeTab(index, false);
        }

        // Remove tab but don't record in deleted tabs history
        function removeTabNoHistory(index) {
            internal.closeTab(index, false, true);
        }

        function removeTabWithoutDestroying(index) {
            internal.closeTab(index, true);
        }

        function selectTab(index) {
            internal.switchToTab(index, true);
        }

        function searchSameUrl(url) {
            let _modelCount = count

            for (let i = 0; i < _modelCount; ++i) {
                if (get(i).url == url) {
                    return i
                }
            }

            return -1
        }
    }

    property Common.BrowserWindow thisWindow
    readonly property bool isFullScreen: thisWindow.visibility == Window.FullScreen
    property Component windowFactory
    property var appWindows: []

    readonly property int maxClosedTabHistory: 20
    property var closedTabHistory: []
    property var deletedClosedTabHistory: []

    property alias leftPageDrawer: leftPageDrawerLoader.item
    property alias shortcutFindNext: shortcutFindNext
    property alias shortcutFindPrevious: shortcutFindPrevious
    property alias allQuickActions: actionsFactory.allActions

    readonly property string backHistoryShortcutText: shortcutBackHistory.nativeText
    readonly property string forwardHistoryShortcutText: shortcutForwardHistory.nativeText
    readonly property string reloadShortcutText: shortcutReloadTab.nativeText
    readonly property string bookmarkTabShortcutText: shortcutBookmarkTab.nativeText
    readonly property string saveAsShortcutText: shortcutSave.nativeText
    readonly property string viewSourceShortcutText: shortcutViewSource.nativeText
    readonly property string tabsViewShortcutText: shortCutTabsView.nativeText
    readonly property string findShortcutText: shortcutFind.nativeText

    readonly property bool hasKeyboard: keyboardModel.count > 0

    function serializeTabState(tab) {
        var state = {};
        state.uniqueId = tab.uniqueId;
        state.url = tab.url.toString();
        state.title = tab.title;
        state.forceDesktopSite = tab.forceDesktopSite
        state.forceMobileSite = tab.forceMobileSite
        if (UrlUtils.schemeIs(tab.icon, "image") && UrlUtils.hostIs(tab.icon, "favicon")) {
            state.icon = tab.icon.toString().substring(("image://favicon/").length)
        } else {
            state.icon = tab.icon.toString()
        }

        if (tab.previewOld == "") {
            state.preview = Qt.resolvedUrl(PreviewManager.previewPathFromUrl(tab.uniqueId)).toString();
        } else {
            state.preview = tab.previewOld.toString();
        }

        state.lastUseTimestamp = tab.lastUseTimestamp

        return state;
    }

    function restoreTabState(state) {
        var properties = {'initialUrl': UrlUtils.urlFromString(state.url), 'initialTitle': state.title,
                          'uniqueId': state.uniqueId, 'initialIcon': state.icon,
                          'forceDesktopSite': state.forceDesktopSite, 'forceMobileSite': state.forceMobileSite,
                          'preview': state.preview, 'previewOld': state.preview, "lastUseTimestamp": state.lastUseTimestamp ? state.lastUseTimestamp : 0 }
        return createTab(properties)
    }

    function createTab(properties) {
        return internal.createTabHelper(properties)
    }

    function bindExistingTab(tab) {
        Reparenter.reparent(tab, tabContainer);

        var properties = internal.buildContextProperties();

        for (var prop in properties) {
            tab[prop] = properties[prop];
        }

        // Ensure that we have switched to the tab
        // otherwise chrome position can break
        internal.switchToTab(tabsModel.count - 1, true);
    }

    function openRecentView(searchMode) {
        recentView.state = "shown"
        recentToolbar.state = "shown"
        if (searchMode) {
            tabslist.focusInput()
        }
    }
    
    function showTooltip(customText, position, customTimeout) {
        globalTooltip.display(customText, position, customTimeout)
    }

    signal newWindowRequested(bool incognito)
    signal newWindowFromTab(var tab, var callback)
    signal openLinkInNewWindowRequested(url url, bool incognito)
    signal openLinkInNewTabRequested(url url, bool background)
    signal switchToTabRequested(int tabIndex)
    signal moveTabNextToCurrentTabRequested(int tabIndex)
    signal shareLinkRequested(url linkUrl, string title)
    signal shareTextRequested(string text)
    signal savePageRequested
    signal viewSourceRequested

    Behavior on leftPadding { UbuntuNumberAnimation { duration: UbuntuAnimation.SnapDuration } }

    onOpenLinkInNewTabRequested: {
        internal.openUrlInNewTab(url, !background, true, tabsModel.selectedIndex + 1, tabsModel.currentTab)
    }

    onSwitchToTabRequested: {
        internal.switchToTab(tabIndex, true)
    }

    onMoveTabNextToCurrentTabRequested: {
        tabsModel.get(tabIndex).lastUseTimestamp = tabsModel.currentTab.lastUseTimestamp - 1
        tabsModel.moveTab(tabIndex, tabsModel.currentIndex + 1)
    }

    onShareLinkRequested: {

        internal.shareLink(linkUrl, title);
    }

    onShareTextRequested: {

        internal.shareText(text)
    }

    onSavePageRequested: {
        internal.savePageAs()
    }

    onViewSourceRequested: {
        var currentUrl = currentWebview.url;
        if (UrlUtils.isPdfViewerExtensionUrl(currentWebview.url)) {
            currentUrl = currentWebview.url.toString().substr(UrlUtils.getPdfViewerExtensionUrlPrefix().length);
        }
        openLinkInNewTabRequested("view-source:%1".arg(currentUrl), false);
    }

    onIsFullScreenChanged: {
        if (isFullScreen) {
            chrome.state = "hidden";
        } else {
            chrome.changeChromeState("shown")
        }
    }
    
    Connections {
        target: tabsModel.currentTab
        onFindInPageModeChanged: {
            if (tabsModel.currentTab && !tabsModel.currentTab.findInPageMode) {
                internal.resetFocus()
            }
        }
    }

    Connections {
        target: currentWebview

        /* Note that we are connecting the mediaAccessPermissionRequested signal
           on the current webview only because we want all the tabs that are not
           visible to automatically deny the request but emit the signal again
           if the same origin requests permissions (which is the default
           behavior in oxide if we don't connect a signal handler), so that we
           can pop-up a dialog asking the user for permission.

           Design is working on a new component that allows per-tab non-modal
           dialogs that will allow asking permission to the user without blocking
           interaction with the rest of the page or the window. When ready all
           tabs will have their mediaAccessPermissionRequested signal handled by
           creating one of these new dialogs.
        */
        //onMediaAccessPermissionRequested: PopupUtils.open(Qt.resolvedUrl("../MediaAccessDialog.qml"), null, { request: request })

        // this opens the file as download
        onPdfPrintingFinished: {

            if (success)
            {
             internal.openUrlInNewTab("file://%1".arg(filePath.replace(/\W/g, encodeURIComponent)), false)
            }
            else
            {
              console.debug(incognito ? "creating pdf failed" : "creating pdf %1 failed".arg(filePath))
            }
        }

        onNavigationRequested: {

            var url = request.url;
            var isMainFrame = request.isMainFrame;

            // for file urls we set currentDomain to "scheme:file", because there is no host
            var currentDomain = UrlUtils.schemeIs(currentWebview.url, "file") ? "scheme:file" : UrlUtils.extractHost(currentWebview.url);

            // handle custom schemes
            if (UrlUtils.hasCustomScheme(url))
            {
                if (! internal.areCustomUrlSchemesAllowed(currentDomain))
                {
                  request.action = WebEngineNavigationRequest.IgnoreRequest;

                  var allowCustomSchemesDialog = PopupUtils.open(Qt.resolvedUrl("../AllowCustomSchemesDialog.qml"), currentWebview);
                  allowCustomSchemesDialog.url = url;
                  allowCustomSchemesDialog.domain = currentDomain;
                  allowCustomSchemesDialog.showAllowPermanentlyCheckBox = ! browser.incognito;
                  allowCustomSchemesDialog.allow.connect(function() {internal.allowCustomUrlSchemes(currentDomain, false);
                                                                     internal.navigateToUrlAsync(url);
                                                                   }
                                                      );
                  allowCustomSchemesDialog.allowPermanently.connect(function() {internal.allowCustomUrlSchemes(currentDomain, true);
                                                                                internal.navigateToUrlAsync(url);
                                                                               }
                                                                   );
                }
                return;
            }

            // handle domain permissions
            var requestDomain = UrlUtils.schemeIs(url, "file") ? "scheme:file" : UrlUtils.extractHost(url);
            var requestDomainWithoutSubdomain = DomainPermissionsModel.getDomainWithoutSubdomain(requestDomain);
            var currentDomainWithoutSubdomain = DomainPermissionsModel.getDomainWithoutSubdomain(UrlUtils.extractHost(currentWebview.url));
            var domainPermission = DomainPermissionsModel.getPermission(requestDomainWithoutSubdomain);

            if (domainPermission !== DomainPermissionsModel.NotSet)
            {
                if (isMainFrame) {
                  DomainPermissionsModel.setRequestedByDomain(requestDomainWithoutSubdomain, "", browser.incognito);
                }
                else if (requestDomainWithoutSubdomain !== currentDomainWithoutSubdomain) {
                  DomainPermissionsModel.setRequestedByDomain(requestDomainWithoutSubdomain, currentDomainWithoutSubdomain, browser.incognito);
                }
            }

            if (domainPermission === DomainPermissionsModel.Blocked)
            {
                if (isMainFrame)
                {
                    var alertDialog = PopupUtils.open(Qt.resolvedUrl("../AlertDialog.qml"), browser.currentWebview);
                    alertDialog.title = i18n.tr("Blocked domain");
                    alertDialog.message = i18n.tr("Blocked navigation request to domain %1.").arg(requestDomainWithoutSubdomain);
                }
                request.action = WebEngineNavigationRequest.IgnoreRequest;
                return;
            }

            if ((domainPermission === DomainPermissionsModel.NotSet) && DomainPermissionsModel.whiteListMode)
            {
                var allowOrBlockDialog = PopupUtils.open(Qt.resolvedUrl("../AllowOrBlockDomainDialog.qml"), currentWebview);
                allowOrBlockDialog.domain = requestDomainWithoutSubdomain;
                if (isMainFrame)
                {
                    allowOrBlockDialog.parentDomain = "";
                    allowOrBlockDialog.allow.connect(function() {
                        DomainPermissionsModel.setRequestedByDomain(requestDomainWithoutSubdomain, "", browser.incognito);
                        DomainPermissionsModel.setPermission(requestDomainWithoutSubdomain, DomainPermissionsModel.Whitelisted, browser.incognito);
                        currentWebview.url = url;
                    });
                }
                else
                {
                    allowOrBlockDialog.parentDomain = currentDomainWithoutSubdomain;
                    allowOrBlockDialog.allow.connect(function() {
                        DomainPermissionsModel.setRequestedByDomain(requestDomainWithoutSubdomain, currentDomainWithoutSubdomain, browser.incognito);
                        DomainPermissionsModel.setPermission(requestDomainWithoutSubdomain, DomainPermissionsModel.Whitelisted, browser.incognito);
                        var alertDialog = PopupUtils.open(Qt.resolvedUrl("../AlertDialog.qml"), browser.currentWebview);
                        alertDialog.title = i18n.tr("Whitelisted domain");
                        alertDialog.message = i18n.tr("domain %1 is now whitelisted, it will be active on the next page reload.").arg(requestDomainWithoutSubdomain);
                    });
                }
                allowOrBlockDialog.block.connect(function() {
                    DomainPermissionsModel.setRequestedByDomain(requestDomainWithoutSubdomain, isMainFrame ? "" : currentDomainWithoutSubdomain, browser.incognito);
                    DomainPermissionsModel.setPermission(requestDomainWithoutSubdomain, DomainPermissionsModel.Blocked, browser.incognito);
                  });
                request.action = WebEngineNavigationRequest.IgnoreRequest;
                return;
            }

            // handle user agents
            if (isMainFrame)
            {
                currentWebview.hideContextMenu();
                var newUserAgentId = (UserAgentsModel.count > 0) ? DomainSettingsModel.getUserAgentId(requestDomain) : 0;

                // change of the custom user agent
                if (newUserAgentId !== currentWebview.context.userAgentId)
                {
                    currentWebview.context.userAgentId = newUserAgentId;
                    currentWebview.context.customUserAgent = (newUserAgentId > 0) ? UserAgentsModel.getUserAgentString(newUserAgentId) : "";

                    // for some reason when letting through the request, another navigation request will take us back to the
                    // to the previous page. Therefore we block it first and navigate to the new url with the correct user agent.
                    request.action = WebEngineNavigationRequest.IgnoreRequest;
                    currentWebview.url = url;
                    return;
                }
                else
                {
                    var appForceDesktop = browser.settings ? browser.settings.setDesktopMode :  false
                    var appForceMobile = browser.settings ? browser.settings.forceMobileSite : false
                    var tabForceDesktop = browser.tabsModel.currentTab.forceDesktopSite
                    var tabForceMobile = browser.tabsModel.currentTab.forceMobileSite
                    var forceDesktopUA = false
                    var forceMobileUA = false

                    if (currentWebview.context.__ua.calcScreenSize() == "small") {
                        // Small screeens use mobile UA by default so only check when desktop UA is forced
                        forceDesktopUA = ((appForceDesktop && !tabForceMobile) || (!appForceDesktop && tabForceDesktop))
                        currentWebview.context.__ua.setDesktopMode(forceDesktopUA);
                    } else {
                        // Large screeens use desktop UA by default so only check when mobile UA is forced
                        if (settings.autoDeskMobSwitch) {
                            forceMobileUA = ((browser.wide && appForceMobile && !tabForceDesktop)
                                                    || (browser.wide && !appForceMobile && tabForceMobile)
                                                    || (!browser.wide && !tabForceDesktop))
                        } else {
                            forceMobileUA = ((appForceMobile && !tabForceDesktop) || (!appForceMobile && tabForceMobile))
                        }

                        currentWebview.context.__ua.forceMobileSite(forceMobileUA);
                    }
                    // console.log("user agent: " + currentWebview.context.httpUserAgent);
                }
            }
        }
    }

    currentWebcontext: SharedWebContext.sharedContext
    defaultVideoCaptureDeviceId: settings.defaultVideoDevice ? settings.defaultVideoDevice : ""

    onDefaultVideoCaptureMediaIdUpdated: {
        if (!settings.defaultVideoDevice) {
            settings.defaultVideoDevice = defaultVideoCaptureDeviceId
        }
    }

    InputDeviceModel {
        id: miceModel
        filter: InputInfo.Mouse
    }

    InputDeviceModel {
        id: touchPadModel
        filter: InputInfo.TouchPad
    }

    InputDeviceModel {
        id: touchScreenModel
        filter: InputInfo.TouchScreen
    }

    Common.FilteredKeyboardModel {
        id: keyboardModel
    }

    FocusScope {
        id: contentsContainer

        readonly property real contentHeightThreshold: chrome.height
        property bool dim: chrome.editing && !chrome.findInPageMode && !newTabViewLoader.active

        states: [
            State {
                name: "chromeAutoHide"
                PropertyChanges {
                    target: contentsContainer
                    height: browser.bigUIMode && !browser.osk.visible ? browser.height - units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode) : browser.height
                }
            }
            , State {
                name: "chromeFixed"
                AnchorChanges {
                    target: contentsContainer
                    anchors.bottom: parent.bottom
                }
            }
            , State {
                name: "chromeFloating"
                AnchorChanges {
                    target: contentsContainer
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                }
            }
        ]

        anchors {
            left: parent.left
            leftMargin: browser.leftPadding
            right: parent.right
            top: chrome.bottom
            bottomMargin: browser.bigUIMode && !browser.osk.visible ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode) : 0
        }

        function changeState() {
            if (chrome.floating && browser.currentWebview && browser.currentWebview.url != "") {
                state = "chromeFloating"
            } else if (chrome.autoHide && browser.currentWebview && browser.currentWebview.contentHeight > browser.currentWebview.height + contentHeightThreshold) {
                state = "chromeAutoHide"
            } else if (!chrome.autoHide || (browser.currentWebview && browser.currentWebview.url == "")
                            || (chrome.autoHide && browser.currentWebview && browser.currentWebview.contentHeight <= browser.currentWebview.height + contentHeightThreshold)) {
                state = "chromeFixed"
            }
        }

        Rectangle {
            id: dimRec
            z: currentWebview ? currentWebview.z + 1 : tabContainer.z + 1
            anchors.fill: parent
            visible: opacity > 0
            color: UbuntuColors.jet
            opacity: contentsContainer.dim ? 0.6 : 0
            Behavior on opacity {
                UbuntuNumberAnimation {duration: UbuntuAnimation.FastDuration}
            }
        }

        Connections {
            target: chrome

            onAutoHideChanged: contentsContainer.changeState()
            onStateChanged: contentsContainer.changeState()
            onFloatingChanged: contentsContainer.changeState()
        }

        Connections {
            target: browser
            onCurrentWebviewChanged: contentsContainer.changeState()
        }

        Connections {
            target: browser.currentWebview
            onLoadingChanged: contentsContainer.changeState()
        }

        FocusScope {
            id: tabContainer

            anchors {
                fill: parent
                bottomMargin: activeFocus ? browser.osk.height : 0
            }

            // disable when newTabView is shown otherwise webview can capture drag events
            // do not use visible otherwise when a new tab is opened the locationBarController.offset
            // doesn't get updated, causing the Chrome to disappear
            enabled: !newTabViewLoader.active

            focus: !errorSheetLoader.focus &&
                   !invalidCertificateErrorSheetLoader.focus &&
                   !newTabViewLoader.focus &&
                   !sadTabLoader.focus

            Keys.onPressed: {
                if (tabContainer.visible && (event.key === Qt.Key_Backspace)) {
                    // Not handled as a window-level shortcut as it would take
                    // precedence over backspace events in HTML text fields
                    // (https://launchpad.net/bugs/1569938).
                    if (event.modifiers === Qt.NoModifier) {
                        internal.historyGoBack()
                        event.accepted = true
                    } else if (event.modifiers === Qt.ShiftModifier) {
                        internal.historyGoForward()
                        event.accepted = true
                    }
                }
            }
        }

        Loader {
            id: errorSheetLoader
            anchors {
                fill: tabContainer
            }
            Component.onCompleted: setSource("../ErrorSheet.qml", {
                                                 "visible": Qt.binding(function(){ return currentWebview ? ((currentWebview.LoadStatus !== WebEngineView.LoadSucceededStatus || currentWebview.LoadStatus !== WebEngineView.LoadStartedStatus) && currentWebview.lastLoadFailed) : false;}),
                                                 "loading": Qt.binding(function(){ return currentWebview ? currentWebview.loading : false;}),
                                                 "url": Qt.binding(function(){ return currentWebview ? currentWebview.url : "";}),
                                                 "errorString" : Qt.binding(function() {return currentWebview ? currentWebview.lastLoadRequestErrorString : "";}),
                                                 "errorDomain" : Qt.binding(function() {return currentWebview ? currentWebview.lastLoadRequestErrorDomain : -1;}),
                                                 "canGoBack" : Qt.binding(function() {return currentWebview && currentWebview.canGoBack;})
                                             })
            Connections {
                target: errorSheetLoader.item
                onBackToSafetyClicked: currentWebview.goBack()
                onRefreshClicked: currentWebview.reload()
            }

            focus: item && item.visible
            asynchronous: true
        }

        Loader {
            id: invalidCertificateErrorSheetLoader
            anchors {
                fill: tabContainer
                topMargin: (chrome.state == "shown") ? chrome.height : 0
            }
            Component.onCompleted: setSource("../InvalidCertificateErrorSheet.qml", {
                                                 "visible": Qt.binding(function(){ return currentWebview && currentWebview.certificateError != null }),
                                                 "certificateError": Qt.binding(function(){ return currentWebview ? currentWebview.certificateError : null })
                                             })
            Connections {
                target: invalidCertificateErrorSheetLoader.item
                onAllowed: {
                    // Automatically allow future requests involving this
                    // certificate for the duration of the session.
                    internal.allowCertificateError(currentWebview.certificateError)
                    currentWebview.resetCertificateError()
                }
                onDenied: {
                    currentWebview.resetCertificateError()
                }
            }
            focus: item && item.visible
            asynchronous: true
        }

        Loader {
            active: newTabViewLoader.active
            asynchronous: true
            anchors.fill: tabContainer
            sourceComponent: Rectangle {
                color: theme.palette.normal.foreground
            }
        }

        Loader {
            id: newTabViewLoader

            readonly property real maximumWidth: units.gu(140)

            anchors {
                top: tabContainer.top
                bottom: tabContainer.bottom
                horizontalCenter: tabContainer.horizontalCenter
            }
            width: Math.min(tabContainer.width, maximumWidth)
            clip: true  // prevents component from overlapping bottom edge etc

            // Avoid loading the new tab view if the webview has or will have content
            Connections {
                target: tabsModel.currentTab
                onEmptyChanged: newTabViewLoader.setActive(tabsModel.currentTab.empty)
                onForceDesktopSiteChanged: if (target.webview && settings.autoDeskMobSwitchReload) target.reload();
                onForceMobileSiteChanged: if (target.webview && settings.autoDeskMobSwitchReload) target.reload();
            }
            Connections {
                target: tabsModel
                onCurrentTabChanged: newTabViewLoader.setActive(tabsModel.currentTab && tabsModel.currentTab.empty)
            }
            active: false
            focus: active
            asynchronous: true

            Connections {
                target: browser
                onWideChanged: newTabViewLoader.selectTabView()
            }
            Component.onCompleted: newTabViewLoader.selectTabView()

            // XXX: this is a workaround for bug #1659435 caused by QTBUG-54657.
            // New tab view was sometimes overlaid on top of other tabs because
            // it was not unloaded even though active was set to false.
            // Ref.: https://launchpad.net/bugs/1659435
            //       https://bugreports.qt.io/browse/QTBUG-54657
            function setActive(active) {
                if (active) {
                    if (newTabViewLoader.source == "") {
                        selectTabView();
                    }
                } else {
                    newTabViewLoader.setSource("", {});
                }
                newTabViewLoader.active = active;
            }

            function selectTabView() {
                var source = browser.incognito ? "NewPrivateTabView.qml" :
                                                 (browser.wide ? "NewTabViewWide.qml" :
                                                                 "NewTabView.qml");
                var properties = browser.incognito ? {} : {"settingsObject": settings,
                                                           "focus": true};

                newTabViewLoader.setSource(source, properties);
            }

            Connections {
                target: newTabViewLoader.item && !browser.incognito ? newTabViewLoader.item : null
                onBookmarkClicked: {
                    chrome.requestedUrl = url
                    currentWebview.url = url
                    tabContainer.forceActiveFocus()
                }
                onBookmarkRemoved: BookmarksModel.remove(url)
                onHistoryEntryClicked: {
                    chrome.requestedUrl = url
                    currentWebview.url = url
                    tabContainer.forceActiveFocus()
                }
            }
            Keys.onUpPressed: chrome.focus = true
        }

        Loader {
            id: sadTabLoader
            anchors {
                fill: tabContainer
            }

            active: webProcessMonitor.crashed || (webProcessMonitor.killed && !currentWebview.loading)
            focus: active

            Component.onCompleted: setSource("SadTab.qml", {
                                                 "webview": Qt.binding(function () {return browser.currentWebview})
                                             })
            Connections {
                target: sadTabLoader.item
                onCloseTabRequested: internal.closeCurrentTab()
            }

            Common.WebProcessMonitor {
                id: webProcessMonitor
                webview: currentWebview
            }

            asynchronous: true
        }

        HoveredUrlLabel {
            anchors {
                left: tabContainer.left
                leftMargin: units.dp(-1)
                bottom: tabContainer.bottom
                bottomMargin: {
                    let additionalHeight = 0

                    if (tabsModel.currentTab) {
                        additionalHeight += tabsModel.currentTab.findInPageMargin

                        if (tabsModel.currentTab.findInPageMode) {
                            additionalHeight += tabsModel.currentTab.findBarHeight
                        }
                    }

                    return additionalHeight + units.dp(-1)
                }
            }
            height: units.gu(3)
            collapsedWidth: Math.min(units.gu(40), tabContainer.width)
            webview: browser.currentWebview
        }
    }

    SortFilterModel {
        id: recentTabsModel

        model: tabsModel
        sort.property: "timestamp"
        sort.order: Qt.DescendingOrder
    }

    PreviewSelectionAnimation {
        id: previewAnimate

        z: 100
        targetY: tabslist.chromeHeight
        targetHeight: browser.height - tabslist.chromeHeight
        onSelectionStart: recentView.reset()
        onSelectionEnd: internal.switchToTab(previewAnimate.sourceIndex)
    }

    FocusScope {
        id: recentView
        objectName: "recentView"

        z: 1
        anchors {
            fill: parent
            leftMargin: browser.leftPadding
        }
        opacity: (browser.currentWebview && !browser.currentWebview.isFullScreen && bottomEdgeHandle.dragging)
                        || tabslist.animating || (state == "shown")
                        || lastUsedTabPreviewLoader.animating ? 1 : 0
        visible: opacity > 0
        onVisibleChanged: {
            if (visible) {
                forceActiveFocus()
                currentWebview.hideContextMenu();
            } else {
                if (lastUsedTabPreviewLoader.item) {
                    lastUsedTabPreviewLoader.item.hide()
                }
                tabslist.hide()
            }
        }

        states: State {
            name: "shown"
        }

        function closeAndSwitchToTab(index) {
            recentView.reset()
            internal.switchToTab(index, false)
        }

        function reset() {
            hideAnimation.restart()
            recentToolbar.state = "hidden"
            tabslist.reset()
            internal.resetFocus()
        }

        Keys.onEscapePressed: {
            recentView.reset()
        }

        Keys.onPressed: {
            if (event.text.trim() !== "") {
                tabslist.focusInput();
                tabslist.searchText = event.text;
            }

            event.accepted = true;
        }

        onStateChanged: {
            if (state == "shown") {
                tabslist.commit()
            } else {
                tabslist.hide()
            }
        }

        Rectangle {
            id: backgroundRec

            anchors.fill: parent
            color: tabslist.gridType == TabsList.GridType.List ? theme.palette.normal.backgroundText : UbuntuColors.jet
            opacity: 0.5
            visible: tabslist.visible

            MouseArea {
                anchors.fill: parent
                preventStealing: true
                onClicked: recentView.reset()
            }
        }

        UbuntuNumberAnimation on opacity {
            id: hideAnimation
            running: false
            to: 0
            duration: UbuntuAnimation.BriskDuration
            onStopped: recentView.state = ""
        }

        Connections {
            target: bottomEdgeHandle

            onStageChanged: {
                if (target.dragging) {
                    switch (target.stage) {
                        case 0:
                        case 1:
                        case 2:
                            tabslist.hide()
                            if (tabslist.opacity == 1) {
                                Common.Haptics.playSubtle()
                            }
                            if (lastUsedTabPreviewLoader.item) {
                                lastUsedTabPreviewLoader.item.show(true)
                            } else {
                                tabslist.show()
                                if (!tabslist.visible) {
                                    Common.Haptics.play()
                                }
                            }
                            break
                        case 3:
                        case 4:
                        case 5:
                        default:
                            if (lastUsedTabPreviewLoader.item) {
                                lastUsedTabPreviewLoader.item.hide()
                            }
                            tabslist.show()
                            if (!tabslist.visible) {
                                Common.Haptics.play()
                            }
                            break
                    }
                }
            }
        }

        Loader {
            id: lastUsedTabPreviewLoader

            readonly property bool animating: commitAnimation.running
            readonly property int tabIndex: item ? tabsModel.indexOf(item.tabData.tab) : -1

            asynchronous: true
            active: recentTabsModel.count > 1
            z: 1
            y: parent.height - (bottomEdgeHandle.dragging ? bottomEdgeHandle.distance : 0)
            height: (width * (browser.height - tabslist.chromeHeight)) / browser.width
            width: bottomEdgeHandle.dragging ? tabslist.width : tabslist.width // Resets the width after the commit animation
            anchors.horizontalCenter: parent.horizontalCenter

            function commit() {
                item.show()

                internal.nextTabIndex = tabIndex
                yAnimation.to = tabslist.chromeHeight
                commitAnimation.restart()
            }

            ParallelAnimation {
                id: commitAnimation

                running: false

                onStopped: {
                    internal.switchToTab(lastUsedTabPreviewLoader.tabIndex)
                    lastUsedTabPreviewLoader.item.hide()
                }

                UbuntuNumberAnimation {
                    target: lastUsedTabPreviewLoader
                    property: "width"
                    to: parent.width
                    duration: yAnimation.duration
                }

                UbuntuNumberAnimation {
                    target: lastUsedTabPreviewLoader
                    property: "height"
                    to: browser.height - tabslist.chromeHeight
                    duration: yAnimation.duration
                }

                UbuntuNumberAnimation {
                    id: yAnimation

                    target: lastUsedTabPreviewLoader
                    property: "y"
                    to: 0
                    duration: UbuntuAnimation.BriskDuration
                }
            }

            sourceComponent: Component {
                Local.LastUsedTabPreview {
                    id: lastUsedTabPreview

                    tabData: recentTabsModel.get(1)
                    incognito: browser.incognito
                    chromeHeight: tabslist.tabChromeHeight
                    visible: opacity > 0
                    showChrome: bottomEdgeHandle.dragging
                    dragging: bottomEdgeHandle.dragging

                    Timer {
                        id: delaytabDataTimer

                        property var nextTab
                        interval: 1
                        onTriggered: {
                            lastUsedTabPreview.tabData = nextTab
                        }
                    }

                    Connections {
                        target: recentTabsModel
                        onDataChanged: {
                            var newTab = recentTabsModel.get(1)
                            if (lastUsedTabPreview.tabData !== newTab) {
                                delaytabDataTimer.nextTab = newTab
                                delaytabDataTimer.restart()
                            }
                        }
                    }
                }
            }
        }

        TabsList {
            id: tabslist
            anchors {
                top: parent.top
                topMargin: !browser.wide ? 0 : browser.height > units.gu(90) ? chromeHeight : units.gu(2)
                bottom: parent.bottom
                bottomMargin: recentToolbar.height
                horizontalCenter: parent.horizontalCenter
            }

            browserWide: browser.wide
            browserHeight: browser.height
            browserWidth: browser.width
            gridSize: browser.settings.tabsGridSize
            gridType: browser.settings.tabsGridType
            usePhysicalGesture: browser.settings.physicalForGestures
            enableImmediateSearch: browser.settings.tabsSearchBypassDelay
            enableNewTabSuggestions: browser.settings.tabsSearchNewTabSuggestions
            searchUrlTemplate: currentSearchEngine.urlTemplate
            width: parent.width >= units.gu(150) ? units.gu(150) : Math.min(parent.width, units.gu(120))
            model: recentTabsModel
            bottomDragDistance: bottomEdgeHandle.dragging ? bottomEdgeHandle.distance : 0
            chromeHeight: chrome.state == "shown" ? chrome.height : 0
            tabWithCurrentContextMenu: currentTabMenu.opened ? currentTabMenu.tab : null
            onScheduleTabSwitch: internal.nextTabIndex = index
            onTabSelected: {
                let fadeOut
                if (browser.tabsModel.get(index).webview) {
                    fadeOut = false
                } else {
                    fadeOut = true
                }
                internal.nextTabIndex = index // Should be executed before the above if else
                previewAnimate.startAnimation(index, previewUrl, previewContainer, fadeOut)
            }
            onTabClosed: internal.closeTab(index)
            onGoToUrlRequested: {
                recentView.reset()
                browser.openLinkInNewTabRequested(url, false)
            }
            onRequestClose: recentView.reset()
            onTabContextMenu: {
                if (!mouseClick) Common.Haptics.play()
                currentTabMenu.openMenu(mouseClick, tabObject)
            }
        }

        Local.Toolbar {
            id: recentToolbar
            objectName: "recentToolbar"

            anchors {
                left: tabslist.left
                right: tabslist.right
            }
            height: units.gu(7)
            state: "hidden"

            leftActions: [
                Common.ToolbarAction {
                    objectName: "doneButton"
                    text: i18n.tr("Close")
                    iconName: "close"
                    
                    onTrigger: {
                        recentView.reset()
                    }
                }
            ]

            rightActions: [
                Common.ToolbarAction {
                    id: gridOptionsAction
                    objectName: "gridOptionsAction"
                    text: i18n.tr("Grid options")
                    iconName: "sort-listitem"

                    onTrigger: gridOptionsMenu.show(i18n.tr("Grid options"), source) // source comes from the trigger signal
                }
                , Common.ToolbarAction {
                    id: reopenAction
                    objectName: "restoreButton"
                    text: i18n.tr("Restore..")
                    iconName: "undo"

                    onTrigger: recentlyClosedMenu.show("", source)
                }
                , Common.ToolbarAction {
                    objectName: "currentTabActions"
                    text: i18n.tr("Current tab/window actions")
                    iconName: "contextual-menu"

                    onTrigger: currentTabMenu.openMenu(true, tabsModel.currentTab, source)
                }
                , Common.ToolbarAction {
                    objectName: "searchTabButton"
                    text: i18n.tr("Search tabs")
                    iconName: "find"

                    onTrigger: tabslist.focusInput();
                }
                , Common.ToolbarAction {
                    objectName: "newTabButton"

                    text: i18n.tr("New tab")
                    iconName: browser.incognito ? "private-tab-new" : "add"

                    onTrigger: {
                        recentView.reset()
                        internal.openUrlInNewTab("", true)
                    }
                }
            ]

            Common.AdvancedMenu {
                id: gridOptionsMenu

                type: Common.AdvancedMenu.Type.BottomAttached

                QQC2.ButtonGroup { id: gridTypeBtnGrp }

                ColumnLayout {
                    anchors {
                        left: parent.left
                        right: parent.right
                        leftMargin: units.gu(2)
                        rightMargin: anchors.leftMargin
                    }

                    QQC2.Label {
                        Layout.fillWidth: true
                        Layout.topMargin: units.gu(2)

                        visible: gridSizeSpinBox.visible
                        text: i18n.tr("Size")
                        Suru.textLevel: Suru.Small
                    }

                    QQC2.SpinBox {
                        id: gridSizeSpinBox

                        Layout.fillWidth: true
                        Layout.margins: units.gu(2)

                        visible: browser.settings.tabsGridType !== TabsList.GridType.List
                        from: 20
                        to: 60
                        stepSize: 2

                        textFromValue: function(value, locale) { return i18n.tr("%1 %2").arg((Number(value) - from) /  (to - from) * 100).arg("%"); }
                        onValueChanged: browser.settings.tabsGridSize = value

                        Binding {
                            target: gridSizeSpinBox
                            property: "value"
                            value: browser.settings.tabsGridSize
                        }

                        property string tooltipText: i18n.tr("Spacing and minimum of two columns are maintained. Size may not change in current view")

                        focusPolicy: Qt.TabFocus

                        QQC2.ToolTip.delay: 1000
                        QQC2.ToolTip.visible: hovered
                        QQC2.ToolTip.text: gridSizeSpinBox.tooltipText
                    }

                    QQC2.Label {
                        Layout.fillWidth: true
                        Layout.topMargin: !gridSizeSpinBox.visible ? units.gu(2) : 0

                        text: i18n.tr("Type")
                        Suru.textLevel: Suru.Small
                    }

                    Repeater {
                        model: [
                            {
                                "title": i18n.tr("Aspect Ratio (%1)").arg(i18n.tr("Default")), "value": TabsList.GridType.AspectRatio
                                , "tooltipText": i18n.tr("Follows the current aspect ratio of the web view")
                            }
                            , {
                                "title": i18n.tr("Square"), "value": TabsList.GridType.Square
                                , "tooltipText": i18n.tr("Display with equal width and height")
                            }
                            , {
                                "title": i18n.tr("List"), "value": TabsList.GridType.List
                                , "tooltipText": i18n.tr("Display as a list without tab previews")
                            }
                        ]

                        Common.CustomizedRadioDelegate {
                            id: gridTypeDelegate

                            readonly property int itemValue: modelData.value

                            Layout.fillWidth: true
                            Layout.preferredHeight: units.gu(6)
                            text: modelData.title
                            tooltipText: modelData.tooltipText
                            onCheckedChanged: if (checked) browser.settings.tabsGridType = itemValue
                            QQC2.ButtonGroup.group: gridTypeBtnGrp

                            Binding {
                                target: gridTypeDelegate
                                property: "checked"
                                value: browser.settings.tabsGridType == gridTypeDelegate.itemValue
                            }
                        }
                    }
                }
            }

            Common.AdvancedMenu {
                id: recentlyClosedMenu

                type: Common.AdvancedMenu.Type.BottomAttached

                Common.CustomizedMenu {
                    id: sessionsMenu

                    title: i18n.tr("Backed up sessions")

                    function showConfirmDialog(fromPrevious, targetIndex, targetTitle) {
                        var properties = {
                            fromPrevious: fromPrevious
                            , targetIndex: targetIndex
                            , restoreTitle: targetTitle
                        }
                        var confirmDialog = sessionRestoreDialogComponent.createObject(browser, properties)
                        if (browser.wide) {
                            confirmDialog.openNormal();
                        } else {
                            confirmDialog.openBottom();
                        }
                    }

                    RecentView.SessionsMenu {
                        id: prevSessionsMenu

                        title: i18n.tr("Previous session")
                        model: webbrowserapp.sessionBackupPrevious.dataArray
                        onItemTriggered: {
                            sessionsMenu.showConfirmDialog(true, targetIndex, targetTitle)
                        }
                    }

                    RecentView.SessionsMenu {
                        id: currentSessionsMenu

                        title: i18n.tr("Current session")
                        model: webbrowserapp.sessionBackupCurrent.dataArray
                        onItemTriggered: {
                            sessionsMenu.showConfirmDialog(false, targetIndex, targetTitle)
                        }
                    }
                }

                RecentView.RecentlyClosedMenu {
                    id: closedTabsMenu

                    maxDisplayedCount: 4
                    title: i18n.tr("Recently closed tabs")
                    model: browser.closedTabHistory
                    deletedModel: browser.deletedClosedTabHistory
                    delegate: Common.CustomizedMenuItem {
                        text: modelData.state.title ? modelData.state.title : modelData.state.url
                        favicon: modelData.state.icon
                        rightDisplay: model.index == 0 ? shortcutReopenLastTab.nativeText : "" //"Ctrl+Shift+T"
                        enabled: menu.overflow ? model.index >= closedTabsMenu.maxDisplayedCount : model.index < closedTabsMenu.maxDisplayedCount
                        onTriggered: {
                            recentView.reset()
                            recentlyClosedMenu.close()
                            internal.undoCloseTab(model.index)
                        }
                    }

                    onClear: {
                        if (closedTabsMenu.canUndo) {
                            browser.closedTabHistory = browser.deletedClosedTabHistory.slice()
                            browser.deletedClosedTabHistory = []
                        } else {
                            browser.deletedClosedTabHistory = browser.closedTabHistory.slice()
                            browser.closedTabHistory = []
                        }
                    }
                }

                RecentView.RecentlyClosedMenu {
                    id: closedWindowsMenu

                    maxDisplayedCount: 4
                    title: i18n.tr("Recently closed windows")
                    model: webbrowserapp.closedWindows
                    deletedModel: webbrowserapp.deletedClosedWindows
                    delegate: Common.CustomizedMenuItem {
                        readonly property string windowTitle: modelData.windowName ? modelData.windowName
                                           : modelData.tabs[modelData.currentIndex].title ? TextUtils.elideText(modelData.tabs[modelData.currentIndex].title)
                                                                                          : modelData.tabs[modelData.currentIndex].url ?
                                                                                                                    TextUtils.elideMidText(modelData.tabs[modelData.currentIndex].url)
                                                                                                                    : i18n.tr("New Tab")
                                                                                                                                

                        text: i18n.tr("%1 (%2 tab)", "%1 (%2 tabs)", modelData.tabs.length).arg(windowTitle).arg(modelData.tabs.length)
                        rightDisplay: model.index == 0 ? shortcutRestoreWindow.nativeText : "" //"Ctrl+Shift+N"
                        enabled: menu.overflow ? model.index >= closedWindowsMenu.maxDisplayedCount : model.index < closedWindowsMenu.maxDisplayedCount
                        onTriggered: webbrowserapp.undoCloseWindow(model.index)
                    }

                    onClear: {
                        if (closedWindowsMenu.canUndo) {
                            webbrowserapp.closedWindows = webbrowserapp.deletedClosedWindows.slice()
                            webbrowserapp.deletedClosedWindows = []
                        } else {
                            webbrowserapp.deletedClosedWindows = webbrowserapp.closedWindows.slice()
                            webbrowserapp.closedWindows = []
                        }
                    }
                }
            }

            Common.AdvancedMenu {
                id: currentTabMenu

                property var tab
                property bool mouseClicked: false
                readonly property bool tabIsCurrent: tab ? browser.tabsModel.currentTab == tab : false

                function openMenu(fromMouseClick, tabObject, sourceComponent) {
                    tab = tabObject
                    mouseClicked = fromMouseClick

                    if (sourceComponent) {
                        type = Common.AdvancedMenu.Type.BottomAttached
                        titleSize = Common.AdvancedMenu.TitleSize.Standard
                        multilineTitle = false
                        useFavicon = false
                        show(i18n.tr("Current tab"), sourceComponent)
                    } else {
                        type = Common.AdvancedMenu.Type.ContextMenu
                        titleSize = Common.AdvancedMenu.TitleSize.Small
                        multilineTitle = true
                        useFavicon = true
                        let headerTitle = tab.title ? tab.title
                                                : (tab.url.toString() ? tab.url
                                                                    : i18n.tr("New tab"))
                        if (showAsCenteredModal) {
                            show(headerTitle)
                        } else {
                            popupWithTitle(headerTitle, tab.icon)
                        }
                    }
                }

                parent: QQC2.Overlay.overlay
                type: Common.AdvancedMenu.Type.BottomAttached
                showAsCenteredModal: !mouseClicked
                modal: !mouseClicked
                incognito: browser.incognito

                Common.CustomizedMenuItem {
                    text: i18n.tr("New adjacent tab")
                    iconName: browser.incognito ? "private-tab-new" : "tab-new"
                    onTriggered: {
                        recentView.reset()
                        internal.openUrlInNewTab("", true, true, browser.tabsModel.indexOf(currentTabMenu.tab) + 1 )
                    }
                }

                Common.CustomizedMenuItem {
                    enabled: currentTabMenu.tab && currentTabMenu.tab.url.toString() ? true : false
                    text: i18n.tr("Duplicate tab")
                    iconName: "edit-copy"
                    onTriggered: {
                        recentView.reset()
                        internal.openUrlInNewTab(currentTabMenu.tab.url, true, true, browser.tabsModel.indexOf(currentTabMenu.tab) + 1, currentTabMenu.tab)
                    }
                }

                Common.CustomizedMenuItem {
                    text: i18n.tr("Move to new window")
                    iconName: "go-next"
                    enabled: browser.appWindows.length == 1 && !browser.incognito
                    onTriggered: {
                        recentView.reset()

                        window.moveTabToWindow(browser.tabsModel.indexOf(currentTabMenu.tab), currentTabMenu.tab, null)
                    }
                }
                
                Local.MoveToWindowMenu {
                    model: browser.appWindows
                    targetIndex: currentTabMenu.tab ? browser.tabsModel.indexOf(currentTabMenu.tab) : -1
                    tab: currentTabMenu.tab
                    thisWindow: browser.thisWindow
                    enabled: !browser.incognito && model.length > 1
                    onActionTriggered: recentView.reset()
                }

                Common.CustomizedMenuItem {
                    enabled: currentTabMenu.tab && currentTabMenu.tab.webview && currentTabMenu.tab.url.toString() ? true : false
                    text: currentTabMenu.tab && currentTabMenu.tab.webview ? currentTabMenu.tab.webview.audioMuted ? i18n.tr("Unmute tab") : i18n.tr("Mute tab")
                                : ""
                    iconName: currentTabMenu.tab && currentTabMenu.tab.webview ? currentTabMenu.tab.webview.audioMuted ? "audio-volume-high" : "audio-volume-muted"
                                : ""
                    onTriggered: {
                        currentTabMenu.tab.toggleMute()
                    }
                }

                Common.CustomizedMenuSeparator{}

                Common.CustomizedMenuItem {
                    text: i18n.tr("Close tab")
                    iconName: "close"
                    rightDisplay: currentTabMenu.tabIsCurrent ? shortcutCloseCurrentTab.nativeText : ""
                    onTriggered: {
                        if (currentTabMenu.tabIsCurrent) {
                            internal.closeCurrentTab()
                        } else {
                            internal.closeTab(browser.tabsModel.indexOf(currentTabMenu.tab))
                        }
                    }
                }

                Common.CustomizedMenuSeparator{
                    visible: cancelMenuItem.enabled
                }

                Common.CustomizedMenuItem {
                    id: cancelMenuItem
                    text: i18n.tr("Cancel")
                    enabled: currentTabMenu.type == Common.AdvancedMenu.Type.ContextMenu
                    onTriggered: currentTabMenu.close()
                }

                Common.CustomizedMenuSeparator{
                    visible: configureWindowMenuItem.enabled
                }

                Common.CustomizedMenuItem {
                    id: configureWindowMenuItem

                    text: i18n.tr("Configure window")
                    enabled: !browser.incognito && currentTabMenu.type == Common.AdvancedMenu.Type.BottomAttached
                    onTriggered: {
                        var properties = {
                            windowName: browser.thisWindow.windowName
                            , primary: browser.thisWindow.primary
                            , protectedWindow: browser.thisWindow.protectedWindow
                        }
                        var saveDialog = windowConfigComponent.createObject(browser, properties)
                        if (browser.wide) {
                            saveDialog.openNormal();
                        } else {
                            saveDialog.openBottom();
                        }
                    }
                }
            }
            
            Component {
                id: windowConfigComponent

                RecentView.WindowConfigDialog {
                    id: windowConfigDialog
                    
                    onSave: {
                        browser.thisWindow.windowName = newWindowName
                        browser.thisWindow.protectedWindow = newProtectedWindow

                        if (newPrimary) {
                            browser.thisWindow.setAsPrimary()
                        } else {
                            browser.thisWindow.primary = newPrimary
                        }
                    }
                }
            }

            Component {
                id: sessionRestoreDialogComponent

                Common.ConfirmationDialog {
                    id: sessionRestoreDialog

                    property bool fromPrevious
                    property int targetIndex
                    property string restoreTitle

                    title: i18n.tr("Restore backup session")

                    text: fromPrevious ? i18n.tr('This will restore the backup <b>from previous session "%1"</b>.<br><br>Current session will be saved as <i>"%2"</i>. \
                                                                You can restore it back from the same menu').arg(restoreTitle)
                                            .arg(i18n.tr("Before last restore"))
                                        : i18n.tr('This will restore the backup <b>from current session "%1"</b>.<br><br>Current session will be saved as <i>"%2"</i>. \
                                                                You can restore it back from the same menu').arg(restoreTitle)
                                            .arg(i18n.tr("Before last restore"))
                    confirmButtonText: i18n.tr("Proceed")

                    onConfirm: {
                        recentView.reset()
                        recentlyClosedMenu.close()
                        webbrowserapp.session.restoreFromBackup(fromPrevious, targetIndex)
                    }
                }
            }
        }
    }

    SearchEngine {
        id: currentSearchEngine
        searchPaths: searchEnginesSearchPaths
        filename: settings.searchEngine
    }

    Chrome {
        id: chrome

        // If the chrome is actually hidden since there are cases when it's not
        // i.e. Web content spans the full viewport
        readonly property bool actualAutoHide: contentsContainer.state == "chromeAutoHide"

        wide: browser.wide
        tab: internal.nextTab || tabsModel.currentTab
        tabsModel: browser.tabsModel
        searchUrl: currentSearchEngine.urlTemplate
        enableFocusedSearch: browser.settings.focusedSearch
        searchSuggestionsSettings: browser.settings.searchSuggestions

        incognito: browser.incognito
        bigUIMode: browser.bigUIMode

        showTabsBar: browser.wide && !settings.hideTabsBar
        showFaviconInAddressBar: !browser.wide
        hideTabsButtonNavBar: settings.hideTabsButtonNavBar || showTabsBar

        thisWindow: browser.thisWindow
        windowFactory: browser.windowFactory

        availableHeight: tabContainer.height - (bottomEdgeHandle.enabled ? bottomEdgeHandle.height : 0)

        touchEnabled: internal.hasTouchScreen

        tabsBarDimmed: dropAreaTopCover.containsDrag || dropAreaBottomCover.containsDrag
        tabListMode: recentView.visible

        autoHide: ((!bigUIMode && settings.headerHide == 1) || (bigUIMode && settings.bigUIModeHeaderHide == 1))
                        && (browser.height < units.gu(100) || (browser.height >= units.gu(100) && !browser.wide)) && !floating
        floating: browser.isFullScreen || ( ((!bigUIMode && settings.headerHide >= 2) || (bigUIMode && settings.bigUIModeHeaderHide >= 2))
                                                && (browser.height < units.gu(100) || (browser.height >= units.gu(100) && !browser.wide))
                                          )
        alwaysHidden: (!bigUIMode && settings.headerHide == 3) || (bigUIMode && settings.bigUIModeHeaderHide == 3)
        timesOut: floating && browser.tabsModel.currentTab && !browser.tabsModel.currentTab.empty

        // Don't hide chrome in certain situations
        holdTimeout: editing || drawerOpen || navHistoryOpen || internal.currentDownloadsDialog || internal.currentBookmarkOptionsDialog
                            || leftMenuOpen || rightMenuOpen || contextTabMenuOpen

        property bool hidden: false

        function isCurrentUrlBookmarked() {
            return tab ? BookmarksModel.contains(tab.url) : false
        }
        bookmarked: isCurrentUrlBookmarked()
        onCloseTabRequested: internal.closeCurrentTab()
        onToggleBookmark: {
            toggleBookmarkState()
        }

        function toggleBookmarkState(displayBottom, linkUrl, linkText) {
            var dialogCaller
            if (displayBottom) {
                dialogCaller = rightBottomAnchorItem
            }
            
            var bookmarkUrl
            var bookmarkTitle
            var isBookmarked
            var favicon
            if (linkUrl) {
                bookmarkUrl = linkUrl
                bookmarkTitle = linkText
                isBookmarked = BookmarksModel.contains(linkUrl)
                // TODO: Find a way to get the favicon from a url
                favicon = ""
            } else {
                bookmarkUrl = tab.url
                bookmarkTitle = tab.title
                isBookmarked = isCurrentUrlBookmarked()
                favicon = (UrlUtils.schemeIs(tab.icon, "image") && UrlUtils.hostIs(tab.icon, "favicon")) ? tab.icon.toString().substring(("image://favicon/").length) : tab.icon
            }

            if (isBookmarked) BookmarksModel.remove(bookmarkUrl)
            // QtWebEngine icons are provided as e.g. image://favicon/https://duckduckgo.com/favicon.ico
            else internal.addBookmark(bookmarkUrl, bookmarkTitle ? bookmarkTitle : i18n.tr("Untitled"), favicon, dialogCaller)
        }
        onToggleDownloads: {
            internal.showDownloadsDialog(caller)
        }

        onWebviewChanged: {
            bookmarked = isCurrentUrlBookmarked()
            if (recentView.state !== "shown" && !timesOut) changeChromeState("shown")
        }
        Connections {
            target: chrome.tab
            onUrlChanged: chrome.bookmarked = chrome.isCurrentUrlBookmarked()
        }
        Connections {
            target: BookmarksModel
            onCountChanged: chrome.bookmarked = chrome.isCurrentUrlBookmarked()
        }

        onSwitchToTab: internal.switchToTab(index, true)
        onRequestNewTab: internal.openUrlInNewTab(url, makeCurrent, true, index, sourceTab)
        onTabClosed: internal.closeTab(index, moving)
        onOpenRecentView: browser.openRecentView()

        onFindInPageModeChanged: {
            if (!chrome.findInPageMode) internal.resetFocus()
            else chrome.forceActiveFocus()
        }

        anchors {
            left: parent.left
            leftMargin: browser.leftPadding
            right: parent.right
        }
        
        drawerMenu: Component {
            Local.DrawerMenu {
                id: menuDrawer

                bigUIMode: browser.bigUIMode
                availWidth: browser.width

                bottomActions: [
                    Common.RowMenuAction {
                        objectName: "browserTabs"
                        text: i18n.tr("%1 (%2)").arg(i18n.tr("View tabs")).arg(browser.tabsViewShortcutText)
                        icon.name: "browser-tabs"
                        closeMenuOnTrigger: true
                        onTriggered: {
                            shortCutTabsView.activated()
                        }
                    }
                    , Common.RowMenuAction {
                        objectName: "findinpage"
                        text: i18n.tr("%1 (%2)").arg(i18n.tr("Find in page...")).arg(browser.findShortcutText)
                        icon.name: "find"
                        enabled: !newTabViewLoader.active
                        closeMenuOnTrigger: true
                        onTriggered: shortcutFind.activated()
                    }
                    , Common.RowMenuAction {
                        objectName: "gofullscreen"
                        text: i18n.tr("%1 (%2)").arg(browser.isFullScreen ? i18n.tr("Exit full screen") : i18n.tr("Go full screen")).arg(shortcutFullscreen.nativeText)
                        icon.name: browser.isFullScreen ? "view-restore" : "view-fullscreen"
                        closeMenuOnTrigger: true
                        onTriggered: shortcutFullscreen.activated()
                    }
                    , Common.RowMenuAction {
                        objectName: "share"
                        text: i18n.tr("Share")
                        icon.name: "share"
                        closeMenuOnTrigger: true
                        enabled: (contentHandlerLoader.status == Loader.Ready) &&
                                 chrome.tab && chrome.tab.url.toString()
                        onTriggered: internal.shareLink(chrome.tab.url, chrome.tab.title)
                    }
                    , Common.RowMenuAction {
                        objectName: "settings"
                        text: i18n.tr("%1 (%2)").arg(i18n.tr("Settings")).arg(shortcutSettings.nativeText)
                        icon.name: "settings"
                        closeMenuOnTrigger: true
                        onTriggered: {
                            if (!leftPageDrawerLoader.isSettings) {
                                shortcutSettings.activated()
                            }
                        }
                    }
                    , Common.RowMenuAction {
                        objectName: "closeDrawer"
                        text: i18n.tr("Close drawer")
                        icon.name: "close"
                        enabled: browser.height - menuDrawer.height <= chrome.height || menuDrawer.shownAtBottom
                        closeMenuOnTrigger: true
                    }
                ]

                Common.CustomizedMenuItem {
                    id: menuItemNewTab
                    objectName: "newTab"
                    text: i18n.tr("New tab")
                    iconName: "tab-new"
                    rightDisplay: shortcutNewTab.nativeText //"Ctrl+T"
                    onTriggered: {
                        internal.openUrlInNewTab("", true)
                    }
                }
                Common.CustomizedMenuItem {
                    id: menuItemNewWindow
                    objectName: "newwindow"
                    text: i18n.tr("New window")
                    iconName: "browser-tabs"
                    rightDisplay: shortcutNewWindow.nativeText //"Ctrl+N"
                    onTriggered: shortcutNewWindow.activated()
                }
                Common.CustomizedMenuItem {
                    id: menuItemNewPWindow
                    objectName: "newprivatewindow"
                    text: i18n.tr("New private window")
                    iconName: "private-browsing"
                    rightDisplay: shortcutNewPrivateWindow.nativeText //"Ctrl+Shift+P"
                    onTriggered: shortcutNewPrivateWindow.activated()
                }

                Common.CustomizedMenuSeparator{
                    enabled: menuItemBookmarks.enabled || menuItemHistory.enabled || menuItemDownloads.enabled
                }

                Common.CustomizedMenuItem {
                    id: menuItemBookmarks
                    objectName: "bookmarks"
                    text: i18n.tr("Bookmarks")
                    iconName: "bookmark"
                    tooltipText: i18n.tr("%1 (%2)").arg(i18n.tr("Manage bookmarks")).arg(shortcutBookmarks.nativeText)
                    rightDisplay: Component {
                        RowLayout {
                            spacing: 0
                            Common.MenuToolButton {
                                Layout.preferredHeight: units.gu(3)
                                iconName: chrome.bookmarked ? "starred" : "non-starred"
                                iconColor: chrome.bookmarked ? theme.palette.normal.focus : theme.palette.normal.baseText
                                text: chrome.bookmarked ? i18n.tr("Remove") : i18n.tr("Add")
                                visible: currentWebview.url.toString() ? true : false
                                tooltipText: i18n.tr("%1 (%2)").arg(chrome.bookmarked ? i18n.tr("Remove this tab from bookmarks") : i18n.tr("Bookmark this tab"))
                                                            .arg(shortcutBookmarkTab.nativeText)
                                onClicked: {
                                    if (!chrome.bookmarked) {
                                        menuDrawer.close()
                                    }
                                    chrome.toggleBookmarkState(menuDrawer.shownAtBottom)
                                }
                            }
                        }
                    }

                    onTriggered: {
                        if (!leftPageDrawerLoader.isBookmarks) {
                            shortcutBookmarks.activated()
                        }
                    }
                }
                Common.CustomizedMenuItem {
                    id: menuItemHistory
                    objectName: "history"
                    text: i18n.tr("History")
                    iconName: "history"
                    rightDisplay: shortcutHistory.nativeText //"Ctrl+H"
                    onTriggered: {
                        if (!leftPageDrawerLoader.isHistory) {
                            shortcutHistory.activated()
                        }
                    }
                }

                Common.CustomizedMenuItem {
                    id: menuItemDownloads
                    objectName: "downloads"
                    text: i18n.tr("Downloads")
                    iconName: "save-to"
                    enabled: contentHandlerLoader.status == Loader.Ready
                    rightDisplay: shortcutDownloads.nativeText //"Ctrl+J"
                    onTriggered: {
                        if (!leftPageDrawerLoader.isDownloads) {
                            shortcutDownloads.activated()
                        }
                    }
                }

                Common.CustomizedMenuSeparator{
                    enabled: menuItemZoom.enabled || menuItemDesktopSite.enabled
                }

                Common.CustomizedMenuItem {
                    id: menuItemZoom
                    objectName: "zoom"
                    text: i18n.tr("Zoom")
                    iconName: "zoom-in"
                    enabled: currentWebview && (currentWebview.url.toString() !== "")
                    topPadding: units.gu(1)
                    bottomPadding: units.gu(1)

                    onTriggered: currentWebview.showZoomMenu()

                    rightDisplay: Component {
                        RowLayout {
                            spacing: 0
                            Common.MenuToolButton {
                                Layout.preferredWidth: units.gu(5)
                                Layout.preferredHeight: units.gu(5)
                                enabled: currentWebview.zoomController.saveAvailable
                                visible: showShortcuts && enabled
                                tooltipText: i18n.tr("Save zoom settings")
                                iconName: "save"
                                onClicked: {
                                    menuItemZoom.menu.close()
                                    currentWebview.zoomController.save()
                                }
                            }
                            Common.MenuToolButton {
                                Layout.preferredWidth: units.gu(5)
                                Layout.preferredHeight: units.gu(5)
                                enabled: currentWebview.zoomController.resetAvailable
                                visible: showShortcuts & enabled
                                tooltipText: i18n.tr("%1 (%2)").arg(i18n.tr("Reset zoom settings")).arg(currentWebview.resetZoomShortCutText)
                                iconName: "reset"
                                onClicked: currentWebview.zoomController.resetSaveFit()
                            }
                            Common.ZoomSpinBox {
                                controller: currentWebview.zoomController
                                Layout.preferredWidth: units.gu(16)
                                Layout.preferredHeight: units.gu(5)
                            }
                        }
                    }
                }

                Common.CustomizedMenuItem {
                    id: menuItemDesktopSite

                    objectName: "desktopSite"
                    text: actionToggleSiteVersion.desktopSwitchMode ? i18n.tr("Desktop site") : i18n.tr("Mobile site")
                    iconName: actionToggleSiteVersion.desktopSwitchMode ? "computer-symbolic" : "phone-smartphone-symbolic"

                    onTriggered: {
                        rightDisplayItem.checked = !rightDisplayItem.checked
                        actionToggleSiteVersion.trigger(false, null)
                    }

                    rightDisplay: Component {
                        Switch {
                            id: switchDesktopSite

                            onClicked: {
                                if (actionToggleSiteVersion.desktopSwitchMode) {
                                    browser.tabsModel.currentTab.forceDesktopSite = checked
                                } else {
                                    browser.tabsModel.currentTab.forceMobileSite = checked
                                }
                                menuDrawer.close()
                            }
                        }
                    }

                    Binding {
                        target: menuItemDesktopSite.rightDisplayItem
                        property: "checked"
                        value: actionToggleSiteVersion.desktopSwitchMode ? browser.tabsModel.currentTab.forceDesktopSite : browser.tabsModel.currentTab.forceMobileSite
                    }
                }

                Common.CustomizedMenuItem {
                    id: menuItemEasyTargetMode

                    objectName: "easyTargetMode"
                    text: i18n.tr("EasyTarget Mode")
                    iconName: "gps"
                    visible: browser.settings.enableBigUIModeToggle

                    onTriggered: {
                        browser.bigUIMode = !browser.bigUIMode
                    }

                    rightDisplay: Component {
                        Switch {
                            id: switchEasyTargetMode

                            onClicked: {
                                browser.bigUIMode = !browser.bigUIMode
                                menuDrawer.close()
                            }
                        }
                    }

                    Binding {
                        target: menuItemEasyTargetMode.rightDisplayItem
                        property: "checked"
                        value: browser.bigUIMode
                    }
                }

                Common.CustomizedMenuSeparator{
                    enabled: menuMore.enabled
                }

                Common.CustomizedMenu {
                    id: menuMore
                    objectName: "more"
                    title: i18n.tr("More")
                    iconName: "other-actions"
                    enabled: menuItemSaveAs.enabled || viewSourceMenuItem.enabled

                    Common.CustomizedMenuItem {
                        id: menuItemSaveAs
                        objectName: "save"
                        text: i18n.tr("Save page as...")
                        iconName: "save-as"
                        enabled: currentWebview && (currentWebview.url.toString() !== "")
                        rightDisplay: browser.saveAsShortcutText //"Ctrl+S"
                        onTriggered: {
                            browser.savePageRequested()
                        }
                    }
                    Common.CustomizedMenuItem {
                        id: viewSourceMenuItem
                        objectName: "view source"
                        text: i18n.tr("View source")
                        iconName: "text-xml-symbolic"
                        rightDisplay: browser.viewSourceShortcutText //"Ctrl+U"
                        enabled: currentWebview && (currentWebview.url.toString() !== "") && (currentWebview.url.toString().substring(0,12) !== "view-source:")
                        onTriggered: {
                            browser.viewSourceRequested()
                        }
                    }
                }

                Common.CustomizedMenuSeparator{
                    enabled: quitMenuItem.enabled
                }

                Common.CustomizedMenuItem {
                    id: quitMenuItem
                    objectName: "quitApp"
                    text: i18n.tr("Quit")
                    iconName: "close"
                    rightDisplay: shortcutQuitApp.nativeText //Ctrl+Q
                    

                    onTriggered: {
                        Qt.quit()
                    }
                }
            }
        }

        canSimplifyText: !browser.wide
        searchEngine: currentSearchEngine
        availableHeightBelowChrome: tabContainer.height - chrome.height / 2

        onDownPressed: {
            if (!incognito && (newTabViewLoader.status == Loader.Ready)) {
                newTabViewLoader.forceActiveFocus()
            }
        }
        onSuggestionSwitchTab: internal.switchToTab(tabIndex)
        onSuggestionsActivated: {
            browser.currentWebview.url = itemData
            tabContainer.forceActiveFocus()
            chrome.requestedUrl = itemData
        }
        onSuggestionsEscaped: internal.resetFocus()

        Keys.onEscapePressed: {
            if (chrome.findInPageMode) {
                chrome.findInPageMode = false
            } else {
                internal.resetFocus()
            }
        }

        Connections {
            target: browser.currentWebview
            onLoadingChanged: {
                if (browser.currentWebview.loading && !recentView.visible && !chrome.timesOut) {
                    chrome.changeChromeState("shown")
                } else if (browser.currentWebview.isFullScreen) {
                    chrome.state = "hidden"
                }
            }
        }

        Connections {
            target: recentView

            onStateChanged: {
                if (!chrome.timesOut) {
                    if (target.state === "") {
                        chrome.changeChromeState("shown")
                    }
                }
            }
        }
    }

    Item {
        id: hoverShowHeader

        z: 1000
        visible: chrome.floating && chrome.state == "hidden"
        height: units.gu(2)
        anchors {
            top: parent.top
            left: parent.left
            leftMargin: browser.leftPadding
            right: parent.right
        }

        HoverHandler {
            id: hoverHander

            onHoveredChanged: {
                if (hovered) {
                    showHeaderOnHoverTimer.restart()
                } else {
                    showHeaderOnHoverTimer.stop()
                }
            }
        }

        Timer {
            id: showHeaderOnHoverTimer
            running: false
            interval: 200
            onTriggered: chrome.state = "shown"
        }
    }

    Loader {
        active: settings.appWideScrollPositioner
        z: contentsContainer.z + 1
        anchors {
            right: parent.right
            rightMargin: units.gu(2)
            bottom: parent.bottom
            bottomMargin: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode) + units.gu(3) : units.gu(3)
        }

        sourceComponent: Common.ScrollPositioner {
            id: scrollPositioner

            target: browser.currentWebview
            mode: chrome.scrollTracker.scrollingUp ? "Up" : "Down"
            forceHide: recentView.visible || browser.currentWebview && browser.currentWebview.isFullScreen

            onPressAndHold: {
                if (mode == "Up") {
                    if (browser.tabsModel.currentTab.webviewPulledDown) {
                        browser.tabsModel.currentTab.pullUpWebview()
                        Common.Haptics.play()
                    }
                } else {
                    if (!browser.tabsModel.currentTab.webviewPulledDown) {
                        browser.tabsModel.currentTab.pullDownWebview()
                        Common.Haptics.play()
                    }
                }
            }
        }
    }

    onWideChanged: {
        recentView.reset()

        if (settings.autoDeskMobSwitch && settings.autoDeskMobSwitchReload) {
            if (currentWebview && currentWebview.context.__ua.calcScreenSize() == "large") {
                    tabsModel.currentTab.reload()
            }
        }
    }

    Common.GoIndicator {
        id: goForwardIcon

        iconName: "go-next"
        shadowColor: UbuntuColors.ash
        swipeProgress: bottomBackForwardHandle.swipeProgress
        enabled: browser.currentWebview ? browser.currentWebview.canGoForward
                                        : false
        anchors {
            right: parent.right
            rightMargin: units.gu(3)
            verticalCenter: parent.verticalCenter
        }
    }

    Common.GoIndicator {
        id: goBackIcon

        iconName: "go-previous"
        shadowColor: UbuntuColors.ash
        swipeProgress: bottomBackForwardHandle.swipeProgress
        enabled: browser.currentWebview ? browser.currentWebview.canGoBack
                                        : false
        anchors {
            left: parent.left
            leftMargin: browser.leftPadding + units.gu(3)
            verticalCenter: parent.verticalCenter
        }
    }

    Loader {
        active: browser.bigUIMode
        asynchronous: true
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        sourceComponent: Rectangle {
            color: theme.palette.normal.foreground
            height: units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
        }
    }

    RowLayout {
        id: bottomGestures

        property real sideSwipeAreaWidth: browser.currentWebview && !browser.currentWebview.isFullScreen ?
                                                        browser.width * (browser.width > browser.height ? 0.15 : 0.30)
                                                        : 0

        anchors {
            bottom: parent.bottom
            left: parent.left
            leftMargin: browser.leftPadding
            right: parent.right
            top: parent.top
        }

        Loader {
            id: leftSwipeAreaLoader

            Layout.alignment: Qt.AlignLeft | Qt.AlignBottom

            active: browser.settings.webviewSideSwipe
            asynchronous: true
            visible: status == Loader.Ready
            sourceComponent: Common.BottomSwipeArea {
                model: browser.settings.webviewQuickActions[0] ? actionsFactory.getActionsModel(browser.settings.webviewQuickActions[0])
                                : []
                implicitWidth: bottomGestures.sideSwipeAreaWidth
                triggerSignalOnQuickSwipe: true
                enableQuickActions: browser.settings.webviewEnableQuickActions
                enableQuickActionsDelay: browser.settings.webviewQuickSideSwipe
                                                || (!browser.settings.webviewQuickSideSwipe && browser.settings.webviewQuickActionEnableDelay)
                edge: Common.BottomSwipeArea.Edge.Left
                bigUIMode: browser.bigUIMode
                maxQuickActionsHeightInInch: browser.settings.webviewQuickActionsHeight
                availableHeight: contentsContainer.height
                availableWidth: contentsContainer.width
                implicitHeight: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
                                                  : units.gu(browser.settings.bottomGesturesAreaHeight)

                onTriggered: {
                    if (!browser.settings.webviewEnableQuickActions
                            || (browser.settings.webviewEnableQuickActions && browser.settings.webviewQuickSideSwipe)) {
                        chrome.triggerLeftAction(true)
                        if (chrome.timesOut && !chrome.alwaysHidden) {
                            chrome.state = "shown"
                        }
                        Common.Haptics.play()
                    }
                }
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignBottom

            MouseArea {
                id: bottomEdgeHint

                readonly property alias color: recVisual.color
                readonly property real defaultHeight: units.gu(0.5)

                hoverEnabled: true
                height: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
                                          : defaultHeight
                visible: bottomEdgeHandle.enabled && !internal.hasMouse
                                && ((!browser.bigUIMode && !browser.settings.hideBottomHint)
                                            || (browser.bigUIMode && browser.settings.bigUIModeBottomHint))
                opacity: recentView.visible ? 0 : 1
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                    bottomMargin: browser.bigUIMode ? 0
                                                    : ((chrome.state == "shown" || chrome.timesOut)
                                                        && browser.currentWebview && !browser.currentWebview.isFullScreen) ? defaultHeight
                                                                                                               : -height

                    Behavior on bottomMargin {
                        UbuntuNumberAnimation {}
                    }
                }

                Behavior on opacity {
                    UbuntuNumberAnimation {}
                }

                onClicked: shortCutTabsView.activated()

                Rectangle {
                    id: recVisual
                    color: bottomEdgeHint.containsMouse ? UbuntuColors.silk : UbuntuColors.ash
                    radius: height / 2
                    height: bottomEdgeHint.containsMouse ? units.gu(1) : units.gu(0.5)
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        leftMargin: browser.settings.webviewSideSwipe ? 0 : bottomGestures.sideSwipeAreaWidth
                        right: parent.right
                        rightMargin: browser.settings.webviewSideSwipe ? 0 : bottomGestures.sideSwipeAreaWidth
                    }
                }
            }

            Common.HorizontalSwipeHandle {
                id: bottomBackForwardHandle
                objectName: "bottomBackForwardHandle"

                leftAction: goBackIcon
                rightAction: goForwardIcon
                immediateRecognition: true
                usePhysicalUnit: browser.settings.physicalForGestures
                height: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
                                          : units.gu(browser.settings.bottomGesturesAreaHeight)
                swipeHoldDuration: 700
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }

                enabled: browser.settings.webviewHorizontalSwipe && bottomEdgeHandle.enabled
                rightSwipeHoldEnabled: browser.currentWebview ? browser.currentWebview.canGoBack
                                                          : false
                leftSwipeHoldEnabled: browser.currentWebview ? browser.currentWebview.canGoForward
                                                         : false
                leftSwipeActionEnabled: goForwardIcon.enabled
                rightSwipeActionEnabled: goBackIcon.enabled
                onRightSwipe:  internal.historyGoBack()
                onLeftSwipe:  internal.historyGoForward()
                onLeftSwipeHeld: chrome.showForwardNavHistory(true, navHistoryMargin)
                onRightSwipeHeld: chrome.showBackNavHistory(true, navHistoryMargin)
                onPressedChanged: if (pressed) Common.Haptics.playSubtle()

                Item {
                    id: navHistoryMargin
                    height: units.gu(10)
                    anchors {
                        left: parent.left
                        right: parent.right
                        bottom: parent.top
                    }
                }
            }

            Common.SwipeGestureHandler {
                id: bottomEdgeHandle
                objectName: "bottomEdgeHandle"

                usePhysicalUnit: browser.settings.physicalForGestures
                immediateRecognition: !bottomBackForwardHandle.enabled
                height: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
                                          : units.gu(browser.settings.bottomGesturesAreaHeight)
                swipeHoldDuration:  500
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }

                enabled: recentView.state == "" &&
                         browser.currentWebview &&
                         (Screen.orientation == Screen.primaryOrientation)

                onSwipeHeld: {
                    if (browser.settings.webviewBottomHoldTabsSearch && stage >= 3) {
                        recentView.state = "shown"
                        recentToolbar.state = "shown"
                        tabslist.focusInput()
                        Common.Haptics.play()
                    }
                }

                onDraggingChanged: {
                    if (!dragging && towardsDirection) {
                        if (browser.currentWebview && browser.currentWebview.isFullScreen) {
                            if (browser.thisWindow && stage > 0) {
                                shortcutExitFullScreen.activated()
                            } 
                        } else if (stage == 1 || stage == 2) {
                            if (tabsModel.count > 1) {
                                    if (lastUsedTabPreviewLoader.item) {
                                        lastUsedTabPreviewLoader.commit()
                                    }
                            } else {
                                recentView.state = "shown"
                                recentToolbar.state = "shown"
                            }
                        } else if (stage >= 3) {
                            recentView.state = "shown"
                            recentToolbar.state = "shown"
                        }
                    }
                }
            }
        }

        Loader {
            id: rightSwipeAreaLoader

            Layout.alignment: Qt.AlignRight | Qt.AlignBottom

            active: browser.settings.webviewSideSwipe
            asynchronous: true
            visible: status == Loader.Ready
            sourceComponent: Common.BottomSwipeArea {
                model: browser.settings.webviewQuickActions[1] ? actionsFactory.getActionsModel(browser.settings.webviewQuickActions[1])
                                : []
                implicitWidth: bottomGestures.sideSwipeAreaWidth
                triggerSignalOnQuickSwipe: true
                enableQuickActions: browser.settings.webviewEnableQuickActions
                enableQuickActionsDelay: browser.settings.webviewQuickSideSwipe
                                                || (!browser.settings.webviewQuickSideSwipe && browser.settings.webviewQuickActionEnableDelay)
                edge: Common.BottomSwipeArea.Edge.Right
                bigUIMode: browser.bigUIMode
                maxQuickActionsHeightInInch: browser.settings.webviewQuickActionsHeight
                availableHeight: contentsContainer.height
                availableWidth: contentsContainer.width
                implicitHeight: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
                                                  : units.gu(browser.settings.bottomGesturesAreaHeight)

                onTriggered: {
                    if (!browser.settings.webviewEnableQuickActions
                            || (browser.settings.webviewEnableQuickActions && browser.settings.webviewQuickSideSwipe)) {
                        chrome.triggerRightAction(true)
                        if (chrome.timesOut && !chrome.alwaysHidden) {
                            chrome.state = "shown"
                        }
                        Common.Haptics.play()
                    }
                }
            }
        }
    }

    Item {
        id: rightBottomAnchorItem

        height: browser.osk.visible ? browser.osk.height : units.gu(4)
        width: {
            if (browser.wide && rightSwipeAreaLoader.item) {
                return rightSwipeAreaLoader.item.width
            }

            return parent.width
        }

        anchors {
            bottom: parent.bottom
            right: parent.right
        }
    }

    Common.PageDrawerLoader {
        id: leftPageDrawerLoader

        readonly property int historyIndex: 0
        readonly property int bookmarksIndex: 1
        readonly property int downloadsIndex: 2
        readonly property int settingsIndex: 3

        readonly property bool isHistory: currentPageIndex == historyIndex
        readonly property bool isBookmarks: currentPageIndex == bookmarksIndex
        readonly property bool isDownloads: currentPageIndex == downloadsIndex
        readonly property bool isSettings: currentPageIndex == settingsIndex

        function displayHistory() {
            openWithPageIndex(historyIndex)
        }

        function displayBookmarks() {
            openWithPageIndex(bookmarksIndex)
        }

        function displayDownloads(synchronous) {
            if (synchronous) {
                openWithPageIndexSynchronously(downloadsIndex)
            } else {
                openWithPageIndex(downloadsIndex)
            }
        }

        function displaySettings() {
            openWithPageIndex(settingsIndex)
        }

        Connections {
            target: browser.currentWebview

            onIsFullScreenChanged: {
                if (target.isFullScreen && leftPageDrawerLoader.opened) {
                    leftPageDrawerLoader.close()
                }
            }
        }

        edge: Qt.LeftEdge
        pagesModel: [
            Common.PageDrawerItem {
                pageId: "history"
                pageComponent: historyViewComponent
                iconName: "history"
                title: i18n.tr("History")
                description: i18n.tr("View and manage your full browsing history")
                shortcutText: shortcutHistory.nativeText
            }
            , Common.PageDrawerItem {
                pageId: "bookmarks"
                pageComponent: bookmarksViewComponent
                iconName: "bookmark"
                title: i18n.tr("Bookmarks")
                description: i18n.tr("View and manage all your bookmarked sites")
                shortcutText: shortcutBookmarks.nativeText
            }
            , Common.PageDrawerItem {
                pageId: "downloads"
                pageComponent: downloadsViewComponent
                iconName: "save-to"
                title: i18n.tr("Downloads")
                description: i18n.tr("View and manage all your downloaded files")
                shortcutText: shortcutDownloads.nativeText
            }
            , Common.PageDrawerItem {
                pageId: "settings"
                pageComponent: settingsViewComponent
                iconName: "settings"
                title: i18n.tr("Settings")
                description: i18n.tr("View and adjust available settings")
                shortcutText: shortcutSettings.nativeText
            }
        ]
        onForcePinnedChanged: browser.thisWindow.leftSidePanePinned = forcePinned
        onLoseFocus: internal.resetFocus()
        
        Binding {
            target: leftPageDrawerLoader
            property: "forcePinned"
            value: browser.thisWindow.leftSidePanePinned
        }
    }

    Component {
        id: historyViewComponent

        HistoryPage {
            onHistoryEntryClicked: {
                if (browser.leftPageDrawer.floating || browser.leftPageDrawer.isFullWidth) {
                    leftPageDrawerLoader.close()
                }
                internal.openUrlInNewTab(url, true)
            }

            Component.onCompleted: loadModel()
        }
    }

    Component {
        id: bookmarksViewComponent

        BookmarksPage {
            onBookmarkEntryClicked: {
                if (browser.leftPageDrawer.floating || browser.leftPageDrawer.isFullWidth) {
                    leftPageDrawerLoader.close()
                }
                internal.openUrlInNewTab(url, true)
            }
        }
    }

    Component {
        id: downloadsViewComponent

        Common.CustomDownloadsPage {
            incognito: browser.incognito

            onDone: leftPageDrawerLoader.close()
        }
    }

    Component {
        id: settingsViewComponent

        CustomSettingsPage {
            settingsObject: settings
            onClearCache: {
                console.log("Cache to delete " + JSON.stringify(sig_itemsToDelete))

                // clear Http cache
                if (sig_itemsToDelete.includes("httpCache")) {
                    SharedWebContext.sharedContext.clearHttpCache();
                    SharedWebContext.sharedIncognitoContext.clearHttpCache();
                }

                var cacheLocationUrl = Qt.resolvedUrl(cacheLocation);
                var dataLocationUrl = Qt.resolvedUrl(dataLocation);

                // clear favicons
                if (sig_itemsToDelete.includes("favicons")) {
                    FileOperations.removeDirRecursively(cacheLocationUrl + "/favicons");
                }

                // remove captures
                if (sig_itemsToDelete.includes("tabPreviews")) {
                    FileOperations.removeDirRecursively(cacheLocationUrl + "/captures");
                }

                // App Cache
                if (sig_itemsToDelete.includes("appCache")) {
                    FileOperations.removeDirRecursively(cacheLocationUrl + "/Cache");
                    FileOperations.removeDirRecursively(cacheLocationUrl + "/qmlcache");
                }

                // Application Cache
                if (sig_itemsToDelete.includes("webApplicationCache")) {
                    FileOperations.removeDirRecursively(dataLocationUrl + "/Application Cache");
                }

                // GPU Cache
                if (sig_itemsToDelete.includes("webGPUCache")) {
                    FileOperations.removeDirRecursively(dataLocationUrl + "/GPUCache");
                }

                // File System
                if (sig_itemsToDelete.includes("webFileSystem")) {
                    FileOperations.removeDirRecursively(dataLocationUrl + "/File System");
                }

                // Local Storage
                if (sig_itemsToDelete.includes("webLocalStorage")) {
                    FileOperations.removeDirRecursively(dataLocationUrl + "/Local Storage");
                }

                // Service WorkerScript
                if (sig_itemsToDelete.includes("webServiceWorker")) {
                    FileOperations.removeDirRecursively(dataLocationUrl + "/Service Worker");
                }

                // Visited Links
                if (sig_itemsToDelete.includes("webVisitedLinks")) {
                    FileOperations.remove(dataLocationUrl + "/Visited Links");
                }
            }

            onClearAllCookies: {
                BrowserUtils.deleteAllCookiesOfProfile(SharedWebContext.sharedContext);
                BrowserUtils.deleteAllCookiesOfProfile(SharedWebContext.sharedIncognitoContext);
            }
        }
    }

    property Component tabComponent
    Loader {
        source: "TabComponent.qml"
        onLoaded: tabComponent = item
    }

    QtObject {
        id: internal

        property int nextTabIndex: -1
        readonly property var nextTab: (nextTabIndex > -1) ? tabsModel.get(nextTabIndex) : null
        onNextTabChanged: {
            if (nextTab) {
                nextTab.aboutToShow()
            }
        }

        readonly property bool hasMouse: (miceModel.count + touchPadModel.count) > 0
        readonly property bool hasTouchScreen: touchScreenModel.count > 0

        // Ref: https://code.google.com/p/chromium/codesearch#chromium/src/components/ui/zoom/page_zoom_constants.cc
        //readonly property var zoomFactors: [0.25, 0.333, 0.5, 0.666, 0.75, 0.9, 1.0, 1.1, 1.25, 1.5, 1.75, 2.0, 2.5, 3.0, 4.0, 5.0]

        function instantiateShareComponent() {
            var component = Qt.createComponent("../Share.qml")
            if (component.status === Component.Ready) {
                var share = component.createObject(browser)
                share.onDone.connect(share.destroy)
                return share
            }
            return null
        }

        function shareLink(url, title) {
            var share = instantiateShareComponent()
            if (share) share.shareLink(url, title)
        }

        function shareText(text) {
            var share = instantiateShareComponent()
            if (share) share.shareText(text)
        }

        function openUrlInNewTab(url, setCurrent, load, index, sourceTab) {
            let _foundTabIndex = browser.settings.openLinkInExistingTab ? browser.tabsModel.searchSameUrl(url) : -1
            if (_foundTabIndex == -1) { // Not found
                let _initProperties
                if (sourceTab) {
                    _initProperties = {"initialUrl": url, "forceDesktopSite": sourceTab.forceDesktopSite
                                        , "forceMobileSite": sourceTab.forceMobileSite}
                } else {
                    _initProperties = {"initialUrl": url}
                }
                load = typeof load !== 'undefined' ? load : true
                let _tab = internal.createTabHelper(_initProperties)
                addTab(_tab, setCurrent, index)

                // Make sure background tab is next to the current tab based on recency
                if (!setCurrent) _tab.lastUseTimestamp = browser.tabsModel.currentTab.lastUseTimestamp - 1

                if (load) {
                    _tab.load()
                }
                if (!url.toString()) {
                    focusAddressBar()
                }
            } else {
                if (setCurrent) {
                    internal.switchToTab(_foundTabIndex, true)
                } else {
                    // Make the found tab the next tab when switching by recency
                    browser.tabsModel.get(_foundTabIndex).lastUseTimestamp = browser.tabsModel.currentTab.lastUseTimestamp - 1
                    // Move the found tab next to the current tav
                    browser.tabsModel.moveTab(_foundTabIndex, browser.tabsModel.currentIndex + 1)
                }
            }
        }

        function addTab(tab, setCurrent, index) {
            if (index === undefined) index = tabsModel.add(tab)
            else index = tabsModel.insert(tab, index)
            if (setCurrent) {
                chrome.requestedUrl = tab.initialUrl
                switchToTab(index, true)
            }
        }

        function buildContextProperties(properties) {
            if (properties === undefined) {
                properties = {};
            }

            properties["bottomEdgeHandle"] = bottomEdgeHandle;
            properties["browser"] = browser;
            properties["chrome"] = chrome;
            properties["contentHandlerLoader"] = contentHandlerLoader;
            properties["downloadDialogLoader"] = downloadDialogLoader;
            //properties["filePickerLoader"] = filePickerLoader;
            properties["internal"] = internal;
            properties["recentView"] = recentView;
            properties["tabsModel"] = tabsModel;

            return properties;
        }

        function createTabHelper(properties) {
            return Reparenter.createObject(tabComponent, tabContainer, internal.buildContextProperties(properties));
        }

        function closeTab(index, moving, doNotSave) {
            moving = moving === undefined ? false : moving;

            var tab = tabsModel.get(index)
            tabsModel.remove(index)

            if (tab) {
                // When moving a tab between windows, don't close the tab as it has been moved
                // and don't add to the closed tabs history
                if (!moving) {
                    if (!doNotSave && !incognito && tab.url.toString().length > 0) {
                        browser.deletedClosedTabHistory = [] // Clear deleted history when new tabs are closed

                        // For triggering property change on closedTabHistory
                        var temp = browser.closedTabHistory.slice()

                        // Delete last item when max is reached
                        if (temp.length == browser.maxClosedTabHistory) {
                            var lastItem = temp.pop()
                        }

                        temp.unshift({
                            state: serializeTabState(tab),
                            index: index
                        })

                        browser.closedTabHistory = temp.slice()
                    }

                    tab.close()
                }
            }
            if (tabsModel.currentTab) {
                tabsModel.currentTab.load()
            }
            if (tabsModel.count === 0) {
                internal.openUrlInNewTab("", true)
                recentView.reset()
            }
        }

        function closeCurrentTab() {
            if (tabsModel.count > 0) {
                closeTab(tabsModel.currentIndex)
            }
        }

        function closeTabsWithUrl(url) {
            var i;
            for (i = 0; i < tabsModel.count; i++) {
                if (tabsModel.get(i).url === url)
                    closeTab(i);
            }
        }

        function undoCloseTab(indexToRestore) {
            if (!incognito && browser.closedTabHistory.length > 0) {
                // For triggering property change on closedTabHistory
                var temp = browser.closedTabHistory.slice()
                var tabInfo
                if (indexToRestore && indexToRestore > -1) {
                    tabInfo = temp[indexToRestore]
                    temp.splice(indexToRestore, 1);
                } else {
                    tabInfo = temp.shift()
                }
                browser.closedTabHistory = temp.slice()
                var tab = restoreTabState(tabInfo.state)
                addTab(tab, true, tabInfo.index)
                tab.load()
            }
        }

        function savePageAs() {
            currentWebview.runJavaScript("document.contentType", function(docContentType) {
                var savePageDialog = PopupUtils.open(Qt.resolvedUrl("../SavePageDialog.qml"), currentWebview);
                savePageDialog.saveAsHtml.connect( function() { currentWebview.triggerWebAction(WebEngineView.SavePage);});
                savePageDialog.canSaveAsHtml = (docContentType.indexOf("text/") === 0) && ! UrlUtils.isPdfViewerExtensionUrl(currentWebview.url);

                savePageDialog.download.connect( function() {currentWebview.runJavaScript("var link = document.createElement('a');link.download='';link.href='%1';link.click();".arg(UrlUtils.isPdfViewerExtensionUrl(currentWebview.url) ? currentWebview.url.toString().substr(UrlUtils.getPdfViewerExtensionUrlPrefix().length) : currentWebview.url));});
                savePageDialog.canDownload = (docContentType !== "text/html") || (UrlUtils.isPdfViewerExtensionUrl(currentWebview.url) && ! currentWebview.url.toString().substr(UrlUtils.getPdfViewerExtensionUrlPrefix().length).indexOf("file://%1".arg(currentWebview.profile.downloadPath)) === 0);

                // the filename of the PDF is determined from the title (replace not allowed / problematic chars with '_')
                // the QtWebEngine does give the filename (.mhtml) for the SavePage action with that pattern as well
                savePageDialog.saveAsPdf.connect( function() { currentWebview.printToPdf("%1/pdf_tmp/%2.pdf".arg(cacheLocation).arg(currentWebview.title.replace(/["/:*?\\<>|~]/g,'_')));});
                savePageDialog.canSaveAsPdf = ((docContentType.indexOf("text/") === 0) || (docContentType.indexOf("image/") === 0)) && ! UrlUtils.isPdfViewerExtensionUrl(currentWebview.url);
            });
        }
        
        function switchToLastUsedTab() {
            internal.switchToTab(tabsModel.indexOf(recentTabsModel.get(1).tab), true)
        }

        function switchToOldestUsedTab() {
            internal.switchToTab(tabsModel.indexOf(recentTabsModel.get(recentTabsModel.count - 1).tab), true)
        }

        function switchToPreviousTab() {
            internal.switchToTab((tabsModel.currentIndex - 1 + tabsModel.count) % tabsModel.count, true)
        }

        function switchToNextTab() {
            internal.switchToTab((tabsModel.currentIndex + 1) % tabsModel.count, true)
        }

        function switchToTab(index, delayed) {
            if (delayed) {
                nextTabIndex = index
                delayedTabSwitcher.restart()
            } else {
                tabsModel.currentIndex = index
                nextTabIndex = -1
                var tab = tabsModel.currentTab
                if (tab) {
                    if (tab.empty) {
                        focusAddressBar()
                    } else {
                        tabContainer.forceActiveFocus()
                        tab.load();
                    }
                }
            }
        }

        function focusAddressBar(selectContent) {
            chrome.forceActiveFocus()
            Qt.inputMethod.show() // work around http://pad.lv/1316057
            if (selectContent) chrome.selectAll()
        }

        function resetFocus() {
            var currentTab = tabsModel.currentTab;
            if (currentTab) {
                if (currentTab.empty) {
                    internal.focusAddressBar()
                } else {
                    contentsContainer.focus = true;
                }
            }
        }

        // Invalid certificates the user has explicitly allowed for this session
        property var allowedCertificateErrors: []

        function allowCertificateError(error) {
            var host = UrlUtils.extractHost(error.url)
            var code = error.certError
            var fingerprint = error.certificate.fingerprintSHA1
            allowedCertificateErrors.push([host, code, fingerprint])
        }

        function isCertificateErrorAllowed(error) {
            var host = UrlUtils.extractHost(error.url)
            var code = error.certError
            var fingerprint = error.certificate.fingerprintSHA1
            for (var i in allowedCertificateErrors) {
                var allowed = allowedCertificateErrors[i]
                if ((host === allowed[0]) &&
                    (code === allowed[1]) &&
                    (fingerprint === allowed[2])) {
                    return true
                }
            }
            return false
        }

        // domains the user has allowed custom protocols for this (incognito) session
        property var domainsWithCustomUrlSchemesAllowed: []

        function allowCustomUrlSchemes(domain, allowPermanently) {
           domainsWithCustomUrlSchemesAllowed.push(domain);

           if (allowPermanently)
           {
                DomainSettingsModel.allowCustomUrlSchemes(domain, true);
           }
        }

        function areCustomUrlSchemesAllowed(domain) {

            for (var i in domainsWithCustomUrlSchemesAllowed) {
                if (domain === domainsWithCustomUrlSchemesAllowed[i]) {
                    return true;
                }
            }

            if (DomainSettingsModel.areCustomUrlSchemesAllowed(domain))
            {
                domainsWithCustomUrlSchemesAllowed.push(domain);
                return true;
            }

            return false;
        }

        function historyGoBack() {
            if (currentWebview && currentWebview.canGoBack) {
                internal.resetFocus()
                currentWebview.goBack()
            }
        }

        function historyGoForward() {
            if (currentWebview && currentWebview.canGoForward) {
                internal.resetFocus()
                currentWebview.goForward()
            }
        }

        function navigateToUrlAsync(targetUrl)
        {
            currentWebview.runJavaScript("window.location.href = '%1';".arg(targetUrl));
        }

        property var currentBookmarkOptionsDialog: null
        function addBookmark(url, title, icon, location) {
            if (title === "") title = UrlUtils.removeScheme(url)
            BookmarksModel.add(url, title, icon, "")
            if (location === undefined) location = chrome.bookmarkTogglePlaceHolder
            var properties = {"bookmarkUrl": url, "bookmarkTitle": title}
            internal.currentBookmarkOptionsDialog = PopupUtils.open(Qt.resolvedUrl("BookmarkOptions.qml"),
                                                           location, properties)
        }
        
        property var recentDownloads: []
        property var currentDownloadsDialog: null
        function showDownloadsDialog(caller) {
            if (!internal.currentDownloadsDialog) { 
                chrome.downloadNotify = false
                if (caller === undefined) caller = chrome.downloadsButtonPlaceHolder
                var properties = {"downloadsList": recentDownloads}

                internal.currentDownloadsDialog = PopupUtils.open(Qt.resolvedUrl("../DownloadsDialog.qml"),
                                                               caller, properties)
           }
        }

        function addNewDownload(download) {
            recentDownloads.unshift(download)
            chrome.showDownloadButton = Qt.binding(
                                        function(){
                                            if (browser.wide) {
                                                return true;
                                            } else {
                                                if (internal.currentDownloadsDialog) {
                                                    if (internal.currentDownloadsDialog.isEmpty) {
                                                        return false;
                                                    } else {
                                                        return true;
                                                    }
                                                } else {
                                                    if (recentDownloads.length > 0) {
                                                        return true;
                                                    } else {
                                                        return false;
                                                    }
                                                }
                                            }
                                        })
            if (internal.currentDownloadsDialog) {
                internal.currentDownloadsDialog.downloadsList = recentDownloads
            }
        }
    }

    Connections {
        target: internal.currentDownloadsDialog

        onShowDownloadsPage: showDownloadsPage()
    }

    // Work around https://launchpad.net/bugs/1502675 by delaying the switch to
    // the next tab for a fraction of a second to avoid a black flash.
    Timer {
        id: delayedTabSwitcher
        interval: 50
        onTriggered: internal.switchToTab(internal.nextTabIndex, false)
    }

    Connections {
        target: tabsModel
        onCurrentTabChanged: {
            chrome.findInPageMode = false
            internal.resetFocus()
        }
    }

    Item {
        id: actionsFactory

        readonly property var allActions: [
            { "id": "webviewBack", "title": i18n.tr("Web View Back"), "component": actionWebviewBack }
            , { "id": "webviewForward", "title": i18n.tr("Web View Forward"), "component": actionWebviewForward }
            , { "id": "webviewReload", "title": i18n.tr("Web View Reload"), "component": actionWebviewReload }
            , { "id": "webviewPullDown", "title": i18n.tr("Pull Down/Up Web View"), "component": actionWebviewPullDown }
            , { "id": "focusAddressbar", "title": i18n.tr("Focus Address Bar"), "component": actionFocusAddressbar }
            , { "id": "newTab", "title": i18n.tr("New Tab"), "component": actionNewTab }
            , { "id": "newWindow", "title": i18n.tr("New Window"), "component": actionNewWindow }
            , { "id": "newPrivateWindow", "title": i18n.tr("New Private Window"), "component": actionNewPrivateWindow }
            , { "id": "openBookmarks", "title": i18n.tr("Bookmarks"), "component": actionOpenBookmarks }
            , { "id": "openHistory", "title": i18n.tr("History"), "component": actionOpenHistory }
            , { "id": "openDownloads", "title": i18n.tr("Downloads"), "component": actionOpenDownloads }
            , { "id": "openSettings", "title": i18n.tr("Settings"), "component": actionOpenSettings }
            , { "id": "openDrawer", "title": i18n.tr("Open Menu"), "component": actionOpenDrawer }
            , { "id": "openZoom", "title": i18n.tr("Open Zoom Dialog"), "component": actionOpenZoom }
            , { "id": "openTabsList", "title": i18n.tr("Open Tabs List"), "component": actionOpenTabsList }
            , { "id": "searchTabs", "title": i18n.tr("Search Tabs"), "component": actionSearchTabs }
            , { "id": "findInPage", "title": i18n.tr("Find In Page"), "component": actionFindInPage }
            , { "id": "share", "title": i18n.tr("Share"), "component": actionShare }
            , { "id": "toggleBookmark", "title": i18n.tr("Bookmark/Unbookmark Page"), "component": actionToggleBookmark }
            , { "id": "toggleFullscreen", "title": i18n.tr("Toggle Full Screen"), "component": actionFullscreen }
            , { "id": "toggleSiteVersion", "title": i18n.tr("Toggle Site Version"), "component": actionToggleSiteVersion }
            , { "id": "muteTab", "title": i18n.tr("Mute Tab"), "component": actionMuteTab }
            , { "id": "closeTab", "title": i18n.tr("Close Tab"), "component": actionCloseTab }
            , { "id": "reopenTab", "title": i18n.tr("Reopen Recently Closed Tab"), "component": actionReopenTab }
            , { "id": "recentDownloads", "title": i18n.tr("Open Recent Downloads"), "component": actionOpenRecentDownloads }
        ]

        function getActionsModel(actionIDsList) {
            let newList = []
            actionIDsList.forEach( action => {
                newList.push(allActions.find(item => item.id == action.id).component)
            });

            return newList
        }

        Common.BaseAction {
            id: actionWebviewBack

            iconName: "go-previous"
            text: i18n.tr("Go back")
            enabled: browser.currentWebview && browser.currentWebview.canGoBack
            onTrigger: shortcutBackHistory.activated()
        }

        Common.BaseAction {
            id: actionWebviewForward

            iconName: "go-next"
            text: i18n.tr("Go forward")
            enabled: browser.currentWebview && browser.currentWebview.canGoForward
            onTrigger: shortcutForwardHistory.activated()
        }

        Common.BaseAction {
            id: actionWebviewReload

            iconName: chrome.loading ? "stop" : "reload"
            text: chrome.loading ? i18n.tr("Stop") : i18n.tr("Reload")
            enabled: !newTabViewLoader.active
            onTrigger: {
                if (chrome.loading) {
                    if (currentWebview) currentWebview.stop()
                } else {
                    shortcutReloadTab.activated()
                }
            }
        }

        Common.BaseAction {
            id: actionFocusAddressbar

            iconName: "stock_website"
            text: i18n.tr("Focus address bar")
            onTrigger: shortcutFocusAddressBar.activated()
        }

        Common.BaseAction {
            id: actionOpenDrawer

            iconName: "navigation-menu"
            text: i18n.tr("Open menu")
            onTrigger: chrome.openDrawerFromBottom()
        }

        Common.BaseAction {
            id: actionOpenRecentDownloads

            iconName: "save-to"
            text: i18n.tr("Recent downloads")
            onTrigger: internal.showDownloadsDialog(bottomGestures)
        }

        Common.BaseAction {
            id: actionWebviewPullDown

            iconName: browser.tabsModel.currentTab && browser.tabsModel.currentTab.webviewPulledDown ? "go-up" : "go-down"
            text: browser.tabsModel.currentTab && browser.tabsModel.currentTab.webviewPulledDown ? i18n.tr("Reset web view") : i18n.tr("Pull down web view")
            onTrigger: {
                if (browser.tabsModel.currentTab.webviewPulledDown) {
                    browser.tabsModel.currentTab.pullUpWebview()
                } else {
                    browser.tabsModel.currentTab.pullDownWebview()
                }
            }
        }

        Common.BaseAction {
            id: actionNewTab

            iconName: browser.incognito ? "private-tab-new" : "tab-new"
            text: i18n.tr("New tab")
            onTrigger: shortcutNewTab.activated()
        }

        Common.BaseAction {
            id: actionNewWindow

            iconName: "browser-tabs"
            text: i18n.tr("New window")
            onTrigger: shortcutNewWindow.activated()
        }

        Common.BaseAction {
            id: actionNewPrivateWindow

            iconName: "private-browsing"
            text: i18n.tr("New private window")
            onTrigger: shortcutNewPrivateWindow.activated()
        }

        Common.BaseAction {
            id: actionSearchTabs

            iconName: "find"
            text: i18n.tr("Search tabs")
            onTrigger: browser.openRecentView(true)
        }

        Common.BaseAction {
            id: actionFindInPage

            iconName: "find"
            text: i18n.tr("Find in page")
            enabled: !newTabViewLoader.active
            onTrigger: shortcutFind.activated()
        }

        Common.BaseAction {
            id: actionShare

            iconName: "share"
            text: i18n.tr("Share")
            enabled: (contentHandlerLoader.status == Loader.Ready) &&
                                 chrome.tab && chrome.tab.url.toString()
            onTrigger: internal.shareLink(chrome.tab.url, chrome.tab.title)
        }

        Common.BaseAction {
            id: actionFullscreen

            iconName: browser.isFullScreen ? "view-restore" : "view-fullscreen"
            text: browser.isFullScreen ? i18n.tr("Exit full screen") : i18n.tr("Go full screen")
            onTrigger: shortcutFullscreen.activated()
        }

        Common.BaseAction {
            id: actionOpenTabsList

            iconName: "browser-tabs"
            text: i18n.tr("View tabs")
            onTrigger: shortCutTabsView.activated()
        }

        Common.BaseAction {
            id: actionOpenBookmarks

            iconName: "bookmark"
            text: i18n.tr("Bookmarks")
            onTrigger: leftPageDrawerLoader.displayBookmarks()
        }

        Common.BaseAction {
            id: actionOpenHistory

            iconName: "history"
            text: i18n.tr("History")
            onTrigger: leftPageDrawerLoader.displayHistory()
        }

        Common.BaseAction {
            id: actionOpenDownloads

            iconName: "save-to"
            text: i18n.tr("Downloads")
            onTrigger: leftPageDrawerLoader.displayDownloads()
        }

        Common.BaseAction {
            id: actionOpenSettings

            iconName: "settings"
            text: i18n.tr("Settings")
            onTrigger: leftPageDrawerLoader.displaySettings()
        }

        Common.BaseAction {
            id: actionOpenZoom

            iconName: "zoom-in"
            text: i18n.tr("Zoom")
            enabled: browser.currentWebview && (currentWebview.url.toString() !== "")
            onTrigger: browser.currentWebview.showZoomMenu()
        }

        Common.BaseAction {
            id: actionToggleBookmark

            iconName: chrome.bookmarked ? "starred" : "non-starred"
            text: chrome.bookmarked ? i18n.tr("Remove as bookmark") : i18n.tr("Bookmark this page")
            enabled: browser.currentWebview && currentWebview.url.toString() ? true : false
            onTrigger: chrome.toggleBookmarkState(true)
        }

        Common.BaseAction {
            id: actionToggleSiteVersion

            readonly property bool smallDevice: browser.currentWebview && browser.currentWebview.context.__ua.calcScreenSize() == "small"
            readonly property bool appForceDesktop: browser.settings ? browser.settings.setDesktopMode :  false
            readonly property bool appForceMobile: browser.settings ? browser.settings.forceMobileSite : false
            readonly property bool desktopSwitchMode: (smallDevice && !appForceDesktop) || (!smallDevice && appForceMobile)
                                                // Always treat as mobile when auto version is enabled and window is NOT wide
                                                // on a large screen
                                                || (!smallDevice && settings.autoDeskMobSwitch && !browser.wide)

            objectName: "desktopSite"
            text: desktopSwitchMode && !checked ? i18n.tr("Desktop site") : i18n.tr("Mobile site")
            iconName: desktopSwitchMode && !checked ? "computer-symbolic" : "phone-smartphone-symbolic"

            onTrigger: {
                checked = !checked
                if (desktopSwitchMode) {
                    browser.tabsModel.currentTab.forceDesktopSite = checked
                } else {
                    browser.tabsModel.currentTab.forceMobileSite = checked
                }
            }
        }

        Binding {
            target: actionToggleSiteVersion
            property: "checked"
            value: actionToggleSiteVersion.desktopSwitchMode ? browser.tabsModel.currentTab && browser.tabsModel.currentTab.forceDesktopSite
                                    : browser.tabsModel.currentTab && browser.tabsModel.currentTab.forceMobileSite
        }

        Common.BaseAction {
            id: actionMuteTab

            iconName: browser.currentWebview && browser.currentWebview.audioMuted ? "audio-volume-high" : "audio-volume-muted"
            text: browser.currentWebview && browser.currentWebview.audioMuted ? i18n.tr("Unmute tab") : i18n.tr("Mute tab")
            enabled: browser.currentWebview && browser.currentWebview.url.toString() ? true : false
            onTrigger: shortcutMuteTab.activated()
        }

        Common.BaseAction {
            id: actionCloseTab

            iconName: "close"
            text: i18n.tr("Close tab")
            onTrigger: shortcutCloseCurrentTab.activated()
        }

        Common.BaseAction {
            id: actionReopenTab

            iconName: "undo"
            text: i18n.tr("Reopen last tab")
            onTrigger: shortcutReopenLastTab.activated()
        }
    }

    // TODO: internationalize non-standard key sequences?

    // Ctrl+Tab or Ctrl+PageDown: cycle through open tabs
    Shortcut {
        sequence: StandardKey.NextChild
        enabled: contentsContainer.visible || recentView.visible
        onActivated: internal.switchToLastUsedTab()
    }
    Shortcut {
        sequence: "Ctrl+PgDown"
        enabled: contentsContainer.visible || recentView.visible
        onActivated: internal.switchToLastUsedTab()
    }

    // Ctrl+Shift+Tab or Ctrl+PageUp: cycle through open tabs in reverse order
    Shortcut {
        sequence: StandardKey.PreviousChild
        enabled: contentsContainer.visible || recentView.visible
        onActivated: internal.switchToOldestUsedTab()
    }
    Shortcut {
        sequence: "Ctrl+Shift+Tab"
        enabled: contentsContainer.visible || recentView.visible
        onActivated: internal.switchToOldestUsedTab()
    }
    Shortcut {
        sequence: "Ctrl+PgUp"
        enabled: contentsContainer.visible || recentView.visible
        onActivated: internal.switchToOldestUsedTab()
    }

    // Ctrl+W or Ctrl+F4: Close the current tab
    Shortcut {
        id: shortcutCloseCurrentTab
        sequence: StandardKey.Close
        enabled: contentsContainer.visible || recentView.visible
        onActivated: internal.closeCurrentTab()
    }
    Shortcut {
        sequence: "Ctrl+F4"
        enabled: contentsContainer.visible || recentView.visible
        onActivated: internal.closeCurrentTab()
    }

    // Ctrl+Shift+W or Ctrl+Shift+T: Undo close tab
    Shortcut {
        sequence: "Ctrl+Shift+W"
        enabled: contentsContainer.visible || recentView.visible
        onActivated: internal.undoCloseTab()
    }
    Shortcut {
        id: shortcutReopenLastTab
        sequence: "Ctrl+Shift+T"
        enabled: contentsContainer.visible || recentView.visible
        onActivated: internal.undoCloseTab()
    }

    // Mute/Unmute tab
    Shortcut {
        id: shortcutMuteTab
        sequence: "Ctrl+M"
        enabled: currentWebview ? true : false
        onActivated: tabsModel.currentTab.toggleMute()
    }

    // Ctrl+T: Open a new Tab
    Shortcut {
        id: shortcutNewTab
        sequence: StandardKey.AddTab
        enabled: contentsContainer.visible || recentView.visible
        onActivated: {
            internal.openUrlInNewTab("", true)
            if (recentView.visible) recentView.reset()
        }
    }

    // F6 or Ctrl+L or Alt+D: Select the content in the address bar
    Shortcut {
        sequence: "F6"
        enabled: contentsContainer.visible
        onActivated: internal.focusAddressBar(true)
    }
    Shortcut {
        id: shortcutFocusAddressBar

        sequence: "Ctrl+L"
        enabled: contentsContainer.visible
        onActivated: internal.focusAddressBar(true)
    }
    Shortcut {
        sequence: "Alt+D"
        enabled: contentsContainer.visible
        onActivated: internal.focusAddressBar(true)
    }

    // Ctrl+D: Toggle bookmarked state on current Tab
    Shortcut {
        id: shortcutBookmarkTab
        sequence: "Ctrl+D"
        enabled: contentsContainer.visible && !newTabViewLoader.active
        onActivated: {
            if (internal.currentBookmarkOptionsDialog) {
                internal.currentBookmarkOptionsDialog.hide()
            } else if (currentWebview) {
                if (BookmarksModel.contains(currentWebview.url)) {
                    BookmarksModel.remove(currentWebview.url)
                } else {
                    internal.addBookmark(currentWebview.url, currentWebview.title, currentWebview.icon)
                }
            }
        }
    }

    // Ctrl+H: Show History
    Shortcut {
        id: shortcutHistory
        sequence: "Ctrl+H"
        enabled: contentsContainer.visible
        onActivated: {
            if (leftPageDrawerLoader.isHistory) {
                leftPageDrawerLoader.close()
            } else {
                leftPageDrawerLoader.displayHistory()
            }
        }
    }

    // Ctrl+Shift+O: Show Bookmarks
    Shortcut {
        id: shortcutBookmarks
        sequence: "Ctrl+Shift+O"
        enabled: contentsContainer.visible
        onActivated: {
            if (leftPageDrawerLoader.isBookmarks) {
                leftPageDrawerLoader.close()
            } else {
                leftPageDrawerLoader.displayBookmarks()
            }
        }
    }

    // Alt+← or Backspace: Goes to the previous page in history
    Shortcut {
        id: shortcutBackHistory
        sequence: StandardKey.Back
        enabled: contentsContainer.visible
        onActivated: internal.historyGoBack()
    }

    // Alt+→ or Shift+Backspace: Goes to the next page in history
    Shortcut {
        id: shortcutForwardHistory
        sequence: StandardKey.Forward
        enabled: contentsContainer.visible
        onActivated: internal.historyGoForward()
    }

    // F5 or Ctrl+R: Reload current Tab
    Shortcut {
        id: shortcutReloadTab
        sequence: "Ctrl+R"
        enabled: contentsContainer.visible
        onActivated: if (currentWebview) currentWebview.reload()
    }
    Shortcut {
        sequence: "F5"
        enabled: contentsContainer.visible
        onActivated: if (currentWebview) currentWebview.reload()
    }

    // Ctrl+F: Find in Page
    Shortcut {
        id: shortcutFind
        sequence: StandardKey.Find
        enabled: contentsContainer.visible && !newTabViewLoader.active
        onActivated: {
            if (tabsModel.currentTab) {
                if (tabsModel.currentTab.findInPageMode) {
                    tabsModel.currentTab.focusFindField()
                } else {
                    tabsModel.currentTab.findInPageMode = true
                }
            }
        }
    }

    // Ctrl+J: Show downloads page
    Shortcut {
        id: shortcutDownloads
        sequence: "Ctrl+J"
        enabled: chrome.visible &&
                 contentHandlerLoader.status == Loader.Ready
        onActivated: {
            if (leftPageDrawerLoader.isDownloads) {
                leftPageDrawerLoader.close()
            } else {
                leftPageDrawerLoader.displayDownloads(false)
            }
        }
    }

    // Alt+P: SHow settings page
    Shortcut {
        id: shortcutSettings
        sequence: "Alt+P"
        onActivated: {
            if (leftPageDrawerLoader.isSettings) {
                leftPageDrawerLoader.close()
            } else {
                leftPageDrawerLoader.displaySettings(false)
            }
        }
    }

    // Ctrl+G: Find next
    Shortcut {
        id: shortcutFindNext

        sequence: "Ctrl+G"
        enabled: currentWebview && tabsModel.currentTab && tabsModel.currentTab.findInPageMode
        onActivated: currentWebview.findController.next()
    }

    // Ctrl+Shift+G: Find previous
    Shortcut {
        id: shortcutFindPrevious

        sequence: "Ctrl+Shift+G"
        enabled: currentWebview && tabsModel.currentTab && tabsModel.currentTab.findInPageMode
        onActivated: currentWebview.findController.previous()
    }

    // F3: Find next
    Shortcut {
        sequence: StandardKey.FindNext
        enabled: currentWebview && tabsModel.currentTab && tabsModel.currentTab.findInPageMode
        onActivated: shortcutFindNext.activated()
    }

    // Shift+F3: Find previous
    Shortcut {
        sequence: StandardKey.FindPrevious
        enabled: currentWebview && tabsModel.currentTab && tabsModel.currentTab.findInPageMode
        onActivated: shortcutFindPrevious.activated()
    }

    // Escape: Exit webview fullscreen
    Shortcut {
        id: shortcutExitFullScreen
        sequence: "Esc"
        enabled: currentWebview && currentWebview.isFullScreen
        onActivated: {
            if (currentWebview.isFullScreen) {
                currentWebview.fullScreenCancelled()
            }
        }
    }

    // Ctrl+Space: Open and search tabs list
    Shortcut {
        id: shortCutTabsView
        sequence: "Ctrl+Space"
        enabled: currentWebview || recentView.visible
        onActivated: {
            if (recentView.visible) {
                recentView.reset()
            } else {
                browser.openRecentView()
            }
        }
    }

    // Ctrl+S: Save page as...
    Shortcut {
        id: shortcutSave
        sequence: StandardKey.Save
        enabled: currentWebview && (currentWebview.url.toString() !== "")
        onActivated: {
            internal.savePageAs()
        }
    }

    // Ctrl+U: View source
    Shortcut {
        id: shortcutViewSource
        sequence: "Ctrl+U"
        enabled: currentWebview && (currentWebview.url.toString() !== "")
        onActivated: {
            browser.viewSourceRequested()
        }
    }

    Loader {
        id: contentHandlerLoader
        source: "../ContentHandler.qml"
        asynchronous: true
    }

    Connections {
        target: contentHandlerLoader.item
        onExportFromDownloads: {
            leftPageDrawerLoader.displayDownloads(true);
            // export from downloads
            leftPageDrawerLoader.currentItem.mimetypeFilter = mimetypeFilter
            leftPageDrawerLoader.currentItem.activeTransfer = transfer
            leftPageDrawerLoader.currentItem.multiSelect = multiSelect
            leftPageDrawerLoader.currentItem.pickingMode = true
        }
    }

    Loader {
        id: contentExportLoader
        source: "../ContentExportDialog.qml"
        asynchronous: true
    }

    Connections {
        target: contentExportLoader.item

        onPreview: {
            if (leftPageDrawerLoader.isDownloads) {
                leftPageDrawerLoader.close()
            }
            internal.openUrlInNewTab(url, true, true, tabsModel.selectedIndex + 1)
        }
    }

    Loader {
        id: downloadDialogLoader
        source: "ContentDownloadDialog.qml"
        asynchronous: true
    }

    function showDownloadsPage() {
        if (!leftPageDrawerLoader.isDownloads) {
            shortcutDownloads.activated()
        }
        return leftPageDrawerLoader.currentItem
    }

    function startDownload(download) {

        var downloadIdDataBase = Common.ActiveDownloadsSingleton.downloadIdPrefixOfCurrentSession.concat(download.id)

        // check if the ID has already been added
        if ( Common.ActiveDownloadsSingleton.currentDownloads[downloadIdDataBase] === download )
        {
           console.log("the download id " + downloadIdDataBase + " has already been added.")
           return
        }

        console.log("adding download with id " + downloadIdDataBase)
        Common.ActiveDownloadsSingleton.currentDownloads[downloadIdDataBase] = download
        DownloadsModel.add(downloadIdDataBase, (download.url.toString().indexOf("file://%1/pdf_tmp".arg(cacheLocation)) === 0) ? "" : download.url, download.path, download.mimeType, incognito)

        internal.addNewDownload(download)
        internal.showDownloadsDialog()
    }

    function setDownloadComplete(download) {

        var downloadIdDataBase = Common.ActiveDownloadsSingleton.downloadIdPrefixOfCurrentSession.concat(download.id)

        if ( Common.ActiveDownloadsSingleton.currentDownloads[downloadIdDataBase] !== download )
        {
            console.log("the download id " + downloadIdDataBase + " is not in the current downloads.")
            return
        }

        console.log("download with id " + downloadIdDataBase + " is complete.")

        DownloadsModel.setComplete(downloadIdDataBase, true)

        if ((download.state === WebEngineDownloadItem.DownloadCancelled) || (download.state === WebEngineDownloadItem.DownloadInterrupted))
        {
          DownloadsModel.setError(downloadIdDataBase, download.interruptReasonString)
        }

        if (!internal.currentDownloadsDialog) {
            chrome.downloadNotify = true
        }
    }

    function actuallyStartDownload(download) {
        console.log("a download was requested with path %1".arg(download.path))
        download.accept();
        browser.startDownload(download);
    }

    // TODO: Find a way to immediately populate the model without doing this
    Instantiator {
        model: DownloadsModel
    }

    Component {
        id: downloadExistsDialogComponent

        Common.DownloadExistsDialog {}
    }

    Connections {

        target: currentWebview ? currentWebview.context : null

        onDownloadRequested: {
            // Only process the download in the active window
            if (browser.thisWindow.active) {
                if (currentWebview.url.toString().match(/^view-source:/i)) {
                    console.log(incognito ? "a download was rejected" : "a download was rejected with url %1".arg(download.url));
                    return;
                }

                // already downloaded item
                if ((download.url.toString().indexOf("file://%1".arg(currentWebview.profile.downloadPath)) === 0) && FileOperations.extension(download.path) !== "mhtml" ) {
                   console.log(incognito ? "download rejected (item has already been downloaded)" : "download rejected (%1 has already been downloaded)".arg(download.url));
                   currentWebview.showMessage(i18n.tr("the file is already in the downloads folder."))
                   return;
                }

                let _downloadExistsDialog = null
                // TODO: Disabled for now since "download" object gets destroyed after the signal handler is executed
                // Find a way to accept the download after user choose an option from the dialog
                let _foundDownloadId = ""
                // let _foundDownloadId = DownloadsModel.findDownload(download.url, browser.incognito)
                if (_foundDownloadId != "") {
                    console.log("This has already been downloaded " + _foundDownloadId)
                    let _properties = {
                        url: download.url
                        , filename: download.suggestedFileName
                        , downloadObj: download
                    }
                    _downloadExistsDialog = downloadExistsDialogComponent.createObject(browser, _properties )
                    browser.proceedDialog = _downloadExistsDialog
                    _downloadExistsDialog.viewDownloads.connect(function() {
                            browser.showDownloadsPage()
                            return;
                        }
                    )
                    _downloadExistsDialog.allow.connect(function(download) {
                            browser.actuallyStartDownload(download)
                        }
                    )

                    if (browser.wide) {
                        _downloadExistsDialog.openNormal();
                    } else {
                        _downloadExistsDialog.openBottom();
                    }
                } else {
                    browser.actuallyStartDownload(download)
                }
            }
        }

        onDownloadFinished: {

            console.log(incognito? "download finished" : "a download was finished with path %1.".arg(download.path));
            browser.setDownloadComplete(download);

            // delete pdf in cache / close the tab
            if (download.url.toString().indexOf("file://%1/pdf_tmp".arg(cacheLocation)) === 0) {
              FileOperations.remove(download.url);
              internal.closeTabsWithUrl(download.url);
            }
        }
    }

    Connections {
        target: settings
        onZoomFactorChanged: DomainSettingsModel.defaultZoomFactor = settings.zoomFactor
        onDomainWhiteListModeChanged: DomainPermissionsModel.whiteListMode = settings.domainWhiteListMode
        onSetDesktopModeChanged: if (browser.currentWebview && settings.autoDeskMobSwitchReload) browser.tabsModel.currentTab.reload();
        onForceMobileSiteChanged: if (browser.currentWebview && settings.autoDeskMobSwitchReload) browser.tabsModel.currentTab.reload();
        onAutoDeskMobSwitchChanged: {
            if (settings.autoDeskMobSwitchReload && currentWebview.context.__ua.calcScreenSize() == "large") {
                browser.tabsModel.currentTab.reload();
            }
        }
    }
    /*

    Loader {
        id: filePickerLoader
        source: "ContentPickerDialog.qml"
        asynchronous: true
    }

    */

    // Cover the webview (gaps around tabsbar) with DropArea so that webview doesn't steal events
    DropArea {
        id: dropAreaTopCover
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }
        height: units.gu(1)
        keys: ["webbrowser/tab-" + (incognito ? "incognito" : "public")]
        visible: chrome.showTabsBar

        onEntered: {
            window.raise()
            window.requestActivate()
        }
    }

    DropArea {
        id: dropAreaBottomCover
        anchors {
            fill: parent
            leftMargin: browser.leftPadding
            topMargin: chrome.tabsBarHeight
        }
        keys: ["webbrowser/tab-" + (incognito ? "incognito" : "public")]

        onEntered: {
            window.raise()
            window.requestActivate()
        }
    }

    Loader {
        id: firstRunWizardLoader

        z: 1000
        active: browser.settings.firstRun
        asynchronous: true
        visible: status == Loader.Ready
        sourceComponent: Common.FirstRunWizard{
            onFinished: browser.settings.firstRun = false
            onShowTooltip: globalTooltip.display(text, "BOTTOM", 2000)
        }
        
        anchors.fill: parent
    }
    
    Common.GlobalTooltip {
        id: globalTooltip
    }

    // Customize the global attached tooltip in QQC2 components
    QQC2.Control {
        QQC2.ToolTip.toolTip {
            readonly property real centeredImplicitWidth: Math.max(QQC2.ToolTip.toolTip.background ? QQC2.ToolTip.toolTip.background.implicitWidth : 0,
                                QQC2.ToolTip.toolTip.contentItem.implicitWidth + QQC2.ToolTip.toolTip.leftPadding + QQC2.ToolTip.toolTip.rightPadding)
            implicitWidth: Math.min(parent.width, centeredImplicitWidth)
            contentItem: QQC2.Label {
                text: QQC2.ToolTip.toolTip.text
                wrapMode: Text.WordWrap
                color: Suru.backgroundColor
            }
        }
    }
}
