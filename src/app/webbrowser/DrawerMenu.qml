import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Controls.Suru 2.2
import QtQuick.Layouts 1.12
import "../" as Common

Common.CustomizedMenu {
    id: drawerMenu
    
    readonly property real heightAtBottom: units.gu(60)
    readonly property real marginAtBottom: units.gu(1)

    property bool bigUIMode: false
    property bool shownAtBottom: false
    property list<QQC2.Action> bottomActions

    transformOrigin: shownAtBottom ? QQC2.Menu.Bottom : QQC2.Menu.Top
    modal: shownAtBottom
    dim: false
    width: bigUIMode ? availWidth : defaultWidth

    onClosed: destroy()

    function openFromTop(caller) {
        shownAtBottom = false
        var mapped = caller.mapToItem(parent, 0, 0)

        x = Qt.binding(function(){
                if (width == maximumWidth) {
                    return mapped.x
                } else {
                    return (availWidth - width)
                }
            })
        y = mapped.y + caller.height 

        open()
    }

    function openFromBottom() {
        shownAtBottom = true
        x = Qt.binding(function(){ return availWidth - width })
        y = availHeight - heightAtBottom - marginAtBottom
        contentItem.interactive = true
        implicitHeight = heightAtBottom

        open()
    }

    contentItem: ListView {
        implicitHeight: contentHeight
        model: drawerMenu.contentModel
        interactive: QQC2.ApplicationWindow.window ? contentHeight > QQC2.ApplicationWindow.window.height : false
        clip: true
        keyNavigationWraps: false
        currentIndex: -1
        verticalLayoutDirection: drawerMenu.shownAtBottom ? ListView.BottomToTop : ListView.TopToBottom
        boundsBehavior: Flickable.StopAtBounds
        section.property: "visible"
        section.labelPositioning: ViewSection.CurrentLabelAtStart
        section.delegate: Component {
            Common.ActionRowMenu {
                model: drawerMenu.bottomActions
                separatorAtTop: drawerMenu.shownAtBottom
                menu: drawerMenu

                anchors {
                    left: parent.left
                    right: parent.right
                }
            }
        }
        QQC2.ScrollIndicator.vertical: QQC2.ScrollIndicator {}
    }
}
