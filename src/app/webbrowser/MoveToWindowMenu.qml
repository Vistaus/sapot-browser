import QtQuick 2.9

ActionToWindowMenu {
    id: moveToWindowMenu

    property int targetIndex
    property var tab

    signal actionTriggered

    title: i18n.tr("Move to...")
    iconName: showIcon ? "go-next" : ""

    onActionToNewWindow: {
        thisWindow.moveTabToWindow(targetIndex, tab, null)
        actionTriggered()
    }

    onActionToWindow: {
        thisWindow.moveTabToWindow(targetIndex, tab, targetWindow)
        actionTriggered()
    }
}
