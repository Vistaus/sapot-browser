/*
 * Copyright 2013-2016 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtWebEngine 1.10
import QtQuick.Controls 2.2 as QQC2
import QtQuick.Layouts 1.12
import webbrowserapp.private 0.1
import webbrowsercommon.private 0.1
import ".."

FocusScope {
    id: root

    property var tab
    property var tabsModel
    property var searchEngine
    property alias bigUIMode: addressbar.bigUIMode
    property alias wide: addressbar.wide
    property alias loading: addressbar.loading
    property alias searchUrl: addressbar.searchUrl
    readonly property string text: addressbar.text
    readonly property string actualUrl: addressbar.actualUrl
    readonly property alias searchType: addressbar.searchType
    readonly property alias focusedSearch: addressbar.focusedSearch
    property alias bookmarked: addressbar.bookmarked
    signal closeTabRequested()
    signal toggleBookmark()
    signal toggleDownloads(var caller)
    property bool showDownloadButton: false
    property bool downloadNotify: false
    property Component drawerMenu
    readonly property bool drawerOpen: internal.openDrawer
    readonly property bool leftMenuOpen: internal.leftBottomMenu && internal.leftBottomMenu.opened
    readonly property bool rightMenuOpen: internal.rightBottomMenu  && internal.rightBottomMenu.opened
    property alias requestedUrl: addressbar.requestedUrl
    property alias canSimplifyText: addressbar.canSimplifyText
    property alias findInPageMode: addressbar.findInPageMode
    property alias tabListMode: addressbar.tabListMode
    property alias contextMenuVisible: addressbar.contextMenuVisible
    property alias editing: addressbar.editing
    property alias incognito: addressbar.incognito
    property alias showFaviconInAddressBar: addressbar.showFavicon
    readonly property alias bookmarkTogglePlaceHolder: addressbar.bookmarkTogglePlaceHolder
    readonly property alias downloadsButtonPlaceHolder: downloadsButton
    property alias showSuggestions: suggestionsList.show
    property alias suggestionsActiveFocus: suggestionsList.activeFocus
    property alias suggestionsFocus: suggestionsList.focus
    property alias suggestionsCount: suggestionsList.count
    property real availableHeightBelowChrome
    property color fgColor: theme.palette.normal.baseText
    property color iconColor: theme.palette.normal.baseText
    property real availableHeight
    property bool navHistoryOpen: false
    property bool hideTabsButtonNavBar: false
    property bool enableFocusedSearch: false
    property var searchSuggestionsSettings: []

    signal suggestionsEscaped
    signal suggestionSwitchTab(int tabIndex)
    signal suggestionsActivated(var itemData)
    signal openRecentView

    onFindInPageModeChanged: if (findInPageMode) addressbar.text = ""
    onIncognitoChanged: findInPageMode = false

    function selectAll() {
        addressbar.selectAll()
    }

    function triggerLeftAction(fromBottom) {
        if (internal.leftBottomMenu) {
            internal.leftBottomMenu.open()
        } else {
            leftBottomMenuLoader.active = true
        }
    }

    function triggerRightAction(fromBottom) {
        if (rightButtonsBar.onlyVisibleButton) {
            rightButtonsBar.onlyVisibleButton.triggerAction(fromBottom, null)
        } else {
            if (internal.rightBottomMenu) {
                internal.rightBottomMenu.open()
            } else {
                rightBottomMenuLoader.active = true
            }
        }
    }

    function showNavHistory(model, fromBottom, caller) {
        navHistPopup.model = model
        navHistPopup.show(fromBottom, caller)
    }

    function openDrawerFromBottom() {
        drawerButton.triggerAction(true, null)
    }

    NavHistoryPopup {
        id: navHistPopup

        property int navOffset: 0
        
        availHeight: root.availableHeight
        availWidth: root.width
        onNavigate: {
            // Navigate only after the dialog has closed
            // so history list won't be seen updating
            navOffset = offset
        }
        onOpened: root.navHistoryOpen = true
        onClosed: {
            root.navHistoryOpen = false
            if (navOffset !== 0) {
                internal.webview.goBackOrForward(navOffset)
            }
            navOffset = 0
        }
    }

    Loader {
        id: leftBottomMenuLoader

        active: false
        asynchronous: true
        visible: status == Loader.Ready
        sourceComponent: BottomMenu {
            leftMargin: browser.leftPadding + margins
            model: leftButtonsBar.children
            transformOrigin: QQC2.Menu.TopLeft
        }

        onLoaded: item.open()
    }

    Loader {
        id: rightBottomMenuLoader

        active: false
        asynchronous: true
        visible: status == Loader.Ready
        sourceComponent: BottomMenu {
            model: rightButtonsBar.children
            transformOrigin: QQC2.Menu.BottomRight
            x: parent.width - width
        }

        onLoaded: item.open()
    }

    FocusScope {
        anchors {
            fill: parent
        }

        focus: true

        RowLayout {
            anchors.fill: parent
            
            RowLayout {
                id: leftButtonsBar

                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                Layout.fillHeight: true
                Layout.leftMargin: units.gu(1)
                spacing: 0
                visible: !addressbar.textFieldWideOnFocus

                ChromeButton {
                    id: backButton
                    objectName: "backButton"

                    Layout.fillHeight:  true
                    implicitWidth: height * 0.8

                    iconName: findInPageMode ? "close" : "previous"
                    iconSize: 0.4 * height
                    iconColor: root.iconColor

                    alwaysDisplayAtBottom: true
                    enableContextMenu: true
                    contextMenu: navHistPopup

                    enabled: findInPageMode || (internal.webview ? internal.webview.canGoBack : false)
                    onTriggerAction: {
                        if (findInPageMode) {
                            findInPageMode = false
                        } else {
                            if (internal.webview.loading) {
                                internal.webview.stop()
                            }
                            internal.webview.goBack()
                        }
                    }

                    onShowContextMenu: {
                        showNavHistory(internal.webview.navigationHistory.backItems, fromBottom, caller)
                    }
                }

                Connections {
                    enabled: ! backButton.enabled
                    target: internal.webview

                    // normally the "canGoBack" should be automatically updated, but does not for webview.url changes.
                    // this is a workaround for https://github.com/ubports/morph-browser/issues/306
                    onLoadingChanged: {
                        if (loadRequest.status !== WebEngineLoadRequest.LoadStartedStatus) {
                            internal.webview.onUrlChanged();
                        }
                    }
                }

                ChromeButton {
                    id: forwardButton
                    objectName: "forwardButton"

                    Layout.fillHeight:  true
                    implicitWidth: height * 0.8

                    iconName: "next"
                    iconSize: 0.4 * height
                    iconColor: root.iconColor

                    visible: root.wide ? true : enabled

                    enableContextMenu: true
                    contextMenu: navHistPopup

                    enabled: findInPageMode ? false :
                             (internal.webview ? internal.webview.canGoForward : false)
                    onTriggerAction: {
                        if (internal.webview.loading)
                        {
                            internal.webview.stop()
                        }
                        internal.webview.goForward()
                    }

                    onShowContextMenu: showNavHistory(internal.webview.navigationHistory.forwardItems, fromBottom, caller)
                }

                ChromeButton {
                    id: reloadButton
                    objectName: "reloadButton"
                    
                    readonly property bool reloadFunction: addressbar.text == addressbar.actualUrl
                    readonly property bool stopFunction: root.loading

                    Layout.fillHeight:  true
                    implicitWidth: height * 0.8

                    iconName: stopFunction ? "stop" : "reload"
                    iconSize: 0.4 * height
                    iconColor: root.iconColor

                    alwaysDisplayAtBottom: true
                    visible: root.wide

                    enabled: !findInPageMode
                    onTriggerAction: {
                        if (stopFunction) {
                            internal.webview.stop()
                        } else {
                            internal.webview.forceActiveFocus()
                            internal.webview.reload()
                        }
                    }
                }

                // Only shown in the bottomMenu
                ChromeButton {
                    id: addressBarButton
                    objectName: "addressBarButton"

                    Layout.fillHeight:  true
                    implicitWidth: height * 0.8

                    iconName: findInPageMode ? "find" : "stock_website"
                    iconSize: 0.4 * height
                    iconColor: root.iconColor

                    alwaysDisplayAtBottom: true
                    closeMenuOnTrigger: true
                    visible: false

                    onTriggerAction: {
                        addressbar.forceActiveFocus()
                        if (!findInPageMode) {
                            root.selectAll()
                        }
                    }
                }

                // Only shown in the bottomMenu
                ChromeButton {
                    id: pullDownWebViewButton
                    objectName: "pullDownWebViewButton"

                    Layout.fillHeight:  true
                    implicitWidth: height * 0.8

                    iconName: root.tab && root.tab.webviewPulledDown ? "up" : "down"
                    iconSize: 0.4 * height
                    iconColor: root.iconColor

                    alwaysDisplayAtBottom: true
                    closeMenuOnTrigger: true
                    visible: false

                    onTriggerAction: {
                        if (root.tab.webviewPulledDown) {
                            root.tab.pullUpWebview()
                        } else {
                            root.tab.pullDownWebview()
                        }
                    }
                }
            }

            Item {
                Layout.fillHeight:  true
                Layout.fillWidth: true
                Layout.topMargin: units.gu(1)
                Layout.bottomMargin: units.gu(1)
                Layout.leftMargin: addressbar.textFieldWideOnFocus ? units.gu(2) : 0
                Layout.rightMargin: addressbar.textFieldWideOnFocus ? units.gu(2) : 0

                Suggestions {
                    id: suggestionsList

                    readonly property bool searchEngineSuggestionsEnabled: getEnabledFromSettings(searchEngineSuggestionsId)
                    readonly property bool searchEngineSuggestionsIncognitoEnabled: searchEngineSuggestionsEnabled
                                                                                    && getIncognitoEnabledFromSettings(searchEngineSuggestionsId)
                    readonly property int searchEngineSuggestionsLimit: getLimitFromSettings(searchEngineSuggestionsId)
                    readonly property string tabsSuggestionsId: "tabs"
                    readonly property string historySuggestionsId: "history"
                    readonly property string bookmarksSuggestionsId: "bookmarks"
                    readonly property string searchEngineSuggestionsId: "searchengine"

                    property bool show: false
                    opacity: show ? 1 : 0
                    Behavior on opacity { UbuntuNumberAnimation {} }
                    enabled: opacity > 0
                    topEmptySpace: addressbar.height
                    focusedSearch: root.focusedSearch
                    searchType: root.searchType
                    incognito: root.incognito
                    focus: false
                    anchors {
                        top: addressbar.top
                        left: addressbar.left
                        right: addressbar.right
                    }

                    height: enabled ? Math.min(contentHeight + topEmptySpace, root.availableHeightBelowChrome) : 0

                    searchTerms: root.text.split(/\s+/g).filter(function(term) { return term.length > 0 })
                    wide: root.wide

                    Keys.onUpPressed: addressbar.focus = true
                    Keys.onEscapePressed: root.suggestionsEscaped()

                    models: {
                        if (searchTerms && searchTerms.length > 0) {
                            let arrList = []

                            if (searchEngineSuggestionsEnabled) {
                                arrList.push(searchSuggestions.limit(searchEngineSuggestionsLimit))
                            }

                            switch (root.searchType) {
                                case "BOOKMARKS":
                                    arrList.unshift(bookmarksSuggestions)
                                    return arrList
                                    break
                                case "HISTORY":
                                    arrList.unshift(historySuggestions)
                                    return arrList
                                    break
                                case "TABS":
                                    arrList.unshift(tabSuggestions)
                                    return arrList
                                    break
                                default:
                                    return getListFromSettings()
                            }
                        } else {
                            return []
                        }
                    }

                    function getLimitFromSettings(itemId) {
                        let foundItem = root.searchSuggestionsSettings.find(item => item.id == itemId)

                        if (foundItem) {
                            return foundItem.limit
                        }

                        return 2
                    }

                    function getEnabledFromSettings(itemId) {
                        let foundItem = root.searchSuggestionsSettings.find(item => item.id == itemId)

                        if (foundItem) {
                            return foundItem.enabled
                        }

                        return false
                    }

                    function getIncognitoEnabledFromSettings(itemId) {
                        let foundItem = root.searchSuggestionsSettings.find(item => item.id == itemId)

                        if (foundItem) {
                            return foundItem.showInIncognito
                        }

                        return false
                    }

                    function getListFromSettings() {
                        let arrList = []

                        root.searchSuggestionsSettings.forEach(item => {
                            if ((item.enabled && !root.incognito)
                                    || (item.enabled && root.incognito && item.showInIncognito)) {
                                switch (item.id) {
                                    case tabsSuggestionsId:
                                        arrList.push(tabSuggestions)
                                        break
                                    case historySuggestionsId:
                                        arrList.push(historySuggestions)
                                        break
                                    case bookmarksSuggestionsId:
                                        arrList.push(bookmarksSuggestions)
                                        break
                                    case searchEngineSuggestionsId:
                                        arrList.push(searchSuggestions.limit(searchEngineSuggestionsLimit))
                                        break
                                }
                            }
                        })

                        return arrList
                    }

                    onActivated: {
                        switch (itemType) {
                            case "TABS":
                                root.suggestionSwitchTab(root.tabsModel.indexOf(itemData))
                                break
                            default:
                                root.suggestionsActivated(itemData)
                                break
                        }
                    }

                    onEnabledChanged: {
                        if (!enabled) {
                            addressbar.focus = true
                        }
                    }
                }

                AddressBar {
                    id: addressbar

                    anchors {
                        top: parent.top
                        bottom: parent.bottom
                        horizontalCenter: parent.horizontalCenter
                    }
                    width: Math.min(parent.width, units.gu(140))
                    Behavior on width {
                        UbuntuNumberAnimation { duration: UbuntuAnimation.SnapDuration }
                    }
                    fgColor: root.fgColor
                    suggestionStyle: suggestionsList.enabled
                    enableFocusedSearch: root.enableFocusedSearch
                    focusedSearchItemMatch: suggestionsList.focusedSearchItemMatch

                    focus: true

                    findInPageMode: findInPageMode
                    findController: internal.webview ? internal.webview.findController : null
                    certificateErrorsMap: internal.webview ? internal.webview.profile.certificateErrorsMap : ({})
                    lastLoadSucceeded: internal.webview ? internal.webview.lastLoadSucceeded : false

                    icon: (internal.webview && internal.webview.certificateError) ? "" : tab ? tab.icon : ""

                    onValidated: {
                        if (!findInPageMode) {
                            internal.webview.forceActiveFocus()
                            internal.webview.url = requestedUrl
                        }
                    }
                    onSelectFirstSuggestion: suggestionsList.selectFirstItem()
                    onRequestReload: {
                        internal.webview.forceActiveFocus()
                        internal.webview.reload()
                    }
                    onRequestStop: internal.webview.stop()
                    onToggleBookmark: root.toggleBookmark()

                    Connections {
                        target: tab
                        onUrlChanged: addressbar.actualUrl = tab.url
                    }
                }
            }

            RowLayout {
                id: rightButtonsBar
                
                property var onlyVisibleButton: {
                    var visibleCount = 0
                    var visibleButton
                    for (var i = 0; i < children.length; i++)   {
                        if (children[i].visible) {
                            visibleCount += 1
                            visibleButton = children[i]
                        }
                    }

                    if (visibleCount == 1) {
                        return visibleButton
                    } else {
                        return null
                    }
                }

                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.fillHeight: true
                Layout.rightMargin: units.gu(1)
                visible: !addressbar.textFieldWideOnFocus

                spacing: 0

                ChromeButton {
                    id: findPreviousButton
                    objectName: "findPreviousButton"

                    Layout.fillHeight:  true
                    implicitWidth: height * 0.8

                    iconName: "up"
                    iconSize: 0.5 * height

                    visible: findInPageMode
                    enabled: internal.webview && internal.webview.findController && internal.webview.findController.foundMatch
                    onTriggerAction: internal.webview.findController.previous()
                }

                ChromeButton {
                    id: findNextButton
                    objectName: "findNextButton"

                    Layout.fillHeight:  true
                    implicitWidth: height * 0.8

                    iconName: "down"
                    iconSize: 0.5 * height

                    visible: findInPageMode
                    enabled: internal.webview && internal.webview.findController && internal.webview.findController.foundMatch
                    onTriggerAction: internal.webview.findController.next()
                }

                ChromeButton {
                    id: downloadsButton
                    objectName: "downloadsButton"

                    Layout.fillHeight:  true
                    implicitWidth: height * 0.8

                    visible: showDownloadButton && !tabListMode
                    iconName: "save"
                    iconSize: 0.35 * height
                    iconColor: downloadNotify ? theme.palette.normal.focus : root.iconColor

                    Connections {
                        target: root

                        onDownloadNotifyChanged: {
                            if (downloadNotify) {
                                shakeAnimation.start()
                            }
                        }
                    }

                    Behavior on iconColor {
                        ColorAnimation { duration: UbuntuAnimation.BriskDuration  }
                    }

                    SequentialAnimation {
                        id: shakeAnimation

                        loops: 4

                        RotationAnimation {
                            target: downloadsButton
                            direction: RotationAnimation.Counterclockwise
                            to: 350
                            duration: 50
                        }

                        RotationAnimation {
                            target: downloadsButton
                            direction: RotationAnimation.Clockwise
                            to: 10
                            duration: 50
                        }

                        RotationAnimation {
                            target: downloadsButton
                            direction: RotationAnimation.Counterclockwise
                            to: 0
                            duration: 50
                        }
                    }

                    closeMenuOnTrigger: true

                    onTriggerAction: {
                        if (fromBottom) {
                            toggleDownloads(caller)
                        } else {
                            toggleDownloads(downloadsButton)
                        }
                    }
                }

                ChromeButton {
                    id: tabsButton
                    objectName: "tabsButton"

                    Layout.fillHeight:  true
                    visible: !root.hideTabsButtonNavBar
                    implicitWidth: height * 0.8

                    iconName: "browser-tabs"
                    iconSize: 0.4 * height
                    iconColor: root.iconColor

                    Label {
                        text: root.tabsModel.count < 100 ? root.tabsModel.count : "X"
                        anchors{
                            centerIn: parent
                            horizontalCenterOffset: units.gu(0.08)
                            verticalCenterOffset: units.gu(0.08)
                        }

                        textSize: text.length > 1 ? Label.XxSmall : Label.XSmall
                    }

                    onTriggerAction: root.openRecentView()
                }

                ChromeButton {
                    id: drawerButton
                    objectName: "drawerButton"

                    Layout.fillHeight:  true
                    implicitWidth: height * 0.8

                    iconName: "navigation-menu"
                    iconSize: 0.5 * height
                    iconColor: root.iconColor
                    
                    onTriggerAction: {
                        if (!internal.openDrawer || internal.openDrawer.opened) {
                            internal.openDrawer = drawerMenu.createObject(QQC2.ApplicationWindow.overlay)
                            if (fromBottom) {
                                internal.openDrawer.openFromBottom()
                            } else {
                                internal.openDrawer.openFromTop(drawerButton)
                            }

                        } else {
                            internal.openDrawer.close()
                        }
                    }
                }
            }
        }
    }

    LimitProxyModel {
        id: tabSuggestions

        limit: root.focusedSearch ? 10 : suggestionsList.getLimitFromSettings(suggestionsList.tabsSuggestionsId)
        readonly property string itemType: "TABS"
        readonly property string icon: "browser-tabs"
        readonly property bool displayUrl: true
        // TODO: Implement proper exclusion of current tab without affecting the limit
        readonly property var excludedTabs: [root.tabsModel.currentTab]
        sourceModel: TextSearchFilterModel {
            sourceModel: root.tabsModel
            terms: suggestionsList.searchTerms
            searchFields: ["url", "title"]
        }
    }

    LimitProxyModel {
        id: historySuggestions

        limit: root.focusedSearch ? 10 : suggestionsList.getLimitFromSettings(suggestionsList.historySuggestionsId)
        readonly property string itemType: "HISTORY"
        readonly property string icon: "history"
        readonly property bool displayUrl: true
        sourceModel: TextSearchFilterModel {
            sourceModel: HistoryModel
            terms: suggestionsList.searchTerms
            searchFields: ["url", "title"]
        }
    }

    LimitProxyModel {
        id: bookmarksSuggestions

        limit: root.focusedSearch ? 10 : suggestionsList.getLimitFromSettings(suggestionsList.bookmarksSuggestionsId)
        readonly property string itemType: "BOOKMARKS"
        readonly property string icon: "non-starred"
        readonly property bool displayUrl: true
        sourceModel: TextSearchFilterModel {
            sourceModel: BookmarksModel
            terms: suggestionsList.searchTerms
            searchFields: ["url", "title"]
        }
    }

    SearchSuggestions {
        id: searchSuggestions
        terms: suggestionsList.searchTerms
        searchEngine: root.searchEngine
        active: (root.activeFocus || suggestionsList.activeFocus)
                 && (
                    (!root.incognito && suggestionsList.searchEngineSuggestionsEnabled)
                    || (root.incognito && suggestionsList.searchEngineSuggestionsIncognitoEnabled)
                 )
                 && !root.findInPageMode && !UrlUtils.looksLikeAUrl(root.text.replace(/ /g, "+"))

        function limit(number) {
            var slice = results.slice(0, number)
            slice.itemType = 'SEARCH'
            slice.icon = 'search'
            slice.displayUrl = false
            return slice
        }
    }

    QtObject {
        id: internal
        property var openDrawer: null
        readonly property var webview: tab ? tab.webview : null
        readonly property alias leftBottomMenu: leftBottomMenuLoader.item
        readonly property alias rightBottomMenu: rightBottomMenuLoader.item
    }

    onTabChanged: {
        if (tab) {
            addressbar.actualUrl = tab.url
            if (!tab.url.toString() && editing) {
                addressbar.text = ""
            }
        } else {
            addressbar.actualUrl = ""
        }
    }
}
