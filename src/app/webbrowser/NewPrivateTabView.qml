/*
 * Copyright 2015 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12

Item {
    id: newPrivateTabView
    objectName: "newPrivateTabView"

    Rectangle {
        anchors.fill: parent
        color: theme.palette.normal.foreground
    }

    ColumnLayout {
        anchors {
            margins: units.gu(2)
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        spacing: units.gu(2)

        Icon {
            Layout.alignment: Qt.AlignHCenter

            width: units.gu(10)
            height: width
            color: theme.palette.normal.foregroundText

            name: "private-browsing"
        }

        Label {
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.maximumWidth: units.gu(40)

            text: i18n.tr("This is a private tab")
            horizontalAlignment: Text.AlignHCenter
            color: theme.palette.normal.foregroundText
            textSize: Label.Large
        }

        Label {
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.maximumWidth: units.gu(40)

            wrapMode: Text.WordWrap
            horizontalAlignment: Text.AlignHCenter
            text: i18n.tr("Pages that you view in this tab won't appear in your browser history.\nBookmarks you create will be preserved, however.")
            color: theme.palette.normal.backgroundSecondaryText

            textSize: Label.Medium
        }
    }
}
