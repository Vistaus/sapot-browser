import QtQuick 2.9
import QtWebEngine 1.10

ActionToWindowMenu {
    id: moveToWindowMenu

    property url linkUrl

    title: i18n.tr("Open link in...")

    onActionToNewWindow: {
        thisWindow.currentWebview.triggerWebAction(WebEngineView.OpenLinkInNewWindow)
    }

    onActionToWindow: {
        thisWindow.openLinkInWindow(linkUrl, targetWindow)
    }
}
