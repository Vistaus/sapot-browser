/*
 * Copyright 2015 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pragma Singleton

import QtQuick 2.4
import QtWebEngine 1.5
import webbrowserapp.private 0.1
import webbrowsercommon.private 0.1
import Morph.Web 0.1
import "../FileUtils.js" as FileUtils

Item {
    property string capturesDir:  cacheLocation + "/captures"
    signal previewSaved(url pageUrl, url previewUrl)

    function previewPathFromUrl(url) {
        return "%1/%2.png".arg(capturesDir).arg(Qt.md5(url))
    }

    function saveToDisk(data, url) {
        if (!FileOperations.exists(Qt.resolvedUrl(capturesDir))) {
            FileOperations.mkpath(Qt.resolvedUrl(capturesDir))
        }

        var filepath = previewPathFromUrl(url)
        var previewUrl = ""
        if (data.saveToFile(filepath)) previewUrl = Qt.resolvedUrl(filepath)
        else console.warn("Failed to save preview to disk for %1 (path is %2)".arg(url).arg(filepath))

        previewSaved(url, previewUrl)
    }


    function checkDelete(url) {
        FileOperations.remove(Qt.resolvedUrl(previewPathFromUrl(url)))
    }

    // Remove all previews stored on disk that are not part of the top sites
    // and that are not for URLs in the doNotCleanUrls list
    function cleanUnusedPreviews(doNotCleanUrls) {
        var dir = Qt.resolvedUrl(capturesDir)
        var previews = FileOperations.filesInDirectory(dir, ["*.png", "*.jpg"])

        for (var i in previews) {
            var preview = previews[i]
            if (doNotCleanUrls.indexOf(preview) === -1) {
                var file = Qt.resolvedUrl("%1/%2".arg(capturesDir).arg(preview))
                console.log("Deleted preview: " + file)
                FileOperations.remove(file)
            }
        }
    }

    function listUsedPreviews(windowList, outputArray, isOpenWindows) {
        var arrResult = outputArray.slice()
        var curWin
        var tabs
        var tabsCount
        var previewUrl
        var closedTabs

        // Check all open windows
        for (let x in windowList) {
            curWin = windowList[x]
            if (isOpenWindows) {
                tabs = curWin.tabsModel
                tabsCount = tabs.count
            } else {
                tabs = curWin.tabs
                tabsCount = tabs.length
            }
            for (let t = 0; t < tabsCount; ++t) {
                if (isOpenWindows) {
                    previewUrl = tabs.get(t).preview.toString()
                } else {
                    previewUrl = tabs[t].preview
                }
                previewUrl = FileUtils.getFilename(previewUrl)

                if (arrResult.indexOf(previewUrl) == -1) {
                    arrResult.push(previewUrl)
                }
            }

            // Check closed tabs
            closedTabs = curWin.closedTabHistory.slice()
            for (let t = 0; t < closedTabs.length; ++t) {
                previewUrl = FileUtils.getFilename(closedTabs[t].state.preview)
                if (arrResult.indexOf(previewUrl) == -1) {
                    arrResult.push(previewUrl)
                }
            }
        }
        
        return arrResult
    }
}
