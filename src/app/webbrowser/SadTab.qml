/*
 * Copyright 2015-2016 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5 as QQC2

Rectangle {
    id: sadTab

    property var webview

    color: theme.palette.normal.background

    signal closeTabRequested()

    ColumnLayout {
        anchors {
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
            verticalCenterOffset: -units.gu(10)
            margins: units.gu(4)
        }
        spacing: units.gu(3)

        Image {
            Layout.alignment: Qt.AlignHCenter
            source: "assets/tab-error.png"
        }

        Label {
            Layout.fillWidth: true
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            text: webview ? i18n.tr("The rendering process has been closed for this tab.") : ""
        }

        Label {
            Layout.fillWidth: true
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            font.weight: Font.Light
            text: {
                return i18n.tr("Something went wrong while displaying %1.").arg(webview.url)
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.maximumWidth: units.gu(40)
            Layout.alignment: Qt.AlignHCenter
            spacing: units.gu(2)

            QQC2.Button {
                objectName: "closeTabButton"
                Layout.fillWidth: true
                icon {
                    name: "close"
                    width: units.gu(1.5)
                    height: units.gu(1.5)
                }
                // FIXME: Remove workaround for icon and label spacing
                text: " " + i18n.tr("Close tab")
                onClicked: closeTabRequested()
            }

            QQC2.Button {
                objectName: "reloadButton"
                Layout.fillWidth: true
                icon {
                    name: "reload"
                    width: units.gu(1.5)
                    height: units.gu(1.5)
                }
                // FIXME: Remove workaround for icon and label spacing
                text: " " + i18n.tr("Reload")
                onClicked: {
                    webview.reload()
                    sadTab.visible = false
                }
            }
        }
    }
}
