/*
 * Copyright 2015-2016 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import QtQuick.Layouts 1.12
import ".."

Item {
    id: tabItem
    objectName: "tabItem"

    property bool incognito: false
    property bool active: false
    property bool hoverable: true
    property real rightMargin: 0

    property alias title: label.text
    property alias subtitle: secondaryLabel.text
    property alias icon: favicon.source
    property alias loading: busyIndicator.running

    property real dragMin: 0
    property real dragMax: 0

    property color fgColor: theme.palette.normal.baseText
    property color fgSecondaryColor: theme.palette.normal.backgroundSecondaryText

    property bool touchEnabled: true

    property bool showCloseIcon: closeIcon.x > units.gu(1) + tabItem.width / 2

    signal selected()
    signal closed()
    signal contextMenu()

    Rectangle {
        id: tabImage
        anchors.fill: parent
        anchors.rightMargin: tabItem.rightMargin
        color: theme.palette.normal.background
        border.color: theme.palette.normal.background
        radius: units.gu(0.5)

        Rectangle {
            height: tabImage.radius
            anchors.bottom: tabImage.bottom
            anchors.left: tabImage.left
            anchors.right: tabImage.right
            color: theme.palette.normal.background
            z:1
        }

        Favicon {
            id: favicon
            visible: !busyIndicator.running
            anchors {
                left: parent.left
                leftMargin: Math.min(tabItem.width / 4, units.gu(2))
                verticalCenter: parent.verticalCenter
            }
            shouldCache: !incognito

            // Scale width and height of favicon when tabWidth becomes small
            height: width
            width: Math.min(units.dp(16), Math.min(tabItem.width - anchors.leftMargin * 2, tabItem.height))
        }
        
        QQC2.BusyIndicator {
            id: busyIndicator
            anchors.centerIn: favicon
            width: units.gu(2)
            height: width
            running: false
        }

        Item {
            anchors.left: favicon.right
            anchors.leftMargin: units.gu(1)
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: closeButton.left
            anchors.rightMargin: units.gu(1)


            RowLayout {
                id: labelLayout

                clip: true
                anchors.fill: parent

                Label {
                    id: label

                    Layout.alignment: Qt.AlignVCenter
                    elide: Text.ElideRight
                    textSize: Label.Medium
                    color: tabItem.fgColor
                }

                Label {
                    id: secondaryLabel

                    Layout.preferredHeight: label.height
                    verticalAlignment: Text.AlignBottom
                    elide: Text.ElideRight
                    textSize: Label.Small
                    color: tabItem.fgSecondaryColor
                    visible: text !== ""
                }

                Item { Layout.fillWidth: true }
            }

            Rectangle {
                anchors.centerIn: labelLayout
                width: label.paintedHeight
                height: labelLayout.width + units.gu(0.25)
                rotation: 90
                gradient: Gradient {
                    GradientStop {
                        position: 0.0;
                        color: active ? theme.palette.normal.background :
                               (hoverArea.containsMouse ? theme.palette.normal.base : theme.palette.normal.foreground)
                    }
                    GradientStop { position: 0.33; color: "transparent" }
                }
            }
        }

        QQC2.AbstractButton {
            id: closeButton
            objectName: "closeButton"

            // On touch the tap area to close the tab occupies the whole right
            // hand side of the tab, while it covers only the close icon in
            // other form factors
            anchors.fill: touchEnabled ? undefined : closeIcon
            anchors.top: touchEnabled ? parent.top : undefined
            anchors.bottom: touchEnabled ? parent.bottom : undefined
            anchors.right: touchEnabled ? parent.right : undefined
            width: touchEnabled ? units.gu(4) : closeIcon.width
            visible: closeIcon.visible

            onClicked: closed()

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.MiddleButton
                onClicked: closed()
            }

            property string tooltipText: i18n.tr("Close tab")

            focusPolicy: Qt.TabFocus

            QQC2.ToolTip.delay: 1000
            QQC2.ToolTip.visible: hovered
            QQC2.ToolTip.text: tooltipText
        }

        Rectangle {
            id: closeIcon
            color: closeButton.hovered ? theme.palette.normal.foregroundText : "transparent"
            radius: width / 2
            height: units.gu(2.5)
            width: height
            anchors.right: parent.right
            anchors.rightMargin: units.gu(1)
            anchors.verticalCenter: parent.verticalCenter
            Icon {
                asynchronous: true
                name: "close"
                anchors.centerIn: parent
                height: units.gu(1.5)
                width: height
                color: closeButton.hovered ? theme.palette.normal.foreground : tabItem.fgColor
            }
            visible: tabItem.showCloseIcon
        }
    }
}
