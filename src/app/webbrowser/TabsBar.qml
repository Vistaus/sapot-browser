/*
 * Copyright 2013-2017 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Extras 0.3 as Extras
import Ubuntu.Components.Popups 1.3
import "."
import ".."

Extras.TabsBar {
    id: tabsBar

    readonly property bool contextTabMenuOpen: contextTabMenu.opened

    actionColor: incognito ? theme.palette.normal.background : theme.palette.normal.backgroundText
    color: incognito ? theme.palette.normal.backgroundSecondaryText : theme.palette.normal.base  // FIXME: not in palette hardcode for now
    backgroundColor: incognito ? theme.palette.normal.overlayText : theme.palette.normal.background  // FIXME: not in palette hardcode for now
    foregroundColor: incognito ? theme.palette.normal.background : theme.palette.normal.backgroundText
    dragAndDrop {
        enabled: __platformName != "ubuntumirclient"
        maxYDiff: height / 12
        mimeType: "webbrowser/tab-" + (incognito ? "incognito" : "public")
        previewUrlFromIndex: function(index) {
            if (tabsBar.model.get(index)) {
                return PreviewManager.previewPathFromUrl(tabsBar.model.get(index).url)
            } else {
                return "";
            }
        }
    }
    fallbackIcon: "stock_website"
    windowFactoryProperties: {
        "incognito": tabsBar.incognito,
        "height": window.height,
        "width": window.width,
    }

    property bool incognito

    signal requestNewTab(string url, int index, bool makeCurrent, var sourceTab)
    signal tabClosed(int index, bool moving)
    signal openRecentView

    onContextMenu: contextTabMenu.show(tabDelegate, index)

    // Note: This works as a binding, when the returned value changes, QML recalls the function
    function iconSourceFromModelItem(modelData, index) {
        return modelData.tab ? modelData.tab.localIcon : "";
    }

    function titleFromModelItem(modelItem) {
        return modelItem.title ? modelItem.title : (modelItem.url.toString() ? modelItem.url : i18n.tr("New tab"))
    }

    actions: [
        Action {
            iconName: "search"
            objectName: "searchTabButton"
            onTriggered: tabsBar.openRecentView()
        },
        Action {
            // FIXME: icon from theme is fuzzy at many GUs
//                     iconSource: Qt.resolvedUrl("Tabs/tab_add.png")
            iconName: "add"
            objectName: "newTabButton"
            onTriggered: tabsBar.model.addTab()
        }
    ]

    AdvancedMenu {
        id: contextTabMenu
        objectName: "contextTabMenu"

        property var caller
        property int targetIndex
        property var tab

        modal: false

        function show(tabCaller, tabIndex) {
            caller = tabCaller
            targetIndex = tabIndex
            tab = tabsBar.model.get(targetIndex) // Binding in properties stops working when moving tabs to new window
            popup()
        }

        CustomizedMenuItem {
            text: i18n.tr("New adjacent tab")
            iconName: tabsBar.incognito ? "private-tab-new" : "tab-new"
            onTriggered: {
                tabsBar.requestNewTab("", contextTabMenu.targetIndex + 1, true, null)
            }
        }

        CustomizedMenuItem {
            text: i18n.tr("Reload")
            iconName: "reload_page"
            rightDisplay: tabsBar.model.selectedIndex == contextTabMenu.targetIndex ? shortcutReloadTab.nativeText : ""
            enabled: contextTabMenu.tab && contextTabMenu.tab.url.toString().length > 0 ? true : false
            onTriggered: contextTabMenu.tab.reload()
        }

        CustomizedMenuItem {
            enabled: contextTabMenu.tab && contextTabMenu.tab.url.toString() ? true : false
            text: i18n.tr("Duplicate tab")
            iconName: "edit-copy"
            onTriggered: {
                tabsBar.requestNewTab(contextTabMenu.tab.url, contextTabMenu.targetIndex + 1, true, contextTabMenu.tab)
            }
        }

        CustomizedMenuItem {
            text: i18n.tr("Move to new window")
            iconName: "go-next"
            enabled: browser.appWindows.length == 1 && !tabsBar.incognito
            onTriggered: {
                window.moveTabToWindow(contextTabMenu.targetIndex, contextTabMenu.tab, null)
            }
        }
        
        MoveToWindowMenu {
            model: browser.appWindows
            targetIndex: contextTabMenu.targetIndex
            tab: contextTabMenu.tab
            thisWindow: window
            enabled: !tabsBar.incognito && model.length > 1
        }

        CustomizedMenuItem {
            enabled: contextTabMenu.tab && contextTabMenu.tab.webview && contextTabMenu.tab.url.toString() ? true : false
            text: contextTabMenu.tab && contextTabMenu.tab.webview ? contextTabMenu.tab.webview.audioMuted ? i18n.tr("Unmute tab") : i18n.tr("Mute tab")
                        : ""
            iconName: contextTabMenu.tab && contextTabMenu.tab.webview ? contextTabMenu.tab.webview.audioMuted ? "audio-volume-high" : "audio-volume-muted"
                            : ""
            rightDisplay: tabsBar.model.selectedIndex == contextTabMenu.targetIndex ? shortcutMuteTab.nativeText : ""
            onTriggered: {
                contextTabMenu.tab.toggleMute()
            }
        }

        CustomizedMenuSeparator{}

        CustomizedMenuItem {
            text: i18n.tr("Close tab")
            iconName: "close"
            rightDisplay: tabsBar.model.selectedIndex == contextTabMenu.targetIndex ? shortcutCloseCurrentTab.nativeText : ""
            onTriggered: {
                tabsBar.tabClosed(contextTabMenu.targetIndex, false)
            }
        }
    }
}
