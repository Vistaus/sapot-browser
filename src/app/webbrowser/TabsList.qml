/*
 * Copyright 2014-2016 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQml.Models 2.2
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Controls.Suru 2.2
import webbrowsercommon.private 0.1
import "../TextUtils.js" as TextUtils
import "../" as Common

Item {
    id: tabslist

    enum GridType {
        AspectRatio
        , Square
        , List
    }

    property bool browserWide: false
    property real browserHeight
    property real browserWidth
    property int gridType
    property int gridSize
    property real chromeHeight
    property bool usePhysicalGesture
    property string searchUrlTemplate
    property real tabChromeHeight: tabslist.gridType == TabsList.GridType.List ? units.gu(6) : units.gu(5)
    property alias model: filteredModel.model
    readonly property int count: model.count
    readonly property bool searchEmpty: filteredModel.count == 0 && !searchDelay.running
    readonly property bool fullyShown: internal.fullyShown
    property alias searchText: searchField.text
    property alias view: list.item
    property bool incognito
    property bool searchMode: true
    property bool enableImmediateSearch: true
    property bool enableNewTabSuggestions: true
    property real bottomDragDistance
    property var tabWithCurrentContextMenu

    signal scheduleTabSwitch(int index)
    signal tabSelected(int index, url previewUrl, var previewContainer)
    signal goToUrlRequested(url url, int index)
    signal tabClosed(int index)
    signal requestClose
    signal tabContextMenu(var tabObject, bool mouseClick)

    readonly property bool animating: showAnimation.running

    opacity: 0
    visible: opacity > 0

    // WORKAROUND: When searchMode is false by default, grid width lsit equal to parent.width
    // even if it should only be a percentage of it. It happens more often on desktop
    // though it also occured on a device.
    Component.onCompleted: searchMode = false

    function reset() {
        if (tabslist.view) {
            tabslist.view.contentY = 0
            if (tabslist.view.currentIndex) {
                tabslist.view.currentIndex = 0
            }
            tabslist.view.focus = false
        }
        searchText = ""
        internal.fullyShown = false
        internal.forceGoToSearch = false
        searchMode = false
        hide()
    }

    function focusInput() {
        searchMode = true
        searchField.selectAll();
        searchField.forceActiveFocus()
    }

    function selectFirstItem() {
        if (internal.goToSearchShown) {
            goToListView.currentIndex = 0
            goToListView.currentItem.clicked()
        } else if (matchGroup.count > 0) {
            var selectedTab = tabslist.view.itemAt(0, 0) // Get first item
            tabslist.tabSelected(tabsModel.indexOf(selectedTab.tabObj), selectedTab.tabObj.preview, selectedTab.previewContainer)
        }
    }

    function commit() {
        reset()
        show()

        if (bottomDragDistance > 0) {
            showAnimation.from = content.y
        } else {
            showAnimation.from = parent.height + units.gu(20)
        }
        showAnimation.restart()
        delayBackground.start()

        if (internal.tall) {
            searchMode = true
        } else {
            searchMode = false
        }

        tabslist.view.forceActiveFocus()
    }

    function show() {
        opacity = 1
    }

    function hide() {
        opacity = 0
    }

    Behavior on opacity {
        UbuntuNumberAnimation {
            duration: UbuntuAnimation.BriskDuration
        }
    }

    QtObject {
        id: internal

        property bool fullyShown: false
        property bool forceGoToSearch: false
        readonly property bool goToSearchShown: tabslist.enableNewTabSuggestions && (tabslist.searchEmpty || forceGoToSearch)
        readonly property bool tall: tabslist.browserWide && tabslist.height >= units.gu(80)
    }

    Timer {
        id: delayBackground
        interval: 300
    }

    Item {
        id: content

        anchors {
            left: parent.left
            right: parent.right
        }
        height: tabslist.fullyShown ? parent.height : parent.height + (parent.height / 4)
        y: {
            if (internal.fullyShown) {
                if (closeSwipeAreaLoader.item && closeSwipeAreaLoader.item.dragging
                            && closeSwipeAreaLoader.item.distance >= 0) {
                    return closeSwipeAreaLoader.item.distance
                } else {
                    return 0
                }
            }

            return parent.height - bottomDragDistance
        }

        UbuntuNumberAnimation on y {
            id: showAnimation

            running: false
            to: 0
            duration: UbuntuAnimation.BriskDuration
            onStopped: internal.fullyShown = true
        }

        Behavior on y {
            enabled: bottomDragDistance == 0 && internal.fullyShown
            UbuntuNumberAnimation { duration: UbuntuAnimation.FastDuration}
        }

        Rectangle {
            id: searchRec

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: units.gu(6)
            color: "transparent"
            opacity: tabslist.searchMode ? 1 : 0
            Behavior on opacity {
                UbuntuNumberAnimation {
                    duration: UbuntuAnimation.FastDuration
                }
            }

            TextField {
                id: searchField

                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    right: parent.right
                    margins: units.gu(1)
                }
                placeholderText: keyboardModel.count > 0 ? i18n.tr("Search Tabs (Ctrl + Space)") : i18n.tr("Search Tabs")
                inputMethodHints: Qt.ImhNoPredictiveText
                primaryItem: Icon {
                    height: parent.height * 0.5
                    width: height
                    name: "search"
                }

                KeyNavigation.down: internal.goToSearchShown ? goToListView : tabslist.view

                onTextChanged: {
                    var trimmedText = text.trim()
                    internal.forceGoToSearch = false

                    if (trimmedText) {
                        if (text.substr(text.length - 2) == "  ") { // Trailing double space
                            internal.forceGoToSearch = true
                        }
                    }

                    if (tabslist.enableImmediateSearch && text.charAt(text.length - 1) == " ") { // Trailing single space
                        searchDelay.triggered()
                    } else {
                        searchDelay.restart()
                    }
                }
                onAccepted: tabslist.selectFirstItem()
                onActiveFocusChanged: {
                    if (activeFocus) {
                        tabslist.searchMode = true
                    }

                    if (KeyNavigation.down == tabslist.view) {
                        tabslist.view.processFocusChange()
                    }
                }

                Timer {
                    id: searchDelay
                    interval: 300
                    onTriggered: filteredModel.update(searchField.text.trim())
                }
            }
        }

        ListView {
            id: goToListView

            property int previousIndex: 0

            visible: internal.goToSearchShown
            height: contentItem.height
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                topMargin: tabslist.searchMode ? searchRec.height : 0
                margins: units.gu(1)
            }
            spacing: units.gu(1)
            currentIndex: -1
            clip: true
            boundsBehavior: Flickable.OvershootBounds
            model: ListModel {}

            delegate: Common.RoundedItemDelegate {
                id: itemDelegate

                anchors {
                    left: parent.left
                    right: parent.right
                }
                radius: units.gu(1.5)
                text: model.text
                rightSideText: index == 0 && ListView.view.currentIndex <= 0 ? tabslist.browserWide ? i18n.tr("↵ Enter")
                                                                                                    : i18n.tr("↵")
                                                                             : ""

                highlighted: goToListView.currentIndex == index

                icon.name: model.type == "url" ? "stock_website" : "find"
                icon.width: units.gu(2)
                icon.height: units.gu(2)

                onClicked: {
                    tabslist.goToUrlRequested(model.url, tabsModel.selectedIndex + 1)
                }
            }

            Keys.onEnterPressed: currentItem.clicked()
            Keys.onReturnPressed: currentItem.clicked()

            onFocusChanged: {
                if (!focus) {
                    previousIndex = currentIndex
                    currentIndex = -1
                } else {
                    if (previousIndex > -1) {
                        currentIndex = previousIndex
                    }
                }
            }

            onActiveFocusChanged: tabslist.view.processFocusChange()

            onVisibleChanged: {
                if (!visible) {
                    focus = false
                    currentIndex = -1
                    previousIndex = 0
                }
            }

            Connections {
                target: tabslist
                onSearchEmptyChanged: {
                    if (internal.goToSearchShown) {
                        var trimmedText = searchField.text.trim()
                        var isURL = UrlUtils.looksLikeAUrl(trimmedText)
                        var elidedText = isURL ? TextUtils.elideMidText(trimmedText, tabslist.browserWide ? 30 : 15)
                                               : TextUtils.elideText(trimmedText, tabslist.browserWide ? 30 : 15)

                        var searchType = isURL ? "url" : "search"
                        var displayText = isURL ? i18n.tr('Go to "%1" in new tab').arg(elidedText)
                                                            : i18n.tr('Search "%1" in new tab').arg(elidedText)

                        var requestedUrl = isURL ? UrlUtils.fixUrl(trimmedText)
                                                    : TextUtils.buildSearchUrl(trimmedText, tabslist.searchUrlTemplate)

                        goToListView.model.clear()
                        goToListView.model.append({
                                         type: searchType
                                         , text: displayText
                                         , url: requestedUrl
                                     })

                        if (isURL) {
                            goToListView.model.append({
                                             type: "search"
                                             , text: i18n.tr('Search "%1" in new tab').arg(elidedText)
                                             , url: TextUtils.buildSearchUrl(trimmedText, tabslist.searchUrlTemplate)
                                         })
                         }
                    }
                }
            }
        }

        Loader {
            id: list

            asynchronous: true
            anchors.fill: parent
            anchors.topMargin: tabslist.searchMode ? searchRec.height + (goToListView.visible ? goToListView.height + units.gu(0.5)
                                                                                              : 0)
                                                   : 0 
            sourceComponent: listWideComponent

            Behavior on anchors.topMargin {
                UbuntuNumberAnimation {
                    duration: UbuntuAnimation.SnapDuration
                }
            }
        }

        Loader {
            id: closeSwipeAreaLoader

            active: tabslist.view.contentY == 0
            asynchronous: true
            anchors.fill: list
            sourceComponent: Common.SwipeGestureHandler {
                readonly property int stageTrigger: usePhysicalUnit ? 4 : 3

                direction: SwipeArea.Downwards
                immediateRecognition: false
                usePhysicalUnit: tabslist.usePhysicalGesture

                onStageChanged: {
                    if (stage == stageTrigger) Common.Haptics.play()
                }

                onDraggingChanged: {
                    if (!dragging && towardsDirection) {
                        if (stage >= stageTrigger) {
                            tabslist.requestClose()
                        }
                    }
                }
            }
        }

        Label {
            id: resultsLabel

            text: searchDelay.running ? i18n.tr("Loading...") : i18n.tr("No matching tabs")
            textSize: Label.Large
            font.weight: Font.DemiBold
            color: UbuntuColors.porcelain
            anchors {
                top: searchDelay.running ? searchRec.bottom : undefined
                horizontalCenter: searchDelay.running ? parent.horizontalCenter : undefined
                centerIn: searchDelay.running ? undefined : parent
                margins: units.gu(3)
            }
            visible: (tabslist.searchEmpty) || (searchDelay.running && filteredModel.count == 0)
        }
    }

    DelegateModel {
        id: filteredModel

        function update(searchText) {
            if (items.count > 0) {
                items.setGroups(0, items.count, ["items"]);
            }

            if (searchText) {
                filterOnGroup = "match"
                var match = [];
                var searchTextUpper = searchText.toUpperCase()
                var titleUpper
                var urlUpper
                var item

                for (var i = 0; i < items.count; ++i) {
                    item = items.get(i);
                    titleUpper = item.model.title.toUpperCase()
                    urlUpper = item.model.url.toString().toUpperCase()

                    if (titleUpper.indexOf(searchTextUpper) > -1 || urlUpper.indexOf(searchTextUpper) > -1 ) {
                        match.push(item);
                    }
                }

            for (i = 0; i < match.length; ++i) {
                    item = match[i];
                    item.inMatch = true;
                }
            } else {
                filterOnGroup = "items"
            }
        }

        groups: [
            DelegateModelGroup {
                id: matchGroup

                name: "match"
                includeByDefault: false
            }
        ]

        delegate: Item {
            id: gridDelegate

            readonly property BrowserTab tabObj: model.tab
            readonly property alias previewContainer: tabPreview.previewContainer

            width: tabslist.view.cellWidth
            height: tabslist.view.cellHeight

            TabPreview {
                id: tabPreview

                title: model.title ? model.title : (model.url.toString() ? model.url : i18n.tr("New tab"))
                subtitle: !showPreviewImage ? UrlUtils.extractHost(model.url) : ""
                tabIcon: model.icon
                incognito: tabslist.incognito
                tab: model.tab
                showPreviewImage: tabslist.gridType !== TabsList.GridType.List
                chromeHeight: tabslist.tabChromeHeight
                isCurrentTab: tabsModel.indexOf(model.tab) == tabsModel.currentIndex // Using currentTab doesn't work properly when moving tabs
                isCurrentItem: gridDelegate.GridView.isCurrentItem
                isContextMenuHighlight: tabslist.tabWithCurrentContextMenu ? tabslist.tabWithCurrentContextMenu == model.tab : false

                anchors {
                    fill: parent
                    margins: gridDelegate.GridView.view.itemMargins
                }

                onSelected: selectedDelay.restart()
                onMouseClicked: selectedDelay.mouseClick = true
                onContextMenu: tabslist.tabContextMenu(model.tab, mouseClick)
                onClosed: tabslist.tabClosed(tabsModel.indexOf(gridDelegate.tabObj))

                function selectItem() {
                    tabslist.tabSelected(tabsModel.indexOf(gridDelegate.tabObj), gridDelegate.tabObj.preview, gridDelegate.previewContainer)
                }

                // WORKAROUND: TapHandler in TabPreview only handles mouse clicks
                // However, it also triggers SwipeDelegate's onClicked signal
                // Any kind of handling touchscreen events to solve this
                // conflicts with the swipe gesture and disables it
                // This will ignore selected signal when mouse is clicked
                Timer {
                    id: selectedDelay
                    property bool mouseClick: false

                    running: false
                    interval: 1
                    onTriggered: {
                        if (!mouseClick) {
                            tabPreview.selectItem()
                        }
                        mouseClick = false
                    }
                }
            }
        }
    }

    Component {
        id: listWideComponent

        GridView {
            id: gridView

            readonly property real preferredGridWidth: units.gu(tabslist.gridSize)
            readonly property int minimumColumns: 2

            property int columnCount: {
                let intendedCount = Math.floor(width / preferredGridWidth)
                return Math.max(intendedCount, minimumColumns)
            }

            readonly property real itemMargins: units.gu(1)

            // Process separately from activeFocusChanged
            // so it won't execute in other activeFocus changes
            // such as window unfocus and menus
            function processFocusChange() {
                if (activeFocus) {
                    if (searchField.text.trim() == "" && !internal.tall) {
                        tabslist.searchMode = false
                    }
                    currentIndex = 0
                } else {
                    currentIndex = -1
                }
            }

            clip: true
            model: filteredModel
            boundsMovement: contentY == 0 ? Flickable.StopAtBounds : Flickable.FollowBoundsBehavior
            boundsBehavior: Flickable.DragOverBounds

            cellWidth: {
                switch (tabslist.gridType) {
                    case TabsList.GridType.List:
                        return width
                    default:
                        return width / columnCount
                }
            }

            cellHeight: {
                switch (tabslist.gridType) {
                    case TabsList.GridType.Square:
                        return cellWidth
                    case TabsList.GridType.List:
                        return tabslist.tabChromeHeight + units.gu(2)
                    default:
                        return (
                            ( ( cellWidth - (itemMargins * 2) ) * ( (tabslist.browserHeight - tabslist.chromeHeight) ) )
                                        / tabslist.browserWidth
                        ) + (itemMargins * 2) + tabslist.tabChromeHeight
                }
            }

            KeyNavigation.up: internal.goToSearchShown ? goToListView : searchField
            Keys.onEnterPressed: tabslist.tabSelected(tabsModel.indexOf(currentItem.tabObj), currentItem.tabObj.preview, currentItem.previewContainer)
            Keys.onReturnPressed: tabslist.tabSelected(tabsModel.indexOf(currentItem.tabObj), currentItem.tabObj.preview, currentItem.previewContainer)

            add: Transition {
                UbuntuNumberAnimation { properties: "y"; duration: UbuntuAnimation.SnapDuration }
            }

            onMovingChanged: {
                if (moving && searchField.text.trim() == "" && !internal.tall) {
                    tabslist.searchMode = false
                }
            }

            MouseArea {
                z: -1
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton
                onClicked: tabslist.requestClose()
            }
        }
    }

    Loader {
        active: true
        z: content.z + 1
        anchors {
            right: parent.right
            rightMargin: units.gu(2)
            bottom: parent.bottom
            bottomMargin: units.gu(3)
        }

        sourceComponent: Common.ScrollPositioner {
            id: scrollPositioner

            target: tabslist.view
            mode: "Down"
        }
    }
}
