/*
 * Copyright 2014-2015 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.12
import ".." as Common

Common.BaseToolbar {
    id: toolbar

    readonly property bool isFullyShown: y == (parent.height - height)

    Behavior on y {
        UbuntuNumberAnimation {
            duration: UbuntuAnimation.BriskDuration
        }
    }

    backgroundColor: browser.incognito ? theme.palette.normal.base : theme.palette.normal.foreground

    state: "shown"

    states: [
        State {
            name: "hidden"
            PropertyChanges {
                target: toolbar
                y: toolbar.parent.height
            }
        },
        State {
            name: "shown"
            PropertyChanges {
                target: toolbar
                y: toolbar.parent.height - toolbar.height
            }
        }
    ]
}
