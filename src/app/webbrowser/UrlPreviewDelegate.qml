/*
 * Copyright 2015-2016 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import webbrowserapp.private 0.1
import webbrowsercommon.private 0.1
import ".."
import "."

AbstractButton {
    id: preview

    property url icon
    property alias title: titleLabel.text
    property url url

    property alias previewHeight: previewShape.height
    property alias previewWidth: previewShape.width

    signal setCurrent()
    signal removed()

    onPressAndHold: previewShape.openContextMenu()

    Column {
        id: contentColumn
        anchors.left: parent.left
        anchors.top: parent.top
        spacing: units.gu(1)

        Label {
            id: titleLabel
            anchors.left: parent.left
            anchors.right: parent.right
            elide: Text.ElideRight
            fontSize: "small"
        }

        UbuntuShape {
            id: previewShape
            anchors.left: parent.left
            width: units.gu(26)
            height: units.gu(16)
            backgroundColor: theme.palette.normal.background

            property url previewUrl: Qt.resolvedUrl(PreviewManager.previewPathFromUrl(preview.url))
            readonly property bool hasPreview: FileOperations.exists(previewUrl)

            Favicon {
                id: previewImage
                source: preview.icon
                anchors.centerIn: parent
                width: height
                height: parent.height * 0.8
            }

            function openContextMenu() {
                preview.setCurrent()
                PopupUtils.open(contextMenuComponent, previewShape)
            }
        }
    }

    MouseArea {
        anchors.fill: contentColumn
        acceptedButtons: Qt.RightButton
        onClicked: previewShape.openContextMenu()
    }

    Component {
        id: contextMenuComponent
        ActionSelectionPopover {
            objectName: "urlPreviewDelegate.contextMenu"
            grabDismissAreaEvents: true
            actions: ActionList {
                Action {
                    objectName: "delete"
                    text: i18n.tr("Remove")
                    onTriggered: {
                        preview.removed()
                        preview.GridView.view.forceActiveFocus()
                    }
                }
            }
        }
    }
}
