/*
 * Copyright 2013-2017 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.5
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import "."
import ".."
import webbrowsercommon.private 0.1
import webbrowserapp.private 0.1
import Qt.labs.platform 1.0
import Morph.Web 0.1
import ".." as Common

QtObject {
    id: webbrowserapp

    function init(urls, newSession, incognito) {
        i18n.domain = "morph-browser"

        // Copy data to the backup file for the previous session
        sessionBackupPrevious.save()

        // Backup session data upon startup
        sessionBackupCurrent.save(0)

        if (!newSession && settings.restoreSession && ! (incognito || settings.incognitoOnStart)) {
            session.restore();
        }
        if (allWindows.length == 0) {
            windowFactory.createObject(null, {"incognito": (incognito || settings.incognitoOnStart)}).show();
        }
        
        var window = allWindows[allWindows.length - 1];
        for (var i in urls) {
            let _foundTabIndex = settings.openLinkInExistingTab ? window.tabsModel.searchSameUrl(urls[i]) : -1

            if (_foundTabIndex == -1) { // Not found
                window.addTab(urls[i]).load();
                window.tabsModel.currentIndex = window.tabsModel.count - 1;
            } else {
                window.tabsModel.get(_foundTabIndex).lastUseTimestamp = (new Date).getTime()
                window.tabsModel.selectTab(_foundTabIndex)
            }
        }
        if (window.tabsModel.count === 0) {
            window.addTab(settings.homepage).load();
            window.tabsModel.currentIndex = 0;
        }
        for (var w in allWindows) {
            allWindows[w].tabsModel.currentTab.load();
        }

        // FIXME: do this asynchronously
        BookmarksModel.databasePath = dataLocation + "/bookmarks.sqlite";
        HistoryModel.databasePath = dataLocation + "/history.sqlite";
        DownloadsModel.databasePath = dataLocation + "/downloads.sqlite";
        DomainPermissionsModel.databasePath = dataLocation + "/domainpermissions.sqlite";
        DomainPermissionsModel.whiteListMode = settings.domainWhiteListMode;
        DomainSettingsModel.defaultZoomFactor = settings.zoomFactor;
        DomainSettingsModel.databasePath = dataLocation + "/domainsettings.sqlite";
        UserAgentsModel.databasePath = DomainSettingsModel.databasePath;

        // create path for pages printed to PDF
        FileOperations.mkpath(Qt.resolvedUrl(cacheLocation) + "/pdf_tmp");
        
        // create and set downloads path
        SharedWebContext.sharedContext.downloadPath = dataLocation + "/Downloads";
        SharedWebContext.sharedIncognitoContext.downloadPath = dataLocation + "/Downloads";
        FileOperations.mkpath(Qt.resolvedUrl(dataLocation) + "/Downloads");

        // Cleanup unused previews
        cleanUpPreviews()
    }

    // Array of all windows, sorted chronologically (most recently active last)
    property var allWindows: []
    property var closedWindows: []
    property var deletedClosedWindows: []
    property int maxClosedWindows: 10
    property int protectedWindowsCount
    
    function cleanUpPreviews() {
        var doNotCleanUrls = []

        // Check all open windows
        doNotCleanUrls = PreviewManager.listUsedPreviews(allWindows, doNotCleanUrls, true)

        // Check all closed windows
        doNotCleanUrls = PreviewManager.listUsedPreviews(closedWindows, doNotCleanUrls, false)

        // Check windows from previous session backup
        for (let ses in sessionBackupPrevious.dataArray) {
            doNotCleanUrls = PreviewManager.listUsedPreviews(sessionBackupPrevious.dataArray[ses].windows, doNotCleanUrls, false)
        }

        // Check windows from current session backup
        for (let ses in sessionBackupCurrent.dataArray) {
            doNotCleanUrls = PreviewManager.listUsedPreviews(sessionBackupCurrent.dataArray[ses].windows, doNotCleanUrls, false)
        }

        PreviewManager.cleanUnusedPreviews(doNotCleanUrls)
    }

    function getLastActiveWindow(incognito) {
        for (var i = allWindows.length - 1; i >= 0; --i) {
            var window = allWindows[i]
            if (window.incognito === incognito) {
                return window
            }
        }
        return null
    }

    function openUrls(urls, newWindow, incognito) {
        let _window
        let _switchToTabIndex = -1
        if (newWindow) {
            _window = windowFactory.createObject(null, {"incognito": incognito})
        } else {
            _window = getLastActiveWindow(incognito)
            // Handle when there's no qualified window
            // i.e. Only open window is an incognito window
            // Open the link in the last closed non-incognito window
            if (!_window) {
                undoCloseWindow(0)
                _window = allWindows[allWindows.length - 1] // Get most recent window
            }
        }

        for (let i in urls) {
            let _foundTabIndex = settings.openLinkInExistingTab ? _window.tabsModel.searchSameUrl(urls[i]) : -1

            if (_foundTabIndex == -1) { // Not found
                _window.addTab(urls[i]).load()
                _switchToTabIndex = _window.tabsModel.count - 1
            } else {
                _window.tabsModel.get(_foundTabIndex).lastUseTimestamp = (new Date).getTime()
                _switchToTabIndex = _foundTabIndex
            }
        }
        if (_window.tabsModel.count === 0) {
            _window.addTab().load()
        }
        // WORKAROUND: window.raise() isn't supported in UT
        // and moved tab doesn't get focused properly
        _window.hide();

        _window.tabsModel.selectTab(_switchToTabIndex)
        _window.show()
        _window.requestActivate()
        _window.raise()
    }

    function undoCloseWindow(indexToRestore) {
        if (closedWindows.length > 0) {
            // For triggering property change on closedWindows
            var temp = closedWindows.slice()
            var windowInfo
            if (indexToRestore && indexToRestore > -1) {
                windowInfo = temp[indexToRestore]
                temp.splice(indexToRestore, 1);
            } else {
                windowInfo = temp.shift()
            }
            closedWindows = temp.slice()
            var window = session.restoreWindowState(windowInfo)
            window.tabsModel.currentTab.load();
            window.requestActivate()
            window.raise()
        }
    }

    property var windowFactory: Component {
        BrowserWindow {
            id: window

            color: "#111111"

            property alias incognito: browser.incognito
            readonly property alias model: browser.tabsModel
            readonly property var tabsModel: browser.tabsModel
            property alias closedTabHistory: browser.closedTabHistory
            property alias deletedClosedTabHistory: browser.deletedClosedTabHistory
            property string windowName: ""
            property bool primary: false
            property bool protectedWindow: false
            property bool leftSidePanePinned: false

            currentWebview: browser.currentWebview

            title: {
                if (browser.title) {
                    if (window.windowName) {
                        // TRANSLATORS: %1 is user set window's name and %2 refers to the current page’s title
                        return i18n.tr("[%1] %2 - Sapot Browser").arg(window.windowName).arg(browser.title)
                    } else {
                        // TRANSLATORS: %1 refers to the current page’s title
                        return i18n.tr("%1 - Sapot Browser").arg(browser.title)
                    }
                } else {
                    if (window.windowName) {
                        // TRANSLATORS: %1 is user set window's name
                        return i18n.tr("[%1] Sapot Browser").arg(window.windowName)
                    } else {
                        return i18n.tr("Sapot Browser")
                    }
                }
            }

            onActiveChanged: {
                if (active) {
                    var index = allWindows.indexOf(this)
                    if (index > -1) {
                        // Trigger data change in allWindows
                        var temp = allWindows.slice()
                        temp.push(temp.splice(index, 1)[0])
                        allWindows = temp.slice()
                    }
                }
            }

            onPrimaryChanged: {
                if (primary && !protectedWindow) {
                    webbrowserapp.protectedWindowsCount += 1
                } else if (!primary && !protectedWindow) {
                    webbrowserapp.protectedWindowsCount -= 1
                }
            }

            onProtectedWindowChanged: {
                if (protectedWindow && !primary) {
                    webbrowserapp.protectedWindowsCount += 1
                } else if (!protectedWindow && !primary) {
                    webbrowserapp.protectedWindowsCount -= 1
                }
            }

            onClosing: {
                if (!session.restoring) {
                    if (allWindows.length == 1) {
                        if (tabsModel.count > 0) {
                            console.log("Session save (Window closing): " + title)
                            session.save(2) // Save before closing since this window is already destroyed in onAboutToQuit
                        } else {
                            console.log("Session clear (Window closing): " + title)
                            session.clear()
                        }
                    } else if (allWindows.length > 1 && !incognito && tabsModel.count > 0
                                        && !(tabsModel.currentTab.url == "" && tabsModel.count == 1) // Window should not be single new tab
                                                && !primary && !protectedWindow) {
                        webbrowserapp.deletedClosedWindows = [] // Clear deleted history when new windows are closed
                        
                        // For triggering property change on closedWindows
                        var temp = webbrowserapp.closedWindows.slice()

                        // Delete last item when max is reached
                        if (temp.length == browser.maxClosedWindows) {
                            var lastItem = temp.pop()
                        }

                        temp.unshift(session.serializeWindowState(this))

                        webbrowserapp.closedWindows = temp.slice()
                    }
                }

                if (incognito && (allWindows.length > 1)) {
                    // If the last incognito window is being closed,
                    // prune incognito entries from the downloads model
                    var incognitoWindows = 0
                    for (var w in allWindows) {
                        var window = allWindows[w]
                        if ((window !== this) && window.incognito) {
                            ++incognitoWindows
                        }
                    }
                    if (incognitoWindows == 0) {
                        DownloadsModel.pruneIncognitoDownloads()
                    }
                }

                if (!session.restoring) {
                    if (allWindows.length > 1)
                    {
                        if (primary || protectedWindow) {
                            Qt.quit()
                        } else {
                            session.save()
                            for (var win in allWindows) {
                                if (this === allWindows[win]) {
                                    // TODO: Why is this necessary???
                                    var tabs = allWindows[win].tabsModel
                                    for (var t = tabs.count - 1; t >= 0; --t) {
                                        //console.log("remove tab with url " + tabs.get(t).url)
                                        tabs.removeTabNoHistory(t)
                                    }
                                    // Trigger data change in allWindows
                                    var temp = allWindows.slice()
                                    temp.splice(win, 1)
                                    allWindows = temp.slice()
                                    return
                                }
                            }
                        }
                    }
                }

                destroy()
            }

            function setAsPrimary() {
                for (var w in allWindows) {
                    if (allWindows[w] == this) {
                        primary = true
                    } else {
                        allWindows[w].primary = false
                    }
                }
            }

            function moveTabToWindow(tabIndex, tab, targetWindow) {
                var newWindow = false
                // callback function only removes from model
                // and not destroy as webview is in new window
                // Create new window, if moving to new window then add existing tab
                if (!targetWindow) {
                    newWindow = true
                    var windowFactoryProperties = {
                        "incognito": window.incognito,
                        "height": window.height,
                        "width": window.width
                    }
                    targetWindow = webbrowserapp.windowFactory.createObject(null, windowFactoryProperties);
                }

                // WORKAROUND: window.raise() isn't supported in UT
                // and moved tab doesn't get focused properly
                targetWindow.hide();

                targetWindow.model.addExistingTab(tab);
                targetWindow.show();
                targetWindow.requestActivate()
                targetWindow.raise();

                // Just remove from model and do not destroy
                // as webview is used in other window
                window.tabsModel.removeTabWithoutDestroying(tabIndex);
            }

            function openLinkInWindow(linkUrl, targetWindow) {
                // WORKAROUND: window.raise() isn't supported in UT
                // and moved tab doesn't get focused properly
                targetWindow.hide();

                targetWindow.tabsModel.addTab(linkUrl)
                targetWindow.tabsModel.currentTab.load()
                targetWindow.show()
                targetWindow.requestActivate()
            }

            Shortcut {
                id: shortcutQuitApp
                sequence: StandardKey.Quit
                onActivated: Qt.quit()
            }

            function toggleApplicationLevelFullscreen() {
                let notFullscreen = visibility !== Window.FullScreen
                setFullscreen(notFullscreen)
                window.manualFullscreen = notFullscreen
            }

            Shortcut {
                sequence: StandardKey.FullScreen
                onActivated: window.toggleApplicationLevelFullscreen()
            }

            Shortcut {
                id: shortcutFullscreen
                sequence: "F11"
                onActivated: window.toggleApplicationLevelFullscreen()
            }

            Shortcut {
                id: shortcutNewWindow
                sequence: "Ctrl+N"
                onActivated: browser.newWindowRequested(false)
            }

            Shortcut {
                id: shortcutNewPrivateWindow
                sequence: "Ctrl+Shift+P"
                onActivated: browser.newWindowRequested(true)
            }

            Shortcut {
                id: shortcutRestoreWindow
                sequence: "Ctrl+Shift+N"
                onActivated: webbrowserapp.undoCloseWindow()
            }

            Component.onCompleted: {
                // Trigger data change in allWindows
                var temp = allWindows.slice()
                temp.push(this)
                allWindows = temp.slice()
            }

            Browser {
                id: browser
                anchors.fill: parent
                thisWindow: window
                settings: webbrowserapp.settings
                windowFactory: webbrowserapp.windowFactory
                appWindows: webbrowserapp.allWindows
                onNewWindowRequested: {
                    var window = windowFactory.createObject(
                        null,
                        {
                            "incognito": incognito,
                            "height": parent.height,
                            "width": parent.width,
                        }
                    )

                    window.addTab()
                    window.tabsModel.currentIndex = 0
                    window.tabsModel.currentTab.load()
                    window.show()

                }
                onOpenLinkInNewWindowRequested: {
                    var window = windowFactory.createObject(
                        null,
                        {
                            "incognito": incognito,
                            "height": parent.height,
                            "width": parent.width,
                        }
                    )
                    window.addTab(url)
                    window.tabsModel.currentIndex = window.tabsModel.count - 1
                    window.tabsModel.currentTab.load()
                    window.show()
                    window.requestActivate()
                }

                // Not handled as a window-level shortcut as it would take
                // precedence over key events in web content.
                Keys.onEscapePressed: {
                    // ESC to exit fullscreen, regardless of whether it was
                    // requested by the page or toggled on by the user.
                    window.setFullscreen(false)
                }
            }

            Connections {
                target: window.incognito ? null : window.tabsModel
                onCurrentIndexChanged: {
                    // console.log("Session save (current tab): " + window.title + " - " + allWindows.length + " windows")
                    delayedSessionSaver.restart()
                }
                onCountChanged: {
                    // console.log("Session save (tab count): " + window.title + " - " + allWindows.length + " windows")
                    delayedSessionSaver.restart()
                }
            }

            function serializeTabState(tab) {
                return browser.serializeTabState(tab)
            }

            function restoreTabState(state) {
                return browser.restoreTabState(state)
            }

            function addTab(url) {
                var tab = browser.createTab({"initialUrl": url || ""})
                tabsModel.add(tab)
                return tab
            }
        }
    }

    property var settings: Settings {
        property url homepage: ""
        property string searchEngine: "duckduckgo"
        property bool restoreSession: true
        property bool setDesktopMode: false
        property bool autoFitToWidthEnabled: false
        property real zoomFactor: 1.0
        property int newTabDefaultSection: 0
        property string defaultAudioDevice: ""
        property string defaultVideoDevice: ""
        property bool domainWhiteListMode: false
        property bool incognitoOnStart: false
        property bool hideTabsBar: false
        property bool autoVideoRotate: true
        property bool hideBottomHint: false
        property bool appWideScrollPositioner: false
        property bool forceMobileSite: false
        property bool autoDeskMobSwitch: false
        property bool autoDeskMobSwitchReload: true
        property bool autoHideHeader: true
        property int headerHide: autoHideHeader ? 1 : 0 // Migrate autoHideHeader value
        /*
         0 - Disabled
         1 - On scroll down
         2 - Time out (Header shows when using bottom gestures)
         3 - Always hidden
        */
        property bool physicalForGestures: false // Use physical unit for swipe gestures (in inches)
        property bool enableHaptics: true
        property bool hideTabsButtonNavBar: false
        property bool enableWebviewPullDownGestures: false
        property int tabsGridType: TabsList.GridType.AspectRatio // 0: App's aspect ratio 1: Square 2: List
        property int tabsGridSize: 40
        property var webviewQuickActions: [
            [
                { "id": "webviewPullDown" }
                , { "id": "focusAddressbar" }
                , { "id": "webviewReload" }
                , { "id": "webviewForward" }
                , { "id": "webviewBack" }
            ]
            , [
                { "id": "closeTab" }
                , { "id": "toggleSiteVersion" }
                , { "id": "openBookmarks" }
                , { "id": "findInPage" } 
                , { "id": "searchTabs" }
                , { "id": "newTab" }
            ]
        ]
        property bool webviewHorizontalSwipe: true
        property bool webviewSideSwipe: true
        property bool webviewQuickSideSwipe: true
        property bool webviewEnableQuickActions: false
        property bool webviewBottomHoldTabsSearch: false
        property bool appPagesPullDown: true
        property bool appPagesQuickSideSwipe: true
        property bool appPagesDirectActions: false
        property bool appPagesSideSwipe: true
        property bool appPagesHorizontalSwipe: false
        property bool appPagesHorizontalSwipeHint: true
        property bool focusedSearch: true
        property bool tabsSearchBypassDelay: true
        property bool tabsSearchNewTabSuggestions: true

        property var searchSuggestions: [
            { "id": "tabs", "enabled": true, "limit": 3, "showInIncognito": true }
            , { "id": "history", "enabled": true, "limit": 2, "showInIncognito": true }
            , { "id": "bookmarks", "enabled": true, "limit": 2, "showInIncognito": true }
            , { "id": "searchengine", "enabled": true, "limit": 4, "showInIncognito": false }
        ]

        property bool firstRun: true
        property bool alwaysAskNewViewRequests: true
        property bool webviewQuickActionEnableDelay: true
        property bool appPagesQuickActionEnableDelay: true
        property real webviewQuickActionsHeight: 3 // In Inch
        property real appPagesQuickActionsHeight: 3 // In Inch
        property real bottomGesturesAreaHeight: 2 // In Grid Unit
        property real bottomGesturesAreaHeightBigUIMode: 4 // In Grid Unit
        property bool enableBigUIModeToggle: false
        property bool bigUIModeBottomHint: true
        property int bigUIModeHeaderHide: 0
        /*
         0 - Disabled
         1 - On scroll down
         2 - Time out (Header shows when using bottom gestures)
         3 - Always hidden
        */
        property bool openLinkInExistingTab: false // Switch to existing tab if it has the same url
        property bool loadImages: true

        Component.onCompleted: Common.Haptics.enabled = Qt.binding( function() { return enableHaptics } )

        function restoreDefaults() {
            homepage = ""
            searchEngine = "duckduckgo";
            restoreSession = true;
            setDesktopMode = false;
            autoFitToWidthEnabled = false;
            zoomFactor = 1.0;
            newTabDefaultSection = 0;
            defaultAudioDevice = "";
            defaultVideoDevice = "";
            domainWhiteListMode = false;
            incognitoOnStart = false;
            hideTabsBar = false;
            autoVideoRotate = true;
            hideBottomHint = false;
            appWideScrollPositioner = false;
            forceMobileSite = false;
            autoDeskMobSwitch = false;
            autoDeskMobSwitchReload = true;
            autoHideHeader = true;
            headerHide = 0;
            physicalForGestures = false;
            enableHaptics = true;
            hideTabsButtonNavBar = false;
            enableWebviewPullDownGestures = false;
            tabsGridType = TabsList.GridType.AspectRatio;
            tabsGridSize = 40;
            webviewQuickActions = [
                [
                    { "id": "webviewPullDown" }
                    , { "id": "focusAddressbar" }
                    , { "id": "webviewReload" }
                    , { "id": "webviewForward" }
                    , { "id": "webviewBack" }
                ]
                , [
                    { "id": "closeTab" }
                    , { "id": "toggleSiteVersion" }
                    , { "id": "openBookmarks" }
                    , { "id": "findInPage" } 
                    , { "id": "searchTabs" }
                    , { "id": "newTab" }
                ]
            ]
            webviewHorizontalSwipe = true
            webviewSideSwipe = true
            webviewEnableQuickActions = false
            webviewBottomHoldTabsSearch = false
            appPagesPullDown = true
            appPagesQuickSideSwipe = true
            appPagesDirectActions = false
            appPagesSideSwipe = true
            appPagesHorizontalSwipe = false
            appPagesHorizontalSwipeHint = true
            focusedSearch = true
            tabsSearchBypassDelay = true
            tabsSearchNewTabSuggestions = true
            searchSuggestions = [
                { "id": "tabs", "enabled": true, "limit": 3, "showInIncognito": true }
                , { "id": "history", "enabled": true, "limit": 2, "showInIncognito": true }
                , { "id": "bookmarks", "enabled": true, "limit": 2, "showInIncognito": true }
                , { "id": "searchengine", "enabled": true, "limit": 4, "showInIncognito": false }
            ]
            alwaysAskNewViewRequests = true
            webviewQuickActionEnableDelay = true
            appPagesQuickActionEnableDelay = true
            webviewQuickActionsHeight = 3
            appPagesQuickActionsHeight = 3
            bottomGesturesAreaHeight = 2
            bottomGesturesAreaHeightBigUIMode = 4
            enableBigUIModeToggle = false
            bigUIModeBottomHint = true
            bigUIModeHeaderHide = 0
            openLinkInExistingTab = false
            loadImages = true
        }

        function resetDomainPermissions() {
            DomainPermissionsModel.deleteAndResetDataBase();
        }

        function resetDomainSettings() {
            DomainSettingsModel.deleteAndResetDataBase();
            // it is a common database with DomainSettingsModel, so it is only for reset here
            UserAgentsModel.deleteAndResetDataBase();
        }
    }

    // Handle runtime requests to open urls as defined
    // by the freedesktop application dbus interface's open
    // method for DBUS application activation:
    // http://standards.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html#dbus
    // The dispatch on the org.freedesktop.Application if is done per appId at the
    // url-dispatcher/upstart level.
    property var openUrlsHandler: Connections {
        target: UriHandler
        onOpened: webbrowserapp.openUrls(uris, false, settings.incognitoOnStart)
    }

    // Saved session data from current session
    property var sessionBackupCurrent: SessionStorage {
        dataFile: dataLocation + "/session_bk_current.json"

        // Index 0: Session saved on app startup
        // Index 1: Session saved periodically
        // Index 2: Session saved before quitting the app
        // Index 3: Session before restoring a backup session
        property var dataArray: []

        function save(itemIndex) {
            var rawData = session.retrieve()

            if (rawData) {
                try {
                    var parsedData = JSON.parse(rawData)
                } catch (e) {
                    console.log("Session data is corrupted: " + e)
                    return
                }

                if (dataArray.length == 0) {
                    // Initialize array
                    dataArray = [{windows: []}, {windows: []}, {windows: []}]
                }
                var temp = dataArray.slice()
                temp[itemIndex] = parsedData
                dataArray = temp.slice()
                store(JSON.stringify({sessions: dataArray}))
            }
        }

        function clear() {
            if (!locked) {
                return
            }
            store("")
        }
    }

    // Saved session data from previous session (previous time app was opened)
    property var sessionBackupPrevious: SessionStorage {
        dataFile: dataLocation + "/session_bk_previous.json"

        property var dataArray

        function save() {
            var rawData = sessionBackupCurrent.retrieve()
            if (rawData) {
                store(rawData)

                try {
                    var parsedData = JSON.parse(rawData)
                    dataArray = parsedData.sessions
                } catch (e) {
                    console.log("Session data is corrupted: " + e)
                    return
                }
            }
        }
    }

    property var session: SessionStorage {
        dataFile: dataLocation + "/session.json"

        function save(backupIndex) {

            if (!locked || restoring) {
                return
            }

            // WORKAROUND: Safety net for unexpected events such as crashing
            // to protect protected windows from being permanently deleted.
            // Possibly doesn't cover all situations
            // so may still fail to protect them from deletion
            var protectedCount = 0

            var windows = []
            for (var w in allWindows) {
                var curWin = allWindows[w]
                if (curWin.incognito) {
                    continue
                }

                if (curWin.primary || curWin.protectedWindow) {
                    protectedCount += 1
                }

                windows.push(serializeWindowState(curWin))
            }

            if (windows.length > 0 && webbrowserapp.protectedWindowsCount == protectedCount) {
                // Backup before saving new data
                // backupIndex determines what type of backup it is
                if (backupIndex) {
                    sessionBackupCurrent.save(backupIndex)
                }

                store(JSON.stringify({windows: windows, closedwindows: closedWindows}))
            } else {
                console.log("Session not saved because of missing protected window: " + webbrowserapp.protectedWindowsCount + " - " + protectedCount)
            }
        }

        property bool restoring: false
        function restore() {
            restoring = true
            _doRestore()
            restoring = false
        }
        function restoreFromBackup(fromPrevious, targetIndex) {
            var alreadyRestored = sessionBackupCurrent.dataArray.length >= 4 ? true : false // Check if a session was already restored

            // Save then backup current session before restoring
            console.log("Session save before restoring backup session: " + webbrowserapp.allWindows.length + " windows")
            session.save()
            if (alreadyRestored) {
                sessionBackupCurrent.save(4)
            } else {
                sessionBackupCurrent.save(3)
            }

            restoring = true

            // Hide current windows first
            // Closing them immediately will close the app in UT
            for (var win in allWindows) {
                allWindows[win].hide()
            }

            var oldWindows = allWindows.slice()
            allWindows = []
            webbrowserapp.protectedWindowsCount = 0

            // Restore selected backup session
            var dataToRestore
            if (fromPrevious) {
                dataToRestore = sessionBackupPrevious.dataArray[targetIndex]
            } else {
                dataToRestore = sessionBackupCurrent.dataArray[targetIndex]
            }

            _doRestore(dataToRestore)

            for (var w in allWindows) {
                allWindows[w].tabsModel.currentTab.load();
            }
            
            if (alreadyRestored) {
                // Delete the last backup session from the last restoration
                var temp = sessionBackupCurrent.dataArray.slice()
                temp.splice(3, 1)
                sessionBackupCurrent.dataArray = temp.slice()
            }
            
            
            // Close old windows
            for (var win in oldWindows) {
                oldWindows[win].close()
            }

            restoring = false
        }

        function _doRestore(dataToRestore) {
            if (!locked) {
                return
            }
            var state = null
            try {
                if (dataToRestore) {
                    state = dataToRestore
                } else {
                    state = JSON.parse(retrieve())
                }
            } catch (e) {
                return
            }
            if (state) {
                closedWindows = state.closedwindows ? state.closedwindows : []

                var windows = state.windows
                var restoredWindow
                if (windows) {
                    for (var w in windows) {
                        restoredWindow = restoreWindowState(windows[w])
                    }
                } else if (state.tabs) {
                    // One-off code path: when launching the app for the first time
                    // after the upgrade that adds support for multiple windows, the
                    // saved session contains a list of tabs, not windows.
                    restoredWindow = restoreWindowState(state)
                }

                if (allWindows.length > 0) {
                    var primaryWindow
                    var newWindow
                    var curWindow

                    for (var win in allWindows) {
                        curWindow = allWindows[win]
                        if (curWindow.primary) {
                            primaryWindow = curWindow
                        }
                    }

                    if (primaryWindow) {
                        newWindow = primaryWindow
                        // WORKAROUND: window.raise() is not yet supported in UT
                        newWindow.show()
                    } else {
                        newWindow = allWindows[allWindows.length - 1]
                    }
                    newWindow.requestActivate()
                    newWindow.raise()
                }
            }
        }

        function serializeWindowState(win) {
            var tabs = []
            for (var i = 0; i < win.tabsModel.count; ++i) {
                tabs.push(win.serializeTabState(win.tabsModel.get(i)))
            }

            return {tabs: tabs, currentIndex: win.tabsModel.currentIndex,
                    width: win.width, height: win.height, closedTabHistory: win.closedTabHistory
                    , windowName: win.windowName, primary: win.primary, protectedWindow: win.protectedWindow
                    , leftSidePanePinned: win.leftSidePanePinned
                    }
        }

        function restoreWindowState(state) {
            var windowProperties = {}
            if (state.width) {
                windowProperties["width"] = state.width
            }
            if (state.height) {
                windowProperties["height"] = state.height
            }
            if (state.closedTabHistory) {
                windowProperties["closedTabHistory"] = state.closedTabHistory
            }
            if (state.windowName) {
                windowProperties["windowName"] = state.windowName
            }
            if (state.primary) {
                windowProperties["primary"] = state.primary
            }
            if (state.protectedWindow) {
                windowProperties["protectedWindow"] = state.protectedWindow
            }
            if (state.leftSidePanePinned) {
                windowProperties["leftSidePanePinned"] = state.leftSidePanePinned
            }

            var newWindow = windowFactory.createObject(null, windowProperties)
            for (var i in state.tabs) {
                newWindow.tabsModel.add(newWindow.restoreTabState(state.tabs[i]))
            }
            newWindow.tabsModel.currentIndex = state.currentIndex

            // Set current index to 0 when current index is still invalid after restoring tabs
            // This logic was removed from the cpp part of tabs model to avoid selecting the first tab
            // every app startup messing up the tab timestamp
            if (newWindow.tabsModel.currentIndex <= -1) {
                newWindow.tabsModel.currentIndex = 0
            }

            // WORKAROUND: window.raise() is not yet supported in UT
            if (!newWindow.primary) {
                newWindow.show()
            }

            return newWindow
        }

        function clear() {
            if (!locked) {
                return
            }
            store("")
        }
    }

    property var delayedSessionSaver: Timer {
        interval: 500
        onTriggered: session.save()
    }

    // Backup session every periodicBackupTime x periodicSessionSaver.interval
    property var delayedBackupSaver: Timer {
        readonly property int periodicBackupTime: 5 // Minutes
        property int periodicBackupCount: 5

        interval: 500

        onTriggered: {
            if (periodicBackupCount == 0) {
                periodicBackupCount = periodicBackupTime
                session.save(1)
            } else {
                periodicBackupCount -= 1
                session.save()
            }
        }
    }

    property var periodicSessionSaver: Timer {
        // Save session periodically to mitigate state loss when the application crashes
        interval: 60000 // every minute
        repeat: true
        running: true
        onTriggered: {
            // console.log("Session save (Periodic): " + allWindows.length + " windows")
            delayedBackupSaver.restart()
        }
    }

    property var applicationMonitor: Connections {
        target: Qt.application
        onStateChanged: {
            if (Qt.application.state !== Qt.ApplicationActive) {
                // console.log("Session save (State): " + Qt.application.state + " - " + allWindows.length + " windows")
                session.save()
            }
        }
        onAboutToQuit: {
            if (allWindows.length > 0) {
                console.log("Session save (About to quit): " + allWindows.length + " windows")
                session.save(2)
            }
        }
    }

    property var memoryPressureMonitor: Connections {
        target: MemInfo
        onFreeChanged: {
            /* Old approach
            // Under that threshold, available memory is considered "low", and the
            // browser is going to try and free up memory from unused tabs. This
            // value was chosen empirically, it is subject to change to better
            // reflect what a system under memory pressure might look like.
            */
            var freeMemRatio = (MemInfo.total > 0) ? (MemInfo.free / MemInfo.total) : 1.0
            var lowMemoryThresholdPercent = 0.2;
            var lowMemoryThreshold = 500000; // ~500MiB
            var tryToFreeMemory = MemInfo.total <= 1000000 ? freeMemRatio < lowMemoryThresholdPercent
                                                : MemInfo.free <= lowMemoryThreshold
            if (tryToFreeMemory) { // Ratio-based when memory is ~1GB, otherwise, fixed to lowMemoryThreshold
                // Unload an inactive tab to (hopefully) free up some memory
                function getCandidate(model) {
                    // Naive implementation that only takes into account the
                    // last time a tab was current. In the future we might
                    // want to take into account other parameters such as
                    // whether the tab is currently playing audio/video.
                    var candidate = null
                    for (var i = 0; i < model.count; ++i) {
                        var tab = model.get(i)
                        if (tab.current || !tab.webview) {
                            continue
                        }

                        if (!candidate || ((candidate.lastUseTimestamp > tab.lastUseTimestamp) && !tab.webview.recentlyAudible)) {
                            candidate = tab
                        }
                    }
                    return candidate
                }
                for (var w in allWindows) {
                    var candidate = getCandidate(allWindows[w].tabsModel)
                    if (candidate) {
                        if (allWindows[w].incognito) {
                            console.warn("Unloading a background incognito tab to free up some memory")
                        } else {
                            console.warn("Unloading background tab (%1) to free up some memory".arg(candidate.url))
                        }
                        candidate.unload()
                        return
                    }
                }
                console.warn("System low on memory, but unable to pick a tab to unload")
            }
        }
    }

    //Component.onCompleted: console.info("morph-browser using oxide %1 (chromium %2)".arg(Oxide.version).arg(Oxide.chromiumVersion))
}
