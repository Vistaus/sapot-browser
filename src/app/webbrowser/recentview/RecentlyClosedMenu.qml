import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Controls.Suru 2.2
import Ubuntu.Components 1.3
import "../.." as Common

Common.AdvancedMenu {
    id: recentlyClosedMenu

    readonly property bool canUndo: model && model.length == 0 && deletedModel && deletedModel.length > 0 ? true : false
    readonly property bool overflow: false
    property int maxDisplayedCount: 4
    property var model
    property var deletedModel
    property bool clearEnabled: true

    signal clear

    headerTitle: title

    Repeater {
        model: recentlyClosedMenu.model
        delegate: recentlyClosedMenu.delegate
    }
    
    QQC2.Label {
        visible: !recentlyClosedMenu.model || recentlyClosedMenu.model.length == 0 ? true : false
        text: i18n.tr("Empty")
        height: visible ? units.gu(9) : 0
        verticalAlignment: Label.AlignVCenter
        horizontalAlignment: Label.AlignHCenter
        leftPadding: units.gu(2)
        rightPadding: units.gu(2)

        Suru.textLevel: Suru.Paragraph
    }

    Common.CustomizedMenu {
        id: moreMenu
        objectName: "moreMenu"

        readonly property bool overflow: true

        title: i18n.tr("More")
        enabled: recentlyClosedMenu.model && (recentlyClosedMenu.model.length > recentlyClosedMenu.maxDisplayedCount) ? true : false

        Repeater {
            model: recentlyClosedMenu.model
            delegate: recentlyClosedMenu.delegate
            onCountChanged: if (count == 0) moreMenu.close()
        }
    }

    Common.CustomizedMenuSeparator{
        visible: clearMenuItem.enabled
    }

    Item {
        height: clearMenuItem.enabled ? units.gu(6) : 0
        Common.CustomizedMenuItem {
            id: clearMenuItem
            objectName: "clearMenuItem"

            anchors.fill: parent
            text: recentlyClosedMenu.canUndo ? i18n.tr("Undo clear") : i18n.tr("Clear")
            iconName: recentlyClosedMenu.canUndo ? "undo" : "delete"
            enabled: recentlyClosedMenu.clearEnabled && (recentlyClosedMenu.model.length > 0 || recentlyClosedMenu.canUndo)
            onTriggered: recentlyClosedMenu.clear()
        }
    }
}
