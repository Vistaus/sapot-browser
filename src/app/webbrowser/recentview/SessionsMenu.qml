import QtQuick 2.9
import "../.." as Common

RecentlyClosedMenu {
    id: sessionsMenu

    signal itemTriggered(int targetIndex, string targetTitle)

    clearEnabled: false
    delegate: Common.CustomizedMenuItem {
        property string itemName: {
            if (!menu.overflow) {
                switch (index) {
                    case 0:
                        return i18n.tr("On startup")
                        break
                    case 1:
                        return i18n.tr("Periodic save")
                        break
                    case 2:
                        return i18n.tr("On closing")
                        break
                    case 3:
                        return i18n.tr("Before last restore")
                        break
                    default:
                        return ""
                        break
                }
            } else {
                return ""
            }
        }

        enabled: modelData && modelData.windows && modelData.windows.length > 0 ? true : false
        text: {
            if (enabled) {
                if (modelData.windows.length == 1) {
                    if (itemName == "") {
                        return i18n.tr("%1 tab", "%1 tabs", modelData.windows[0].tabs.length).arg(modelData.windows[0].tabs.length)
                    } else {
                        return i18n.tr("%1 - (%2 tab)", "%1 - (%2 tabs)", modelData.windows[0].tabs.length).arg(itemName).arg(modelData.windows[0].tabs.length)
                    }
                } else {
                    if (itemName == "") {
                        return i18n.tr("%1 window", "%1 windows", modelData.windows.length).arg(modelData.windows.length)
                    } else {
                        return i18n.tr("%1 (%2 window)", "%1 (%2 windows)", modelData.windows.length).arg(itemName).arg(modelData.windows.length)
                    }
                }
            } else {
                return ""
            }
        }
        onTriggered: {
            sessionsMenu.itemTriggered(index, text)
        }
    }
}
