import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12
import "../.." as Common
import "." as Local

Common.DialogWithContents {
    id: windowConfigDialog
    objectName: "windowConfigDialog"

    property string windowName
    property bool primary
    property bool protectedWindow

    title: i18n.tr("Current Window")
    anchorToKeyboard: true

    signal save(string newWindowName, bool newPrimary, bool newProtectedWindow)

    TextField {
        id: windowNameTextField

        Layout.fillWidth: true
        placeholderText: "Window name/label"
        text: windowConfigDialog.windowName
    }

    Common.CustomizedSwitchDelegate {
        id: primarySwitch

        Layout.fillWidth: true
        text: i18n.tr("Primary window")
        checked: windowConfigDialog.primary
    }

    Common.DialogCaption {
        Layout.fillWidth: true
        text: i18n.tr("Primary window is displayed as top window when opening the app and it is automatically protected")
    }

    Common.CustomizedSwitchDelegate {
        id: protectedSwitch

        Layout.fillWidth: true
        text: i18n.tr("Protected window")
        visible: !primarySwitch.checked
        checked: windowConfigDialog.protectedWindow
    }

    Common.DialogCaption {
        Layout.fillWidth: true
        text: i18n.tr("Protected windows cannot be closed individually and will close the whole app instead")
    }

    ColumnLayout {
        spacing: units.gu(2)
        Layout.fillWidth: true

        Button {
            id: saveButton
            objectName: "saveButton"

            Layout.fillWidth: true
            text: i18n.tr("Save")
            color: theme.palette.normal.positive

            onClicked: {
                windowConfigDialog.save(windowNameTextField.text, primarySwitch.checked, protectedSwitch.checked)
                windowConfigDialog.close();
            }
        }

        Button {
            objectName: "cancelButton"

            Layout.fillWidth: true

            text: i18n.tr("Cancel")
            onClicked: {
                windowConfigDialog.close();
            }
        }
    }
}
